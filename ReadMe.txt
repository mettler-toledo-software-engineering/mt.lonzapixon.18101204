
Bootstrapper

This file describes how the bootstrapper "boot.exe" works.

First the following flag files will be evaluated:

  - "\Hard Disk\IND890Boot\BOS.FLG": if this file exists (content does not matter), immediately at 
    the start a message box is shown which will display the current IP addresses. The boot process
    is stopped until this message box is closed with OK.
  - "\Hard Disk\IND890Boot\DEBUG.FLG": if this file exits (content does not matter), the processes
    "ConmanClient2.exe" and "CMAccept.exe" will be started that it is possible to debug applications
    on the terminal.
  - "\Hard Disk\IND890Boot\ATTACH.FLG": if this file exits (content does not matter), the registry key
    "HKLM\SOFTWARE\Microsoft\.NETCompactFramework\Managed Debugger\AttachEnabled" will be set to "1"
    that it is possible to attach to a running application on the terminal.

After this the file "\Hard Disk\IND890Boot\boot.xml" is processed. See into this file to get further
information about its content.   
