﻿using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Drawing;
using System;
using System.Collections.Generic;
using System.Text;

namespace MT.LonzaPixon.Controls
{

    public class DeltaTracViewModel : PropertyChangedBase
    {
        private static readonly Brush ctrlBackgro = new SolidColorBrush(new Color(0xFF44E726));

        private Visual Parent;
        double TargetValue { get; set; }
        double MinimalValue { get; set; }
        double UpperToleranceAbs { get; set; }
        double LowerToleranceAbs { get; set; }
        double TargetPositionInPercent { get; set; }

        public DeltaTracViewModel(Visual Parent, int Width, int Height, double MinimalValue, double TargetValue, double TargetPositionInPercent, double UpperToleranceAbs, double LowerToleranceAbs)
        {
            this.Parent = Parent;
            PanelWidth = Width;
            PanelHeight = Height;
            this.MinimalValue = MinimalValue;
            this.TargetValue = TargetValue;
            this.TargetPositionInPercent = TargetPositionInPercent;
            this.UpperToleranceAbs = UpperToleranceAbs;
            this.LowerToleranceAbs = LowerToleranceAbs;
            RebuildControl();
        }




        // int CtrlHeight { get; set; }


        bool inTolerance { get; set; }
        bool underTolerance { get; set; }
        bool overTolerance { get; set; }
        private double barValue = 0;
        public double Value
        {
            get
            {
                return barValue;
            }
            set
            {
                barValue = value;
                if (barValue < (TargetValue - LowerToleranceAbs) || barValue > (TargetValue + UpperToleranceAbs))
                    CtrlBarFill = BarFillBad;
                else
                    CtrlBarFill = BarFillGood;
                int y = (int)((barValue - MinimalValue) * ratio);
                if (y < 0)
                    y = 0;
                else if (y > PanelWidth)
                    y = PanelWidth;
                CtrlBarWidth = y;
                underTolerance = value < (TargetValue - LowerToleranceAbs);
                overTolerance = value > (TargetValue + UpperToleranceAbs);
                inTolerance = !underTolerance && !overTolerance;
            }
        }


        private double ratio;
        private int lowerLimPt, upperLimPt, targetLinePt;

        //public DeltaTrac(int Width, int Height, double MinimalValue, double TargetValue, double TargetPositionInPercent, double UpperToleranceAbs, double LowerToleranceAbs)
        //{
        //    InitializeComponents();
        //    this.CtrlWidth = Width;
        //    this.CtrlHeight = Height;
        //    this.MinimalValue = MinimalValue;
        //    this.TargetValue = TargetValue;
        //    this.UpperToleranceAbs = UpperToleranceAbs;
        //    this.LowerToleranceAbs = LowerToleranceAbs;
        //    this.TargetPositionInPercent = TargetPositionInPercent;
        //    CtrlBackground = new SolidColorBrush(new Color(0xFFFFFFFF));
        //    BarFillGood = new SolidColorBrush(new Color(0xFF44E726));
        //    BarFillBad = new SolidColorBrush(new Color(0xFFF40202));
        //    RebuildControl();
        //    Value = 0;
        //}
        //public DeltaTrac()
        //{
        //    InitializeComponents();
        //    CtrlWidth = 300;
        //    CtrlHeight = 20;
        //    TargetValue = 100;
        //    UpperToleranceAbs = 5;
        //    LowerToleranceAbs = 5;
        //    CtrlBackground = new SolidColorBrush(new Color(0xFFFFFFFF));
        //    BarFillGood = new SolidColorBrush(new Color(0xFF44E726));
        //    BarFillBad = new SolidColorBrush(new Color(0xFFF40202));
        //    RebuildControl();
        //    Value = 0;
        //}

        private void RebuildControl()
        {
            PanelBackground = CtrlBackground;
            CtrlBarHeight = PanelHeight;
            ratio = (double)PanelWidth / (100 / TargetPositionInPercent * (TargetValue - MinimalValue));
            targetLinePt = (int)((TargetValue - MinimalValue) * ratio)-1;
            lowerLimPt = (int)(((TargetValue - MinimalValue) - LowerToleranceAbs) * ratio);
            upperLimPt = (int)(((TargetValue - MinimalValue) + UpperToleranceAbs) * ratio);
            TargetLineHeight = PanelHeight;
            LowerTolLineHeight = PanelHeight;
            UpperTolLineHeight = PanelHeight;
            TargetLineMargin = new Thickness(targetLinePt, 0, 0, 0);
            LowerTolLineMargin = new Thickness(lowerLimPt, 0, 0, 0);
            UpperTolLineMargin = new Thickness(upperLimPt, 0, 0, 0);
        }

        Brush ctrlBackground = new SolidColorBrush(new Color(0xFFFFFFFF));

        public Brush CtrlBackground
        {
            get { return ctrlBackground; }
            set { ctrlBackground = value; }
        }

        Brush barFillGood = new SolidColorBrush(new Color(0xFF44E726));

        public Brush BarFillGood
        {
            get { return barFillGood; }
            set { barFillGood = value; }
        }


        private Brush barFillBad = new SolidColorBrush(new Color(0xFFF40202));

        public Brush BarFillBad
        {
            get { return barFillBad; }
            set { barFillBad = value; }
        }





        int ctrlWidth;

        public int CtrlWidth1
        {
            get { return ctrlWidth; }
            set { ctrlWidth = value; }
        }
        #region Properties
        int panelWidth; // 1000

        public int PanelWidth
        {
            get { return panelWidth; }
            set
            {
                if (value != panelWidth)
                {
                    panelWidth = value;
                    NotifyPropertyChanged();
                }
            }
        }


        int panelHeight; // 20

        public int PanelHeight
        {
            get { return panelHeight; }
            set
            {
                if (value != panelHeight)
                {
                    panelHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Brush panelBackground;  //"#FFD5EAFF"

        public Brush PanelBackground
        {
            get { return panelBackground; }
            set
            {
                if (value != panelBackground)
                {
                    panelBackground = value;
                    NotifyPropertyChanged();
                }
            }
        }
        int ctrlBarHeight; //20

        public int CtrlBarHeight
        {
            get { return ctrlBarHeight; }
            set
            {
                if (value != ctrlBarHeight)
                {
                    ctrlBarHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }
        int ctrlBarWidth; //260

        public int CtrlBarWidth
        {
            get { return ctrlBarWidth; }
            set
            {
                if (value != ctrlBarWidth)
                {
                    ctrlBarWidth = value;
                    NotifyPropertyChanged();
                }
            }
        }
        Brush ctrlBarFill; // "#FF44E726

        public Brush CtrlBarFill
        {
            get { return ctrlBarFill; }
            set
            {
                if (value != ctrlBarFill)
                {
                    ctrlBarFill = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Thickness lowerTolLineMargin; // "750,0,0,0"

        public Thickness LowerTolLineMargin
        {
            get { return lowerTolLineMargin; }
            set
            {
                if (value != lowerTolLineMargin)
                {
                    lowerTolLineMargin = value;
                    NotifyPropertyChanged();
                }
            }
        }

        int lowerTolLineHeight;//20

        public int LowerTolLineHeight
        {
            get { return lowerTolLineHeight; }
            set
            {
                if (value != lowerTolLineHeight)
                {
                    lowerTolLineHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }
        Thickness targetLineMargin; // "800,0,0,0"

        public Thickness TargetLineMargin
        {
            get { return targetLineMargin; }
            set
            {
                if (value != targetLineMargin)
                {
                    targetLineMargin = value;
                    NotifyPropertyChanged();
                }
            }
        }
        int targetLineHeight;//20

        public int TargetLineHeight
        {
            get { return targetLineHeight; }
            set
            {
                if (value != targetLineHeight)
                {
                    targetLineHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }
        Thickness upperTolLineMargin;//"850,0,0,0"

        public Thickness UpperTolLineMargin
        {
            get { return upperTolLineMargin; }
            set
            {
                if (value != upperTolLineMargin)
                {
                    upperTolLineMargin = value;
                    NotifyPropertyChanged();
                }
            }
        }
        int upperTolLineHeight;// 20

        public int UpperTolLineHeight
        {
            get { return upperTolLineHeight; }
            set
            {
                if (value != upperTolLineHeight)
                {
                    upperTolLineHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }




        #endregion

    }
}
