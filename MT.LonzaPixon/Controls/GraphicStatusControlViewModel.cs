﻿using MT.LonzaPixon.Logic;
using MT.LonzaPixon.Pages;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MT.LonzaPixon.Controls
{


    public class GraphicStatusControlViewModel : PropertyChangedBase
    {
        private Visual Parent;
        public Spinner spinner;
        private FlexiconPump _pump = FlexiconPump.Instance;
        private DigitalIOLonza _digitalIOLonza { get { return DigitalIOLonza.InstanceLonza; } }

        public GraphicStatusControlViewModel(Visual Parent)
        {
            this.Parent = Parent;
            spinner = new Spinner(Parent);
        }

        public async void InitParams()
        {
            Speed = _pump.FlexiconParams.RPM;
            Direction = _pump.FlexiconParams.PumpDirection == PumpDirectionEnum.Left_Counterclockwise ? Spinner.Direction.left : Spinner.Direction.right;
            _pump.PumpStatusChanged += _pump_PumpStatusChanged;
            _digitalIOLonza.EventKlemmenStatusChanged += _digitalIOLonza_EventKlemmenStatusChanged;
            _digitalIOLonza.EventFuellkopfStatusChanged += _digitalIOLonza_EventFuellkopfStatusChanged;
            spinner.viewModel.InitImage();

            DigitalIOLonza.FuellkopfIstState state = DigitalIOLonza.InstanceLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollQuery);
            SetImageFuelLance(state);

        }

        private void SetImageFuelLance(DigitalIOLonza.FuellkopfIstState state)
        {
            switch (state)
            {
                case DigitalIOLonza.FuellkopfIstState.istHoch:
                case DigitalIOLonza.FuellkopfIstState.istUndef:
                    {
                        ImageRUpVisibility = Visibility.Visible;
                        ImageRDownVisibility = Visibility.Collapsed;
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istTief:
                    {
                        ImageRUpVisibility = Visibility.Collapsed;
                        ImageRDownVisibility = Visibility.Visible;
                        break;
                    }
            }
        }

        private void SetImageKlemme(DigitalIOLonza.KlemmenStatus state)
        {
            switch (state)
            {
                case DigitalIOLonza.KlemmenStatus.KlemmeAuf:
                    {
                        ImageClosedVisibility = Visibility.Collapsed;
                        ImageOpenVisibility = Visibility.Visible;
                        break;
                    }
                case DigitalIOLonza.KlemmenStatus.KlemmeZu:
                    {
                        ImageClosedVisibility = Visibility.Visible;
                        ImageOpenVisibility = Visibility.Collapsed;
                        break;
                    }
            }
        }

        void _digitalIOLonza_EventFuellkopfStatusChanged(object sender, DigitalIOLonza.EventFuellkopfStatusChangedArgs e)
        {
            // e.FuellKopfStatus contains current state of Fuellkopf
            Debug.WriteLine("-----> Event FuellkopfStatus: " + e.FuellkopfStatus);
            SetImageFuelLance(_digitalIOLonza.FuellkopfIstStatus);
        }

        void _digitalIOLonza_EventKlemmenStatusChanged(object sender, DigitalIOLonza.EventKlemmenStatusChangedArgs e)
        {
            // e.KlemmenStatus contains current status of Klemme
            Debug.WriteLine("-----> Event KlemmenStatus: " + e.KlemmenStatus);
           SetImageKlemme(e.KlemmenStatus);
        }

        void _pump_PumpStatusChanged(object sender, FlexiconPump.PumpStatusChangedEventArgs e)
        {
            Direction = e.FlexiconParams.PumpDirection == PumpDirectionEnum.Left_Counterclockwise ? Spinner.Direction.left : Spinner.Direction.right;
            Speed = e.FlexiconParams.RPM;
            if (e.FlexiconParams.PumpRunning)
                spinner.viewModel.StartSpin(Direction);
            else
                spinner.viewModel.StopSpin();

        }

        public void Dispose()
        {
            spinner.viewModel.Dispose();
            _pump.PumpStatusChanged -= _pump_PumpStatusChanged;
            _digitalIOLonza.EventFuellkopfStatusChanged -= _digitalIOLonza_EventFuellkopfStatusChanged;
            _digitalIOLonza.EventKlemmenStatusChanged -= _digitalIOLonza_EventKlemmenStatusChanged;
        }

        #region Properties


        Visibility imageOpenVisibility = Visibility.Collapsed;

        public Visibility ImageOpenVisibility
        {
            get { return imageOpenVisibility; }
            set
            {
                if (value != imageOpenVisibility)
                {
                    imageOpenVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Visibility imageClosedVisibility = Visibility.Visible;

        public Visibility ImageClosedVisibility
        {
            get { return imageClosedVisibility; }
            set
            {
                if (value != imageClosedVisibility)
                {
                    imageClosedVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Visibility imageRDownVisibility;

        public Visibility ImageRDownVisibility
        {
            get { return imageRDownVisibility; }
            set
            {
                if (value != imageRDownVisibility)
                {
                    imageRDownVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Visibility imageRUpVisibility;

        public Visibility ImageRUpVisibility
        {
            get { return imageRUpVisibility; }
            set
            {
                if (value != imageRUpVisibility)
                {
                    imageRUpVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private double speed = 0;

        public double Speed
        {
            get { return speed; }
            set
            {
                if (value != speed)
                {
                    speed = value;
                    SpeedText = speed.ToString("0");
                }
            }
        }

        string speedText;

        public string SpeedText
        {
            get { return speedText; }
            set
            {
                if (value != speedText)
                {
                    speedText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public Spinner.Direction Direction
        {
            get { return spinner.viewModel.Direction; }
            set
            {
                spinner.viewModel.Direction = value;
            }
        }
        #endregion
    }
}
