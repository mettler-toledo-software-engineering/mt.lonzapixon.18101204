﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Drawing;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for DeltaTrac
    /// </summary>
    public partial class DeltaTrac
    {
        public DeltaTracViewModel viewModel;

        public DeltaTrac(Visual Parent, int Width, int Height, double MinimalValue, double TargetValue, double TargetPositionInPercent, double UpperToleranceAbs, double LowerToleranceAbs)
        {
            viewModel = new DeltaTracViewModel(Parent, Width, Height, MinimalValue, TargetValue, TargetPositionInPercent, UpperToleranceAbs, LowerToleranceAbs);
            InitializeComponents();
        }
    }
}
