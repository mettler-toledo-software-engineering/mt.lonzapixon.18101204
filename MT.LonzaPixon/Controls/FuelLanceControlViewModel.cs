﻿using MT.LonzaPixon.Logic;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MT.LonzaPixon.Pages;

namespace MT.LonzaPixon.Controls
{
    public class FuelLanceControlViewModel : PropertyChangedBase
    {
        private Visual Parent;



        public FuelLanceControlViewModel(Visual Parent)
        {
            this.Parent = Parent;

        }

        public async void InitControl()
        {
            DigitalIOLonza.FuellkopfIstState state = DigitalIOLonza.InstanceLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollQuery);
            switch (state)
            {
                case DigitalIOLonza.FuellkopfIstState.istHoch:
                    {
                        DoCmdRUp();
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istTief:
                    {
                        DoCmdRDown();
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istTimeout:
                    {
                        if (!HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            MessageBox.Show(HomeScreen.Instance, "Timeout fuel lance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istUndef:
                    {
                        if (!HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            MessageBox.Show(HomeScreen.Instance, "Undefined position of fuel lance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
            }
        }

        public ICommand cmdRUp
        {
            get { return new DelegateCommand(DoCmdRUp); }
        }

        public async void DoCmdRUp()
        {
            DigitalIOLonza.FuellkopfIstState state = DigitalIOLonza.InstanceLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollHoch);
            switch (state)
            {
                case DigitalIOLonza.FuellkopfIstState.istHoch:
                    {
                        ImageRDownVisibility = Visibility.Collapsed;
                        ImageRUpVisibility = Visibility.Visible;
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istTief:
                    {
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istTimeout:
                    {
                        if (!HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            MessageBox.Show(HomeScreen.Instance, "Timeout fuel lance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istUndef:
                    {
                        if (!HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            MessageBox.Show(HomeScreen.Instance, "Undefined position of fuel lance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
            }

        }

        public ICommand cmdRDown
        {
            get { return new DelegateCommand(DoCmdRDown); }
        }

        public async void DoCmdRDown()
        {
            DigitalIOLonza.FuellkopfIstState state = DigitalIOLonza.InstanceLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollTief);
            switch (state)
            {
                case DigitalIOLonza.FuellkopfIstState.istHoch:
                    break;
                case DigitalIOLonza.FuellkopfIstState.istTief:
                    {
                        ImageRDownVisibility = Visibility.Visible;
                        ImageRUpVisibility = Visibility.Collapsed;
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istTimeout:
                    {
                        if (!HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            MessageBox.Show(HomeScreen.Instance, "Timeout fuel lance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                case DigitalIOLonza.FuellkopfIstState.istUndef:
                    {
                        if (!HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            MessageBox.Show(HomeScreen.Instance, "Undefined position of fuel lance", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
            }

        }



        #region Properties




        Visibility imageRDownVisibility;

        public Visibility ImageRDownVisibility
        {
            get { return imageRDownVisibility; }
            set
            {
                if (value != imageRDownVisibility)
                {
                    imageRDownVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Visibility imageRUpVisibility;

        public Visibility ImageRUpVisibility
        {
            get { return imageRUpVisibility; }
            set
            {
                if (value != imageRUpVisibility)
                {
                    imageRUpVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        #endregion

    }
}
