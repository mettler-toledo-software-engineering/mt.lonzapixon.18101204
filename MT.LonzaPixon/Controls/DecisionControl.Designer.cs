﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class DecisionControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Presentation.Controls.Button btnYes;
        private MT.Singularity.Presentation.Controls.Image imgYes;
        private MT.Singularity.Presentation.Controls.TextBlock tbYes;
        private MT.Singularity.Presentation.Controls.Button btnAdditionalManualFilling;
        private MT.Singularity.Presentation.Controls.Image imgAdditionalManualFilling;
        private MT.Singularity.Presentation.Controls.TextBlock tbAdditionalManualFilling;
        private MT.Singularity.Presentation.Controls.Button btnReject;
        private MT.Singularity.Presentation.Controls.Image imgReject;
        private MT.Singularity.Presentation.Controls.TextBlock tbReject;
        private MT.Singularity.Presentation.Controls.Button btnCancel;
        private MT.Singularity.Presentation.Controls.Image imgCancel;
        private MT.Singularity.Presentation.Controls.TextBlock tbCancel;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.GroupPanel internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.GroupPanel internal9;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Margin = new MT.Singularity.Presentation.Thickness(20);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() =>  viewModel.MessageTitle,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.FontSize = ((System.Nullable<System.Int32>)36);
            internal3.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.Margin = new MT.Singularity.Presentation.Thickness(20);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  viewModel.MessageSubTitle,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4.FontSize = ((System.Nullable<System.Int32>)22);
            internal4.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            btnYes = new MT.Singularity.Presentation.Controls.Button();
            btnYes.Margin = new MT.Singularity.Presentation.Thickness(10);
            btnYes.Width = 180;
            btnYes.Height = 90;
            btnYes.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            btnYes.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            imgYes = new MT.Singularity.Presentation.Controls.Image();
            imgYes.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.ok.al8";
            imgYes.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            imgYes.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            imgYes.Margin = new MT.Singularity.Presentation.Thickness(4);
            tbYes = new MT.Singularity.Presentation.Controls.TextBlock();
            tbYes.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            tbYes.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            tbYes.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Accept);
            tbYes.AddTranslationAction(() => {
                tbYes.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Accept);
            });
            tbYes.FontSize = ((System.Nullable<System.Int32>)20);
            tbYes.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal6 = new MT.Singularity.Presentation.Controls.GroupPanel(imgYes, tbYes);
            btnYes.Content = internal6;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => btnYes.Command,() =>  viewModel.btnYes,MT.Singularity.Expressions.BindingMode.OneWay,false);
            btnAdditionalManualFilling = new MT.Singularity.Presentation.Controls.Button();
            btnAdditionalManualFilling.Margin = new MT.Singularity.Presentation.Thickness(10);
            btnAdditionalManualFilling.Width = 180;
            btnAdditionalManualFilling.Height = 90;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => btnAdditionalManualFilling.Visibility,() =>  viewModel.AdditionalManualFillingVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            btnAdditionalManualFilling.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            btnAdditionalManualFilling.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            imgAdditionalManualFilling = new MT.Singularity.Presentation.Controls.Image();
            imgAdditionalManualFilling.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Click-Tap.al8";
            imgAdditionalManualFilling.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            imgAdditionalManualFilling.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            imgAdditionalManualFilling.Margin = new MT.Singularity.Presentation.Thickness(4);
            tbAdditionalManualFilling = new MT.Singularity.Presentation.Controls.TextBlock();
            tbAdditionalManualFilling.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            tbAdditionalManualFilling.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            tbAdditionalManualFilling.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.AdditionalFilling);
            tbAdditionalManualFilling.AddTranslationAction(() => {
                tbAdditionalManualFilling.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.AdditionalFilling);
            });
            tbAdditionalManualFilling.FontSize = ((System.Nullable<System.Int32>)20);
            tbAdditionalManualFilling.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(imgAdditionalManualFilling, tbAdditionalManualFilling);
            btnAdditionalManualFilling.Content = internal7;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => btnAdditionalManualFilling.Command,() =>  viewModel.btnAdditionalManualFilling,MT.Singularity.Expressions.BindingMode.OneWay,false);
            btnReject = new MT.Singularity.Presentation.Controls.Button();
            btnReject.Margin = new MT.Singularity.Presentation.Thickness(10);
            btnReject.Width = 180;
            btnReject.Height = 90;
            btnReject.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            btnReject.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            imgReject = new MT.Singularity.Presentation.Controls.Image();
            imgReject.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Waste.al8";
            imgReject.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            imgReject.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            imgReject.Margin = new MT.Singularity.Presentation.Thickness(4);
            tbReject = new MT.Singularity.Presentation.Controls.TextBlock();
            tbReject.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            tbReject.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            tbReject.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Waste);
            tbReject.AddTranslationAction(() => {
                tbReject.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Waste);
            });
            tbReject.FontSize = ((System.Nullable<System.Int32>)20);
            tbReject.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(imgReject, tbReject);
            btnReject.Content = internal8;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => btnReject.Command,() =>  viewModel.btnReject,MT.Singularity.Expressions.BindingMode.OneWay,false);
            btnCancel = new MT.Singularity.Presentation.Controls.Button();
            btnCancel.Margin = new MT.Singularity.Presentation.Thickness(10);
            btnCancel.Width = 180;
            btnCancel.Height = 90;
            btnCancel.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            btnCancel.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            imgCancel = new MT.Singularity.Presentation.Controls.Image();
            imgCancel.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Cancel.al8";
            imgCancel.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            imgCancel.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            imgCancel.Margin = new MT.Singularity.Presentation.Thickness(4);
            tbCancel = new MT.Singularity.Presentation.Controls.TextBlock();
            tbCancel.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            tbCancel.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            tbCancel.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Back);
            tbCancel.AddTranslationAction(() => {
                tbCancel.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Back);
            });
            tbCancel.FontSize = ((System.Nullable<System.Int32>)20);
            tbCancel.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal9 = new MT.Singularity.Presentation.Controls.GroupPanel(imgCancel, tbCancel);
            btnCancel.Content = internal9;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => btnCancel.Command,() =>  viewModel.btnCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(btnYes, btnAdditionalManualFilling, btnReject, btnCancel);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal5);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(30, 0, 0, 0);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal2.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[7];
    }
}
