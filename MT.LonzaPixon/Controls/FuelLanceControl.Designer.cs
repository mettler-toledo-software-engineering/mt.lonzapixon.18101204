﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class FuelLanceControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.Button internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.Image internal8;
            MT.Singularity.Presentation.Controls.Button internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.StackPanel internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.Image internal14;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.FuelLance);
            internal3.AddTranslationAction(() => {
                internal3.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.FuelLance);
            });
            internal3.FontSize = ((System.Nullable<System.Int32>)22);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.Button();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(4, 44, 4, 4);
            internal6.Width = 60;
            internal6.Height = 60;
            internal6.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal6.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal8 = new MT.Singularity.Presentation.Controls.Image();
            internal8.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.ArrowUp.al8";
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8);
            internal6.Content = internal7;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Command,() =>  viewModel.cmdRUp,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9 = new MT.Singularity.Presentation.Controls.Button();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9.Width = 60;
            internal9.Height = 60;
            internal9.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.ArrowDown.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11);
            internal9.Content = internal10;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Command,() =>  viewModel.cmdRDown,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal9);
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.RUp.png";
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Visibility,() => viewModel.ImageRUpVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal14 = new MT.Singularity.Presentation.Controls.Image();
            internal14.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.RDown.png";
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Visibility,() => viewModel.ImageRDownVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal12 = new MT.Singularity.Presentation.Controls.StackPanel(internal13, internal14);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4, 24, 4, 4);
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal12);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(8);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.Height = 220;
            internal1.Width = 160;
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4284461385u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
