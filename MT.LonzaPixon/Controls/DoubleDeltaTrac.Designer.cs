﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class DoubleDeltaTrac : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private MT.Singularity.Presentation.Controls.DynamicStackPanel PanelDoubleDeltaTrac;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.Text = "-T";
            internal4.FontSize = ((System.Nullable<System.Int32>)14);
            internal4.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(7, 0, 0, 0);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() => viewModel.LowerToleranceFormated,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal5.FontSize = ((System.Nullable<System.Int32>)22);
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(30, 0, 0, 0);
            internal6.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Target.png";
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 0, 0);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => viewModel.TargetFormated,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal7.FontSize = ((System.Nullable<System.Int32>)22);
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.Margin = new MT.Singularity.Presentation.Thickness(30, 2, 0, 0);
            internal8.Text = "+T";
            internal8.FontSize = ((System.Nullable<System.Int32>)14);
            internal8.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 0, 0);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => viewModel.UpperToleranceFormated,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal5, internal6, internal7, internal8, internal9);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal3.Margin = new MT.Singularity.Presentation.Thickness(4);
            PanelDoubleDeltaTrac = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, PanelDoubleDeltaTrac);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
