﻿using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for ManualWork
    /// </summary>
    public partial class PumpControl
    {
        public PumpControlViewModel viewModel;
        private Visual Parent;
        public PumpControl(Visual Parent)
        {
            viewModel = new PumpControlViewModel(Parent);
            this.Parent = Parent;
            InitializeComponents();
            PanelSpinner.Add(viewModel.spinner);
            viewModel.InitDownSpeedButton(cmdSpeedDown);
            viewModel.InitUpSpeedButton(cmdSpeedUp);
            viewModel.InitParams();
        }
        
    }
}
