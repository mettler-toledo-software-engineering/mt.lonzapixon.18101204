﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for ClampControl
    /// </summary>
    public partial class ClampControl
    {
        public ClampControlViewModel viewModel;
        private Visual Parent;
        public ClampControl(Visual Parent)
        {
            viewModel = new ClampControlViewModel(Parent);
            this.Parent = Parent;
            InitializeComponents();
            viewModel.InitVisuals(swOnOff);
        }
    }
}
