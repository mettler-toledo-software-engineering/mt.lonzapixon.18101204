﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class ClampControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Presentation.Controls.Switch swOnOff;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Klemmensteuerung);
            internal3.AddTranslationAction(() => {
                internal3.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Klemmensteuerung);
            });
            internal3.FontSize = ((System.Nullable<System.Int32>)22);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(8);
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            internal5.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.closed.png";
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Visibility,() => viewModel.ImageClosedVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.open.png";
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Visibility,() => viewModel.ImageOpenVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal6);
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            swOnOff = new MT.Singularity.Presentation.Controls.Switch();
            swOnOff.Width = 120;
            swOnOff.Height = 50;
            swOnOff.Margin = new MT.Singularity.Presentation.Thickness(34);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => swOnOff.IsChecked,() =>  viewModel.SwitchChecked,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(swOnOff);
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal4, internal7);
            internal1.Height = 220;
            internal1.Width = 200;
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4284461385u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
