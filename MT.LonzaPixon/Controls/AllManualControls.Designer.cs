﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class AllManualControls : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private MT.Singularity.Presentation.Controls.DynamicStackPanel PanelManualControls;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            PanelManualControls = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            PanelManualControls.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            PanelManualControls.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(PanelManualControls);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4284461385u));
            this.Content = internal1;
        }
    }
}
