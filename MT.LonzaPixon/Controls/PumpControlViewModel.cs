﻿using MT.LonzaPixon.Controls;
using MT.LonzaPixon.Logic;
using MT.LonzaPixon.Pages;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MT.LonzaPixon
{
    public class PumpControlViewModel : PropertyChangedBase
    {
        private Visual Parent;
        public Spinner spinner;
        private FlexiconPump _pump = FlexiconPump.Instance;

        public bool Running = false;

        public PumpControlViewModel(Visual Parent)
        {
            this.Parent = Parent;
            spinner = new Spinner(Parent);
        }

        public async void InitParams()
        {
            try
            {
                DoPumpOff(false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }


            Speed = _pump.FlexiconParams.RPM;
            Direction = _pump.FlexiconParams.PumpDirection == PumpDirectionEnum.Left_Counterclockwise ? Spinner.Direction.left : Spinner.Direction.right;
            spinner.viewModel.InitImage();
        }

        public void InitUpSpeedButton(Button button)
        {
            ButtonLogic upSpeed = new ButtonLogic(button);
            upSpeed.KeyEvents += upSpeed_KeyEvents;
        }

        async void upSpeed_KeyEvents(object sender, ButtonLogic.KeyEventsEventArgs e)
        {
            switch (e.KeyStrokeType)
            {
                case ButtonLogic.KeystrokeType.ShortClick:
                    if (Speed < FlexiconPump._maxPumpSpeed)
                    {
                        Speed++;
                        _pump.PumpSpeed(Speed);
                    }
                    break;
                case ButtonLogic.KeystrokeType.Repeater:
                    if (Speed < FlexiconPump._maxPumpSpeed)
                    {
                        Speed += 10;
                        _pump.PumpSpeed(Speed);
                    }
                    if (Speed > FlexiconPump._maxPumpSpeed)
                    {
                        Speed = FlexiconPump._maxPumpSpeed;
                        _pump.PumpSpeed(Speed);
                    }
                    break;
                default:
                    break;
            }
        }

        public void InitDownSpeedButton(Button button)
        {
            ButtonLogic downSpeed = new ButtonLogic(button);
            downSpeed.KeyEvents += downSpeed_KeyEvents;
        }

        async void downSpeed_KeyEvents(object sender, ButtonLogic.KeyEventsEventArgs e)
        {
            switch (e.KeyStrokeType)
            {
                case ButtonLogic.KeystrokeType.ShortClick:
                    if (Speed > 0)
                    {
                        Speed--;
                        _pump.PumpSpeed(Speed);
                    }
                    break;
                case ButtonLogic.KeystrokeType.Repeater:
                    if (Speed > 0)
                    {
                        Speed -= 10;
                        _pump.PumpSpeed(Speed);
                    }
                    if (Speed < 0)
                    {
                        Speed = 0;
                        _pump.PumpSpeed(Speed);
                    }
                    break;
                default:
                    break;
            }
        }

        public async Task Dispose()
        {
            spinner.viewModel.Dispose();
            StatusText = Localization.Get(Localization.Key.SetPumpOff);
            if (_pump.IsOpen)
            {
                _pump.PumpOff(true);
            }
        }

        #region button_Interface_cmdTurnLeft
        public ICommand cmdTurnLeft
        {
            get { return new DelegateCommand(DoCmdTurnLeft); }
        }

        public void DoCmdTurnLeft()
        {
            Direction = Spinner.Direction.left;
            if (!Running)
            {
                spinner.viewModel.InitImage();
            }
            DoTurnLeft();
        }
        public async void DoTurnLeft()
        {
            StatusText = Localization.Get(Localization.Key.SetPumpDirection) + " " + Localization.Get(Localization.Key.left) + "..";
            _pump.PumpDirection(PumpDirectionEnum.Left_Counterclockwise);
        }
        #endregion
        #region button_Interface_cmdTurnRight
        public ICommand cmdTurnRight
        {
            get { return new DelegateCommand(DoCmdTurnRight); }
        }

        public void DoCmdTurnRight()
        {
            Direction = Spinner.Direction.right;
            if (!Running)
            {
                spinner.viewModel.InitImage();
            }
            DoTurnRight();
        }
        public async void DoTurnRight()
        {
            StatusText = Localization.Get(Localization.Key.SetPumpDirection) + " " + Localization.Get(Localization.Key.right) + "..";
            _pump.PumpDirection(PumpDirectionEnum.Right_Clockwise);
        }

        #endregion
        #region button_Interface_cmdStart
        public ICommand cmdStart
        {
            get { return new DelegateCommand(DoCmdStart); }
        }



        public async void DoCmdStart()
        {
            Running = !Running;
            if (Running)
            {
                if (DoPumpOn(false))
                {
                    spinner.viewModel.StartSpin(Direction);
                }
            }
            else
            {
                if (DoPumpOff(false))
                {
                    spinner.viewModel.StopSpin();
                }
            }

        }

        public bool DoPumpOn(bool openKlemme)
        {
            if (DigitalIOLonza.InstanceLonza.NotTaster)
            {
                StatusText = Localization.Get(Localization.Key.SetPumpOn);
                _pump.PumpOn(openKlemme);
                return true;
            }
            else
            {
                StatusText = Localization.Get(Localization.Key.EmergencySwitchActivated);
                return false;
            }
        }


        public bool DoPumpOff(bool closeKlemme)
        {
            StatusText = Localization.Get(Localization.Key.SetPumpOff);
            _pump.PumpOff(closeKlemme);
            return true;
        }
        #endregion

        #region button_Interface_cmdPresetFineFlow
        public ICommand cmdPresetFineFlow
        {
            get { return new DelegateCommand(DoCmdPresetFineFlow); }
        }

        public async void DoCmdPresetFineFlow()
        {
            Speed = HomeScreen.Instance.ViewModel.Configuration.FeinStrom;
            _pump.PumpSpeed(Speed);
        }
        #endregion
        #region button_Interface_cmdPresetCoarseFlow
        public ICommand cmdPresetCoarseFlow
        {
            get { return new DelegateCommand(DoCmdPresetCoarseFlow); }
        }

        public async void DoCmdPresetCoarseFlow()
        {
            Speed = HomeScreen.Instance.ViewModel.Configuration.GrobStrom;
            _pump.PumpSpeed(Speed);
        }
        #endregion


        private string _statusText = "";
        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText != value)
                {
                    _statusText = value;
                    HomeScreen.Instance.ViewModel.SetStatusText(value);
                    NotifyPropertyChanged();
                }
            }
        }


        private double speed = 1;

        public double Speed
        {
            get { return speed; }
            set
            {
                if (value != speed)
                {
                    speed = value;
                    SpeedText = speed.ToString("0");
                }
            }
        }

        string speedText;

        public string SpeedText
        {
            get { return speedText; }
            set
            {
                if (value != speedText)
                {
                    speedText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public Spinner.Direction Direction
        {
            get { return spinner.viewModel.Direction; }
            set
            {
                spinner.viewModel.Direction = value;
            }
        }


    }
}
