﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class DeltaTrac : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Presentation.Controls.GroupPanel CntrlPanel;
        private MT.Singularity.Presentation.Controls.Rectangle CtrlBar;
        private MT.Singularity.Presentation.Controls.Rectangle LowerTolLine;
        private MT.Singularity.Presentation.Controls.Rectangle TargetLine;
        private MT.Singularity.Presentation.Controls.Rectangle UpperTolLine;
        private void InitializeComponents()
        {
            CtrlBar = new MT.Singularity.Presentation.Controls.Rectangle();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => CtrlBar.Height,() =>  viewModel.CtrlBarHeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => CtrlBar.Width,() =>  viewModel.CtrlBarWidth,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => CtrlBar.Fill,() =>  viewModel.CtrlBarFill,MT.Singularity.Expressions.BindingMode.OneWay,false);
            LowerTolLine = new MT.Singularity.Presentation.Controls.Rectangle();
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => LowerTolLine.Margin,() =>  viewModel.LowerTolLineMargin,MT.Singularity.Expressions.BindingMode.OneWay,false);
            LowerTolLine.Width = 1;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => LowerTolLine.Height,() =>  viewModel.LowerTolLineHeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            LowerTolLine.Fill = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            TargetLine = new MT.Singularity.Presentation.Controls.Rectangle();
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => TargetLine.Margin,() =>  viewModel.TargetLineMargin,MT.Singularity.Expressions.BindingMode.OneWay,false);
            TargetLine.Width = 3;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => TargetLine.Height,() =>  viewModel.TargetLineHeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            TargetLine.Fill = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            UpperTolLine = new MT.Singularity.Presentation.Controls.Rectangle();
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => UpperTolLine.Margin,() =>  viewModel.UpperTolLineMargin,MT.Singularity.Expressions.BindingMode.OneWay,false);
            UpperTolLine.Width = 1;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => UpperTolLine.Height,() =>  viewModel.UpperTolLineHeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            UpperTolLine.Fill = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            CntrlPanel = new MT.Singularity.Presentation.Controls.GroupPanel(CtrlBar, LowerTolLine, TargetLine, UpperTolLine);
            CntrlPanel.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => CntrlPanel.Width,() =>  viewModel.PanelWidth,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => CntrlPanel.Height,() =>  viewModel.PanelHeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => CntrlPanel.Background,() =>  viewModel.PanelBackground,MT.Singularity.Expressions.BindingMode.OneWay,false);
            CntrlPanel.Margin = new MT.Singularity.Presentation.Thickness(0, 1, 0, 1);
            this.Content = CntrlPanel;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[12];
    }
}
