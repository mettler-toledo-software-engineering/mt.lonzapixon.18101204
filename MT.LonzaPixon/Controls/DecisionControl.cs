﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for DecisionControl
    /// </summary>
    public partial class DecisionControl
    {
        public DecisionControlViewModel viewModel;
        private Visual Parent;
        public DecisionControl(Visual Parent)
        {
            this.Parent = Parent;
            viewModel = new DecisionControlViewModel();
            InitializeComponents();
        }
    }
}
