﻿using MT.LonzaPixon.Pages;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace MT.LonzaPixon.Controls
{
    public class DecisionControlViewModel : PropertyChangedBase
    {
        public DecisionControlViewModel()
        {

        }



        #region Buttons
        // ---------------------------------------------------------
        public ICommand btnYes
        {
            get { return new DelegateCommand(DoYes); }
        }

        public void DoYes()
        {
            OnKeyPressed(new KeyPressedEventArgs(KeyType.Yes));
        }
        // ---------------------------------------------------------
        public ICommand btnCancel
        {
            get { return new DelegateCommand(DoCancel); }
        }

        public void DoCancel()
        {
            OnKeyPressed(new KeyPressedEventArgs(KeyType.Cancel));
        }

        // ---------------------------------------------------------
        public ICommand btnAdditionalManualFilling
        {
            get { return new DelegateCommand(DoAdditionalManualFilling); }
        }



        public void DoAdditionalManualFilling()
        {
            OnKeyPressed(new KeyPressedEventArgs(KeyType.ManualFill));
        }

        // ---------------------------------------------------------
        public ICommand btnReject
        {
            get { return new DelegateCommand(DoReject); }
        }



        public void DoReject()
        {
            OnKeyPressed(new KeyPressedEventArgs(KeyType.Reject));
        }

        #endregion

        #region Properties
  
        
        string messageTitle;

        public string MessageTitle
        {
            get { return messageTitle; }
            set
            {
                if (messageTitle != value)
                {
                    messageTitle = value;
                    NotifyPropertyChanged();
                }
            }
        }

        string messageSubTitle;

        public string MessageSubTitle
        {
            get { return messageSubTitle; }
            set
            {
                if (messageSubTitle != value)
                {
                    messageSubTitle = value;
                    NotifyPropertyChanged();
                }
            }
        }


        Globals.MessageType messageType;

        public Globals.MessageType MessageType
        {
            get { return messageType; }
            set
            {
                if (messageType != value)
                {
                    messageType = value;
                    AdditionalManualFillingVisibility = messageType == Globals.MessageType.UnderFilled ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        Visibility _additionalManualFillingVisibility;

        public Visibility AdditionalManualFillingVisibility
        {
            get { return _additionalManualFillingVisibility; }
            set
            {
                if (_additionalManualFillingVisibility != value)
                {
                    _additionalManualFillingVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Events

        public enum KeyType
        {
            Yes,
            ManualFill,
            Reject,
            Cancel
        }
        /// <summary>
        /// Occures when the event is raised
        /// </summary>
        public event EventHandler<KeyPressedEventArgs> KeyPressed;

        /// <summary>
        /// Raises the <see cref="KeyPressed"/> event
        /// </summary>
        /// <param name="e">An <see cref="KeyPressedEventArgs"/> that contains the event data.</param>
        protected virtual void OnKeyPressed(KeyPressedEventArgs e)
        {
            if (KeyPressed != null)
                KeyPressed(this, e);
        }

        /// <summary>
        /// Contains all information related to a KeyPressed event
        /// </summary>
        public class KeyPressedEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="KeyPressed"/> class
            /// </summary>
            /// 
            public KeyType KeyType { get; set; }
            public KeyPressedEventArgs(KeyType KeyType)
            {
                this.KeyType = KeyType;
            }
        }

        #endregion
    }
}
