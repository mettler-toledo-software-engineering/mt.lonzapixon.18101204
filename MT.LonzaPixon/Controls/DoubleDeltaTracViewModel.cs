﻿using MT.LonzaPixon.Pages;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace MT.LonzaPixon.Controls
{
    public class DoubleDeltaTracViewModel : PropertyChangedBase
    {
       
        Visual Parent;
        public DoubleDeltaTracViewModel(Visual Parent)
        {
            this.Parent = Parent;
        }
       
                
        #region Properties
        double target;

        public double Target
        {
            get { return target; }
            set
            {
                if (value != target)
                {
                    target = value;
                    TargetFormated = Globals.FormatWeightWithUnits(target);
                }
            }
        }

        string targetFormated;
        public string TargetFormated
        {
            get
            {
                return targetFormated;
            }
            set
            {
                if (value != targetFormated)
                {
                    targetFormated = value;
                    NotifyPropertyChanged();
                }
            }
        }

        double lowerTolerance;
        public double LowerTolerance
        {
            get { return lowerTolerance; }
            set
            {
                if (value != lowerTolerance)
                {
                    lowerTolerance = value;
                    LowerToleranceFormated = Globals.FormatWeightUnit_g(lowerTolerance);
                }
            }
        }

        string lowerToleranceFormated;
        public string LowerToleranceFormated
        {
            get
            {
                return lowerToleranceFormated;
            }
            set
            {
                if (value != lowerToleranceFormated)
                {
                    lowerToleranceFormated = value;
                    NotifyPropertyChanged();
                }
            }

        }
        double upperTolerance;
        public double UpperTolerance
        {
            get { return upperTolerance; }
            set
            {
                if (value != upperTolerance)
                {
                    upperTolerance = value;
                    UpperToleranceFormated = Globals.FormatWeightUnit_g(upperTolerance);
                }
            }
        }

        string upperToleranceFormated;
        public string UpperToleranceFormated
        {
            get
            {
                return upperToleranceFormated;
            }
            set
            {
                if (value != upperToleranceFormated)
                {
                    upperToleranceFormated = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

    }
}
