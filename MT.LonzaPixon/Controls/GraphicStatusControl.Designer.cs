﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class GraphicStatusControl : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private MT.Singularity.Presentation.Controls.DynamicStackPanel PanelSpinner;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.Image internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.StackPanel internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.Image internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Pumpensteuerung);
            internal4.AddTranslationAction(() => {
                internal4.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Pumpensteuerung);
            });
            internal4.FontSize = ((System.Nullable<System.Int32>)22);
            PanelSpinner = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            PanelSpinner.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            PanelSpinner.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            PanelSpinner.Width = 80;
            PanelSpinner.Height = 80;
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() => viewModel.SpeedText,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.BaseLineRight;
            internal6.FontSize = ((System.Nullable<System.Int32>)40);
            internal6.Width = 70;
            internal6.Text = "100";
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Text = "rpm";
            internal7.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleLeft;
            internal7.FontSize = ((System.Nullable<System.Int32>)22);
            internal7.Margin = new MT.Singularity.Presentation.Thickness(4, 24, 0, 0);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, PanelSpinner, internal5);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Klemmensteuerung);
            internal9.AddTranslationAction(() => {
                internal9.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Klemmensteuerung);
            });
            internal9.FontSize = ((System.Nullable<System.Int32>)22);
            internal10 = new MT.Singularity.Presentation.Controls.Image();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(0, 40, 0, 0);
            internal10.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.closed.png";
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Visibility,() => viewModel.ImageClosedVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Margin = new MT.Singularity.Presentation.Thickness(0, 40, 0, 0);
            internal11.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.open.png";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Visibility,() => viewModel.ImageOpenVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10, internal11);
            internal8.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.FuelLance);
            internal13.AddTranslationAction(() => {
                internal13.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.FuelLance);
            });
            internal13.FontSize = ((System.Nullable<System.Int32>)22);
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal14 = new MT.Singularity.Presentation.Controls.Image();
            internal14.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.RUp.png";
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Visibility,() => viewModel.ImageRUpVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.RDown.png";
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Visibility,() => viewModel.ImageRDownVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            internal12 = new MT.Singularity.Presentation.Controls.StackPanel(internal13, internal14, internal15);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal8, internal12);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(18);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4284002376u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[5];
    }
}
