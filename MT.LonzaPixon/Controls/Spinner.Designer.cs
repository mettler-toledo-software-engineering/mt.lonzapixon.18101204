﻿using MT.Singularity.Presentation.Controls;
namespace MT.LonzaPixon.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class Spinner : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.Image internal4;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Visibility,() =>  viewModel.ContentVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() =>  viewModel.Content,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.FontSize = ((System.Nullable<System.Int32>)30);
            internal3.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal4 = new MT.Singularity.Presentation.Controls.Image();
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Source,() =>  viewModel.ImageSource,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4);
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
