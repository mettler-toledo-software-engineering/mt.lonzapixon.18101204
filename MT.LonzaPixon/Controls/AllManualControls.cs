﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.LonzaPixon.Logic;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for ManualControls
    /// </summary>
    public partial class AllManualControls
    {
        public PumpControl pumpControl;
        public ClampControl clampControl;
        public FuelLanceControl fuelLanceControl;

        public AllManualControls(Visual Parent)
        {
            InitializeComponents();
            pumpControl = new PumpControl(Parent);
            PanelManualControls.Add(pumpControl);
            clampControl = new ClampControl(Parent);
            PanelManualControls.Add(clampControl);
            fuelLanceControl = new FuelLanceControl(Parent);
            PanelManualControls.Add(fuelLanceControl);
            DigitalIOLonza.InstanceLonza.StartTasterLED = true;
            DigitalIOLonza.InstanceLonza.EventStartTasterChanged += InstanceLonza_EventStartTasterChanged;
            DigitalIOLonza.InstanceLonza.EventNotTasterChanged += InstanceLonza_EventNotTasterChanged;
        }

        void InstanceLonza_EventNotTasterChanged(object sender, DigitalIOLonza.EventNotTasterChangedArgs e)
        {
            if (!e.NotTaster)
            {
                if (pumpControl.viewModel.Running)
                {
                    pumpControl.viewModel.DoCmdStart();
                    DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                    clampControl.viewModel.SwitchChecked = false;
                    clampControl.viewModel.UpdateSwitch();
                }
            }
        }


        public async Task Dispose()
        {
            DigitalIOLonza.InstanceLonza.StartTasterLED = false;
            DigitalIOLonza.InstanceLonza.EventStartTasterChanged -= InstanceLonza_EventStartTasterChanged;
            DigitalIOLonza.InstanceLonza.EventNotTasterChanged -= InstanceLonza_EventNotTasterChanged;
            await pumpControl.viewModel.Dispose();
            base.Dispose();
        }

        async void InstanceLonza_EventStartTasterChanged(object sender, DigitalIOLonza.EventStartTasterChangedArgs e)
        {
            Task.Run(async () =>
            {

                if (e.StartTaster)
                {
                    fuelLanceControl.viewModel.DoCmdRDown();
                    pumpControl.viewModel.Running = false;
                    pumpControl.viewModel.DoCmdStart();
                    if (DigitalIOLonza.InstanceLonza.NotTaster)
                    {
                        clampControl.viewModel.SwitchChecked = true;
                    }
                }
                else
                {
                    pumpControl.viewModel.Running = true;
                    pumpControl.viewModel.DoCmdStart();
                    clampControl.viewModel.SwitchChecked = false;
                }
                clampControl.viewModel.UpdateSwitch();
            });
        }
    }
}

