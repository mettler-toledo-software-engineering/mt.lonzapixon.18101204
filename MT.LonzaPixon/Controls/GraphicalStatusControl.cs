﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for GraphicalStatusControl
    /// </summary>
    public partial class GraphicStatusControl
    {

        public GraphicStatusControlViewModel viewModel;
        Visual Parent;
        public GraphicStatusControl(Visual Parent)
        {
            this.Parent = Parent;
            viewModel = new GraphicStatusControlViewModel(Parent);
            InitializeComponents();
            PanelSpinner.Add(viewModel.spinner);
            viewModel.InitParams();
        }

        public void Dispose()
        {
            viewModel.Dispose();
        }
    }
}
