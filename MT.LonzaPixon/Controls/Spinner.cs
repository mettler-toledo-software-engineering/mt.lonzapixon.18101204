﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for Spinner
    /// </summary>
    public partial class Spinner
    {
        public SpinnerViewModel viewModel;
        private Visual Parent;

        public Spinner(Visual Parent)
        {
            viewModel = new SpinnerViewModel(Parent);
            this.Parent = Parent;
            InitClass();
        }
        public Spinner(Visual Parent, string Title)
        {
            viewModel = new SpinnerViewModel(Parent, Title);
            this.Parent = Parent;
            InitClass();
        }
        private void InitClass()
        {
            InitializeComponents();
        }
        public enum Direction
        {
            left,
            right
        }
    }
}
