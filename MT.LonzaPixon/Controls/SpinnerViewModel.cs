﻿using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MT.LonzaPixon.Controls
{
    public class SpinnerViewModel : PropertyChangedBase
    {
        private Visual Parent;
        Thread drawThread;
        bool threadEnabled = false;
        int idx = 0;
        public SpinnerViewModel(Visual Parent)
        {
            this.Parent = Parent;
            Content = "";
            ContentVisibility = Visibility.Collapsed;
            InitThread();
        }

        public SpinnerViewModel(Visual Parent, string Title)
        {
            this.Parent = Parent;
            Content = Title;
            ContentVisibility = Visibility.Visible;
            InitThread();
        }
        private void InitThread()
        {
            drawThread = new Thread(ThreadDraw);
            drawThread.Start();
        }

        public void Dispose()
        {
            quit = true;
        }

        public void InitImage()
        {
            idx = 0;
            if (Direction == Spinner.Direction.right)
                ImageSource = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Spinner.RotRight_1.jpg";
            else
                ImageSource = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Spinner.RotLeft_1.jpg";
        }

        bool quit = false;
        void ThreadDraw()
        {
            while (!quit)
            {
                if (threadEnabled)
                {

                    if (Direction == Spinner.Direction.right)
                    {
                        ImageSource = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Spinner.RotRight_" + (idx + 1).ToString() + ".jpg";
                        if (++idx == 4)
                            idx = 0;
                    }
                    else
                    {
                        ImageSource = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Spinner.RotLeft_" + (idx + 1).ToString() + ".jpg";
                        if (--idx < 0)
                            idx = 3;
                    }
                }
                Thread.Sleep(200);
            }
        }


        public void StartSpin(Spinner.Direction Direction)
        {
            this.Direction = Direction;
            threadEnabled = true;
            OnSpinnerControl(new SpinnerControlEventArgs(EnumSpinnerControl.Show));
        }

        public void StopSpin()
        {
            threadEnabled = false;
            OnSpinnerControl(new SpinnerControlEventArgs(EnumSpinnerControl.Close));
        }

        /// <summary>
        /// Occures when SpinnerControl
        /// </summary>
        public event EventHandler<SpinnerControlEventArgs> SpinnerControl;

        /// <summary>
        /// Raises the <see cref="SpinnerControl"/> event
        /// </summary>
        /// <param name="e">An <see cref="SpinnerControlEventArgs"/> that contains the event data.</param>
        protected virtual void OnSpinnerControl(SpinnerControlEventArgs e)
        {
            if (SpinnerControl != null)
                SpinnerControl(this, e);
        }

        /// <summary>
        /// Contains all information related to a SpinnerControl event
        /// </summary>
        public class SpinnerControlEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="SpinnerControl"/> class
            /// </summary>
            /// 
            public EnumSpinnerControl EnumSpinnerControl { get; set; }
            public SpinnerControlEventArgs(EnumSpinnerControl EnumSpinnerControl)
            {
                this.EnumSpinnerControl = EnumSpinnerControl;
            }
        }
        public enum EnumSpinnerControl
        {
            Show,
            Close
        }


        #region Properties

        Spinner.Direction direction;

        public Spinner.Direction Direction
        {
            get { return direction; }
            set { direction = value; }
        }


        private Visibility contentVisibility = Visibility.Collapsed;

        public Visibility ContentVisibility
        {
            get { return contentVisibility; }
            set
            {
                if (contentVisibility != value)
                {
                    contentVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Object imageSource = null;

        public Object ImageSource
        {
            get { return imageSource; }
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string content = "";

        public string Content
        {
            get { return content; }
            set
            {
                if (content != value)
                {
                    content = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
    }

}
