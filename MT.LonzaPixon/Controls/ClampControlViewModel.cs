﻿using MT.LonzaPixon.Logic;
using MT.LonzaPixon.Pages;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace MT.LonzaPixon.Controls
{
    public class ClampControlViewModel : PropertyChangedBase
    {

        private Visual Parent;
        private Switch sw;

        public ClampControlViewModel(Visual Parent)
        {
            this.Parent = Parent;
        }

        public void InitVisuals(Switch sw)
        {
            this.sw = sw;
            sw.Click += sw_Click;
            SwitchChecked = false;
            UpdateSwitch();
        }

        void sw_Click(object sender, Singularity.Presentation.Input.HandledEventArgs e)
        {
            UpdateSwitch();
        }

        public void UpdateSwitch()
        {
            if (SwitchChecked)
            {
                ImageOpenVisibility = Visibility.Visible;
                ImageClosedVisibility = Visibility.Collapsed;
                DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeAuf;
            }
            else
            {
                ImageOpenVisibility = Visibility.Collapsed;
                ImageClosedVisibility = Visibility.Visible;
                DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;

            }
        }



        #region Properties



        Visibility imageOpenVisibility = Visibility.Collapsed;

        public Visibility ImageOpenVisibility
        {
            get { return imageOpenVisibility; }
            set
            {
                if (value != imageOpenVisibility)
                {
                    imageOpenVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Visibility imageClosedVisibility = Visibility.Visible;

        public Visibility ImageClosedVisibility
        {
            get { return imageClosedVisibility; }
            set
            {
                if (value != imageClosedVisibility)
                {
                    imageClosedVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }


        bool switchChecked = true;

        public bool SwitchChecked
        {
            get { return switchChecked; }
            set
            {
                if (value != switchChecked)
                {
                    switchChecked = value;
                    NotifyPropertyChanged();
                }
            }
        }
       



        #endregion

    }
}
