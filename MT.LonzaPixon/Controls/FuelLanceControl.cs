﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Data;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for FuelLanceControl
    /// </summary>
    public partial class FuelLanceControl 
    {
        private Visual Parent;
        public FuelLanceControlViewModel viewModel;
        public FuelLanceControl(Visual Parent)
        {
            viewModel = new FuelLanceControlViewModel(Parent);
            this.Parent = Parent;
            InitializeComponents();
            viewModel.InitControl();
        }
    }
}

