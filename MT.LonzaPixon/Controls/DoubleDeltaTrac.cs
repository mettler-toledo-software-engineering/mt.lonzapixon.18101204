﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Platform.Devices.Scale;
using MT.LonzaPixon.Pages;

namespace MT.LonzaPixon.Controls
{
    /// <summary>
    /// Interaction logic for DoubleDeltaTrac
    /// </summary>
    public partial class DoubleDeltaTrac
    {
        public DoubleDeltaTracViewModel viewModel;
        DeltaTrac Dt, DtFine;
        public DoubleDeltaTrac(Visual Parent, int Width, int Height, double TargetValue, double UpperToleranceAbs, double LowerToleranceAbs)
        {
            viewModel = new DoubleDeltaTracViewModel(Parent);
            InitializeComponents();
            Dt = new DeltaTrac(Parent, Width, Height / 2, 0, TargetValue, 80, UpperToleranceAbs, LowerToleranceAbs);
            DtFine = new DeltaTrac(Parent, Width, Height / 2, TargetValue - 7 * LowerToleranceAbs, TargetValue, 80, UpperToleranceAbs, LowerToleranceAbs);
            Dt.viewModel.PanelBackground = new SolidColorBrush(new Color(0xFFE0E0E0));
            DtFine.viewModel.PanelBackground = new SolidColorBrush(new Color(0xFFE0E0E0));

            PanelDoubleDeltaTrac.Add(DtFine);
            PanelDoubleDeltaTrac.Add(Dt);
            viewModel.Target = TargetValue;
            viewModel.UpperTolerance = UpperToleranceAbs;
            viewModel.LowerTolerance = LowerToleranceAbs;
            Activate();

        }

        public void UpdateTarget()
        {
            var x = viewModel.Target;
            viewModel.Target = double.MinValue;
            viewModel.Target = x;
        }

        IScaleService _scaleService { get { return HomeScreen.Instance.ViewModel.ScaleService; } }
        public void Activate()
        {
            _scaleService.SelectedScale.NewWeightInDisplayUnit += SelectedScale_NewWeightInDisplayUnit;
        }

        void SelectedScale_NewWeightInDisplayUnit(WeightInformation weight)
        {
            double convertedNet = WeightUnits.Convert(weight.NetWeight, weight.Unit, WellknownWeightUnit.Kilogram);
            Dt.viewModel.Value = convertedNet;

            DtFine.viewModel.Value = convertedNet;
        }

        public void Dispose()
        {
            _scaleService.SelectedScale.NewWeightInDisplayUnit -= SelectedScale_NewWeightInDisplayUnit;
        }

    }
}
