﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform;

namespace MT.LonzaPixon.Logic
{
    public class Printer
    {
        private string _iPAddress;
        private int _port;
        private System.Net.Sockets.Socket prnChannel { get; set; }
        private string templateData = "";
        private string lastData = "";
        private IPAddress _hostIP;
        private IPEndPoint _remoteEP;

        private string LonzaPixonTemplateName;

        public Printer(string iPAddress, int port)
        {
            SingularityEnvironment env = new SingularityEnvironment("MT.LonzaPixon");
            var dataPath = env.DataDirectory;
            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }
            LonzaPixonTemplateName = Path.Combine(dataPath, "LonzaPixonTemplate.prn");

            _iPAddress = iPAddress;
            _port = port;
            _hostIP = IPAddress.Parse(_iPAddress);
            _remoteEP = new IPEndPoint(_hostIP, _port);
        }

        private bool ReadTemplate()
        {
            if (File.Exists(LonzaPixonTemplateName))
            {
                using (var sr = new StreamReader(LonzaPixonTemplateName, System.Text.Encoding.Default)) // Encoding.Unicode from WebService XML, use normally Encoding.Default
                {
                    // read ZPL template
                    templateData = sr.ReadToEnd();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private void FillTemplate(bool totalLabel)
        {
            if (!totalLabel)
            {
                templateData = templateData.Replace("$NettoLbl$", "Netto");
                templateData = templateData.Replace("$NettoVal$", Globals.FormatWeightWithUnits(DosingParams.Instance.IstWert));
                templateData = templateData.Replace("$FlaschenLbl$", "Flaschen-Nr");
                templateData = templateData.Replace("$GebNrVal$", DosingParams.Instance.GebindeNr.ToString());
            }
            else
            {
                templateData = templateData.Replace("$NettoLbl$", "Total Netto");
                templateData = templateData.Replace("$NettoVal$", Globals.FormatWeightWithUnits(DosingParams.Instance.NettoSum));
                templateData = templateData.Replace("$FlaschenLbl$", "Total Flaschen");
                templateData = templateData.Replace("$GebNrVal$", DosingParams.Instance.GebindeNr.ToString());
            }
        }

        private void FillManualTemplate(double newWeight)
        {
            templateData = templateData.Replace("$NettoLbl$", "Netto");
            templateData = templateData.Replace("$NettoVal$", Globals.FormatWeightWithUnits(newWeight));
            templateData = templateData.Replace("$FlaschenLbl$", "Manuell gefuellt");
            templateData = templateData.Replace("$GebNrVal$", "");
        }

        public async Task<bool> PrintLabel()
        {
            if (!ReadTemplate())
            {
                return false;
            }

            FillTemplate(false);
            lastData = templateData;
            return await Reprint();
        }

        public async Task<bool> PrintManualLabel(double netWeight)
        {
            if (!ReadTemplate())
            {
                return false;
            }

            FillManualTemplate(netWeight);
            lastData = templateData;
            return await Reprint();
        }

        public async Task<bool> PrintTotalLabel()
        {
            if (!ReadTemplate())
            {
                return false;
            }

            FillTemplate(true);
            lastData = templateData;
            return await Reprint();
        }

        public async Task<bool> Reprint()
        {
            if (!String.IsNullOrEmpty(lastData))
            {
                IAsyncResult result;
                try
                {
                    prnChannel = new System.Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    result = prnChannel.BeginConnect(_remoteEP, null, null);
                    bool Success = result.AsyncWaitHandle.WaitOne(2500, false);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Error open printer channel:\n" + ex.Message);
                    return false;
                }

                if (!prnChannel.Connected)
                {
                    // ToDo Handle connection error
                    return false;
                }
                prnChannel.EndConnect(result);

                byte[] msg = Encoding.ASCII.GetBytes(lastData);
                prnChannel.Send(msg);

                prnChannel.Close();
                string formattedLastData = lastData.Replace("\r\n", "<cr><lf>");
                Log4NetManager.ApplicationLogger.Info("Label printed: " + formattedLastData);

                return true;
            }
            return false;
        }

        public async Task OpenPrintChannel()
        {

        }

    }
}
