﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MT.LonzaPixon.Pages;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Serialization;

namespace MT.LonzaPixon.Logic
{
    public class FlexiconPump
    {
        // const definition for emergency handling of pump
        public const string NotAusMarker = "NotAus";

        // instance of this class (not ensured)
        public static FlexiconPump Instance;

        // pump number. Must correspondent with Flexicon pump number (setup via menu on Flexicon pump)
        private const string _pumpNr = "1";

        // maximum pump speed in RPM
        public const double _maxPumpSpeed = 300.0;

        // timeout in [s] program is waiting for next status request response
        private int _pumpTimeout = 2;

        private StringSerializer _stringSerializer;

        public ChannelEventEmitter<string> EventEmitter;

        private FlexiconParams _flexiconParams;
        public FlexiconParams FlexiconParams
        {
            get { return _flexiconParams; }
        }

        private bool _online = false;

        public FlexiconPump()
        {
            bool result = false;
            Task.Run(async () =>
            {
                result = await Initialize();
            });
        }

        private async Task<bool> Initialize()
        {
            _flexiconParams = new FlexiconParams(_pumpNr);

            // init serial channel for Flexicon pump
            if (HomeScreen.Instance.ViewModel.PumpConnectionChannel != null)
            {
                // default line end for Flexicon pump: <CR>
                var delimiterSerializer = new DelimiterSerializer(HomeScreen.Instance.ViewModel.PumpConnectionChannel, CommonDataSegments.Cr);
                _stringSerializer = new StringSerializer(delimiterSerializer);
                if (!await OpenSerialAsync())
                {
                    MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.ErrorOpenSerial), Localization.Get(Localization.Key.Error), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                OnEventOnlineChanged(new EventOnlineChangedArgs(_online));
                EventEmitter = new ChannelEventEmitter<string>(_stringSerializer);
                EventEmitter.MessageRead += eventEmitter_MessageRead;
            }
            Thread pumpThread = new Thread(pumpTask);
            pumpThread.Start();

            return true;
        }

        public void eventEmitter_MessageRead(object sender, MessageReadEventArgs<string> e)
        {
            _flexiconParams.RSCommandResponse(e.Message);
        }

        private Boolean _watchDogRunning = false;
        private Boolean _watchDogStopped = false;
        private object _lockPumpConnection = new object();
        private bool _pumpLocked = false;
        private Task wdog;

        public bool IsOpen = false;

        public async Task<bool> OpenSerialAsync()
        {
            Debug.WriteLine("Open serial channel");
            if (_stringSerializer != null)
            {
                try
                {
                    IsOpen = false;
                    Log4NetManager.ApplicationLogger.Debug("Open serial port");
                    await _stringSerializer.OpenAsync();
                    IsOpen = true;
                    //WatchDogStart();
                    return true;
                }
                catch (Exception ex)
                {
                    Log4NetManager.ApplicationLogger.Error("Error opening serial port", ex);
                    Task.Run(async () => { await _stringSerializer.CloseAsync(); return false; });
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool CloseSerial()
        {
            Debug.WriteLine("Close serial channel");
            if (_stringSerializer != null)
            {
                try
                {
                    //await WatchDogStop();

                    Log4NetManager.ApplicationLogger.Debug("Closing serial port");
                    _stringSerializer.Close();

                    IsOpen = false;
                    return true;
                }
                catch (Exception ex)
                {
                    Log4NetManager.ApplicationLogger.Error("Error closing serial port", ex);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> CloseSerialAsync()
        {
            Debug.WriteLine("Close serial channel async");
            if (_stringSerializer != null)
            {
                try
                {
                    //await WatchDogStop();

                    Log4NetManager.ApplicationLogger.Debug("Closing serial port async");
                    await _stringSerializer.CloseAsync();

                    IsOpen = false;
                    return true;
                }
                catch (Exception ex)
                {
                    Log4NetManager.ApplicationLogger.Error("Error closing serial port async", ex);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private List<String> _pumpStack = new List<string>();
        private object _pumpLock = new object();

        private void AddToPumpStack(string cmd)
        {
            lock (_pumpLock)
            {
                _pumpStack.Add(cmd);
                Log4NetManager.ApplicationLogger.Debug("AddToPumpStack: "+cmd);
            }
            Debug.WriteLine("+++++> cmd added: " + cmd + " (" + _pumpStack.Count + ")");
        }

        public bool Running = true;

        private bool _openErrorMsg = false;

        private void pumpTask()
        {
            while (Running)
            {
                while (_pumpStack.Count > 0)
                {
                    String cmd = "";
                    cmd = _pumpStack[0];
                    lock (_pumpLock)
                    {
                        _pumpStack.RemoveAt(0);
                    }
                    if (IsOpen)
                    {
                        _stringSerializer.Write(_pumpNr + cmd);
                        Log4NetManager.ApplicationLogger.Debug("PumpCmd: " + cmd);
                        Debug.WriteLine("=====> sync pump cmd: " + cmd +
                                        (_pumpStack.Count > 0 ? " (" + _pumpStack.Count + " cmds waiting)" : ""));
                        _stringSerializer.Write(_pumpNr + "RS");
                        if (!WaitForNextResponse(_flexiconParams.RSResponseNr))
                        {
                            Log4NetManager.ApplicationLogger.Debug("Pump timeout");
                        }
                        else
                        {
                            Debug.WriteLine("=====> sync pump okay");
                            OnPumpStatusChanged(new PumpStatusChangedEventArgs(_flexiconParams));
                        }
                    }
                    else
                    {
                        Debug.WriteLine("Serial channel not opened");
                        if (!_openErrorMsg)
                        {
                            _openErrorMsg = true;
                            MessageBox.Show(HomeScreen.Instance.NavigationFrame, "Serial channel not opened", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
                Thread.Sleep(10);
            }
        }

        public bool WaitForNextResponse(int actRSResponseNr)
        {

            DateTime timeoutTime = DateTime.Now.AddSeconds(_pumpTimeout);

            while (actRSResponseNr == _flexiconParams.RSResponseNr)
            {
                if (DateTime.Now > timeoutTime)
                {
                    return false;
                }
                Thread.Sleep(100);
            }
            return true;
        }

        private string _nextPumpSpeed = "";
        public string NextPumpSpeed
        {
            set
            {
                if (_nextPumpSpeed != value)
                {
                    _nextPumpSpeed = value;
                    Log4NetManager.ApplicationLogger.Debug("Pump speed " + value.ToString());
                    if (value != NotAusMarker)
                        AddToPumpStack(value);
                }
            }
        }

        public void PumpSpeed(double rpm)
        {
            NextPumpSpeed = "SP" + rpm.ToString("0.0");
        }

        private string _nextPumpOnOff = "";
        public string NextPumpOnOff
        {
            set
            {
                if (_nextPumpOnOff != value)
                {
                    _nextPumpOnOff = value;
                    Log4NetManager.ApplicationLogger.Debug("Pump on/off " + value);
                    if (value != NotAusMarker)
                        AddToPumpStack(value);
                }
            }
        }

        public void PumpOn(bool openKlemme)
        {
            if (DigitalIOLonza.InstanceLonza.NotTaster)
            {
                if (openKlemme)
                    DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeAuf;
                NextPumpOnOff = "GO";
            }
            else
            {
                Log4NetManager.ApplicationLogger.Warn("Emergency swith activated, pump not started");
            }
        }

        public void PumpOff(bool closeKlemme)
        {
            NextPumpOnOff = "ST";
            if (closeKlemme)
                DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
        }

        private string _nextPumpDirecttion = "";
        public string NextPumpDirection
        {
            set
            {
                if (_nextPumpDirecttion != value)
                {
                    _nextPumpDirecttion = value;
                    Log4NetManager.ApplicationLogger.Debug("Pump direction " + value);
                    if (value != NotAusMarker)
                        AddToPumpStack(value);
                }
            }
        }

        public void PumpDirection(PumpDirectionEnum direction)
        {
            NextPumpDirection = "R" + (direction == PumpDirectionEnum.Right_Clockwise ? "R" : "L");
        }

        public event EventHandler<EventOnlineChangedArgs> EventOnlineChanged;
        protected virtual void OnEventOnlineChanged(EventOnlineChangedArgs e)
        {
            if (EventOnlineChanged != null)
            {
                Debug.WriteLine("Pump online: " + _online.ToString());
                EventOnlineChanged(this, e);
            }
        }

        public class EventOnlineChangedArgs : EventArgs
        {
            public bool Online { get; set; }

            public EventOnlineChangedArgs(bool online)
            {
                Online = online;
            }
        }
        /// <summary>
        /// Occures when PumpStatusChanged
        /// </summary>
        public event EventHandler<PumpStatusChangedEventArgs> PumpStatusChanged;

        /// <summary>
        /// Raises the <see cref="PumpStatusChanged"/> event
        /// </summary>
        /// <param name="e">An <see cref="PumpStatusChangedEventArgs"/> that contains the event data.</param>
        protected virtual void OnPumpStatusChanged(PumpStatusChangedEventArgs e)
        {
            if (PumpStatusChanged != null)
                PumpStatusChanged(this, e);
        }

        /// <summary>
        /// Contains all information related to a PumpStatusChanged event
        /// </summary>
        public class PumpStatusChangedEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PumpStatusChanged"/> class
            /// </summary>
            /// 
            public FlexiconParams FlexiconParams { get; set; }
            public PumpStatusChangedEventArgs(FlexiconParams FlexiconParams)
            {
                this.FlexiconParams = FlexiconParams;
            }
        }


    }

    public class FlexiconParams
    {
        public FlexiconParams(string pumpNr)
        {
            _pumpNr = pumpNr;
        }

        private string _pumpNr = "";
        public string PumpNr
        {
            get { return _pumpNr; }
            set { _pumpNr = value; }
        }
        private string _pumpType = "";
        public string PumpType
        {
            get { return _pumpType; }
            set { _pumpType = value; }
        }
        private double _mlPerRound = 0;
        public double MlPerRound
        {
            get { return _mlPerRound; }
            set { _mlPerRound = value; }
        }
        private string _pumpHead = "";
        public string PumpHead
        {
            get { return _pumpHead; }
            set { _pumpHead = value; }
        }
        private string _pipeSize = "";
        public string PipeSize
        {
            get { return _pipeSize; }
            set { _pipeSize = value; }
        }
        private double _rPM = 0;
        public double RPM
        {
            get { return _rPM; }
            set { _rPM = value; }
        }
        private PumpDirectionEnum _pumpDirection = PumpDirectionEnum.Right_Clockwise;
        public PumpDirectionEnum PumpDirection
        {
            get { return _pumpDirection; }
            set { _pumpDirection = value; }
        }
        private double _tachoCount = 0;
        public double TachoCount
        {
            get { return _tachoCount; }
            set { _tachoCount = value; }
        }
        private bool _pumpRunning = false;
        public bool PumpRunning
        {
            get { return _pumpRunning; }
            set { _pumpRunning = value; }
        }

        private int _rSResponseNr = 0;
        public int RSResponseNr
        {
            get { return _rSResponseNr; }
            set { _rSResponseNr = value; }
        }

        public void RSCommandResponse(string rSResponse)
        {
            if (String.IsNullOrEmpty(rSResponse))
                return;
            try
            {
                if (rSResponse.Length > 1)
                {
                    // response to 1RS-Command
                    //   "520DI 1.00 520R 0.0MM  60.0 CW P/N 1 608581 0 !"<CR>
                    //
                    //    Pumpentyp       520DI       idx  0
                    //    ml/Umin-1       1.00        idx  1
                    //    Pumpenkopf      520R        idx  2
                    //    Schlauchgrösse  0.0MM       idx  3
                    //    Drehzahl        60.0        idx  4
                    //    Drehrichtung    CW          idx  5         (CW and CCW)
                    //    Pumpen-Nr       P/N 1       idx  6 idx 7
                    //    Tachozählung    608581      idx  8
                    //    läuft           1           idx  9
                    //    End             !           idx 10
                    string res = rSResponse;
                    if (res.Contains('!'))
                    {
                        while (res.Contains("  "))
                            res = res.Replace("  ", " ");
                        string[] resarray = res.Split(' ');
                        if (resarray.Length >= 10)
                        {
                            double d;
                            PumpType = resarray[0];
                            if (Globals.Double_TryParse(resarray[1], out d))
                                _mlPerRound = d;
                            PumpHead = resarray[2];
                            PipeSize = resarray[3];
                            if (Globals.Double_TryParse(resarray[4], out d))
                                RPM = d;
                            _pumpDirection = (resarray[5] == "CW")
                                ? PumpDirectionEnum.Right_Clockwise
                                : PumpDirectionEnum.Left_Counterclockwise;
                            if (Globals.Double_TryParse(resarray[8], out d))
                                TachoCount = d;
                            PumpRunning = resarray[9] == "1";
                        }
                    }
                }
                if (RSResponseNr < 1000)
                    RSResponseNr++;
                else
                    RSResponseNr = 1;

                Debug.WriteLine("Pump response : \"" + rSResponse + "\" " + RSResponseNr);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error RsCommandResponse", ex);
            }
        }


    }

    public enum PumpDirectionEnum
    {
        Right_Clockwise,
        Left_Counterclockwise
    }



}
