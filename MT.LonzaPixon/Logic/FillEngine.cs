﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MT.LonzaPixon.Config;
using MT.LonzaPixon.Pages;
using MT.Singularity;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System.Collections.Generic;
using MT.LonzaPixon.Controls;
using MT.Singularity.Logging;
using Timer = MT.Singularity.Presentation.Utils.Timer;
using MT.Singularity.Platform.Memories;

namespace MT.LonzaPixon.Logic
{
    public class FillEngine : PropertyChangedBase
    {
        public static FillEngine Instance;

        private DigitalIOLonza _digIoLonza { get { return DigitalIOLonza.InstanceLonza; } }
        private FlexiconPump _flexiconPump { get { return FlexiconPump.Instance; } }
        private IScaleService _scaleService { get { return HomeScreen.Instance.ViewModel.ScaleService; } }
        private LonzaPixonConfiguration _configuration { get { return HomeScreen.Instance.ViewModel.Configuration; } }
        private WeightFillPageViewModel _viewModel;
        private Printer _printer;
        private Timer threadTimer;


        public WeightFillPageViewModel ViewModel { get; set; }
        public DecisionControl decisionControl { get; set; }

        public FillEngine()
        {

            bool res = false;
            Task.Run(async () =>
            {
                res = await Initialize();
            });

            // render button and LED state independent of current status
            NotifyPropertyChanged("StartEnabled");

            threadTimer = new Timer();
            threadTimer.Interval = 100;
            threadTimer.Tick += threadTimer_Tick;
        }

        void threadTimer_Tick(object sender, EventArgs e)
        {
            FillThread();
        }

        public async Task<bool> Initialize()
        {
            if (_digIoLonza != null)
            {
                _digIoLonza.EventNotTasterChanged += _digIoLonza_EventNotTasterChanged;
                _digIoLonza.EventStartTasterChanged += _digIoLonza_EventStartTasterChanged;
            }

            return true;
        }

        void _digIoLonza_EventStartTasterChanged(object sender, DigitalIOLonza.EventStartTasterChangedArgs e)
        {
            if (e.StartTaster && StartEnabled)
            {
                _startTasterActivated = true;
            }
        }

        void _digIoLonza_EventNotTasterChanged(object sender, DigitalIOLonza.EventNotTasterChangedArgs e)
        {
            if (!e.NotTaster)
            {
                //#if DEBUG
                //                MessageBox.Show(HomeScreen.Instance.NavigationFrame, Localization.Get(Localization.Key.EmergencySwitchActivated), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //#else
                NotTasteActivated = true;
                Pause = true;
                btnBackVisibility = Visibility.Visible;
                SetPumpEmergencyOff();
                SetStatusError(Localization.Get(Localization.Key.EmergencySwitchActivated));
                MessageBox.Show(HomeScreen.Instance.NavigationFrame,
                    Localization.Get(Localization.Key.EmergencySwitchActivated), "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                //#endif
            }
        }



        MT.LonzaPixon.Globals.MessageType messageType;

        void ViewDecisionPanel(MT.LonzaPixon.Globals.MessageType messageType, string Title, string SubTitle)
        {
            this.messageType = messageType;
            decisionControl.viewModel.MessageType = messageType;
            SaveMenu();
            SetupMenuButtons(null);

            decisionControl.viewModel.MessageTitle = Title;
            decisionControl.viewModel.MessageSubTitle = SubTitle;
            GraphicStatusVisibility = Visibility.Collapsed;
            PanelDecisionVisibility = Visibility.Visible;
            decisionControl.viewModel.KeyPressed += viewModel_KeyPressed;

        }

        void HideDecisionPanel()
        {
            decisionControl.viewModel.KeyPressed -= viewModel_KeyPressed;
            PanelDecisionVisibility = Visibility.Collapsed;
            GraphicStatusVisibility = Visibility.Visible;
        }


        void viewModel_KeyPressed(object sender, DecisionControlViewModel.KeyPressedEventArgs e)
        {
            switch (e.KeyType)
            {
                case DecisionControlViewModel.KeyType.Yes:
                    switch (messageType)
                    {
                        case Globals.MessageType.OverFilled:
                        case Globals.MessageType.Cancel:
                            SetStatusText(Localization.Get(Localization.Key.OverfillAccepted));
                            HideDecisionPanel();
                            SetupMenuButtons(null);
                            FillState = FillingState.RetrieveStableWeight;
                            break;
                        case Globals.MessageType.UnderFilled:
                            SetStatusText(Localization.Get(Localization.Key.UnderfillAccepted));
                            HideDecisionPanel();
                            SetupMenuButtons(null);
                            FillState = FillingState.RetrieveStableWeight;
                            break;
                    }
                    break;
                case DecisionControlViewModel.KeyType.ManualFill:
                    switch (messageType)
                    {
                        case Globals.MessageType.UnderFilled:
                            Pause = true;
                            SetStatusText(Localization.Get(Localization.Key.UnderfillNotAccepted));
                            HideDecisionPanel();
                            DoManual();
                            break;
                        case Globals.MessageType.OverFilled:
                            Pause = true;
                            SetStatusText(Localization.Get(Localization.Key.OverfillNotAccepted));
                            HideDecisionPanel();
                            DoManual();
                            break;
                        case Globals.MessageType.Cancel:
                            break;
                    }
                    break;
                case DecisionControlViewModel.KeyType.Reject:
                    DoReject();
                    break;

                case DecisionControlViewModel.KeyType.Cancel:
                    HideDecisionPanel();
                    RestoreMenu();
                    break;
                default:
                    break;
            }
        }



        #region Buttons


        // ---------------------------------------------------------
        public ICommand btnReject
        {
            get { return new DelegateCommand(DoReject); }
        }

        public async void DoReject()
        {
            MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.BottleDiscarded1) + "\n" + Localization.Get(Localization.Key.BottleDiscarded2), "Warnung", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, new Action<DialogResult>(answer =>
           {
               if (answer == DialogResult.Yes)
               {
                   SetPumpOff();
                   _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                   FuellkopfHoch();
                   FillState = FillingState.WaitForEmptyBalance;
                   HideDecisionPanel();
                   DosingParams.Instance.GebindeNr--;
                   Tare = 0;
               }
           }));
        }
        // ---------------------------------------------------------
        public ICommand btnStart
        {
            get { return new DelegateCommand(DoStart); }
        }

        public void DoStart()
        {
            _startTasterActivated = true;
        }
        // ---------------------------------------------------------
        public ICommand btnCancel
        {
            get { return new DelegateCommand(DoCancel); }
        }

        public void DoCancel()
        {
            ViewDecisionPanel(MT.LonzaPixon.Globals.MessageType.Cancel, "Abbruch der Dosierung", "Toleranzverletzung akzepieren ?");
            FillState = FillingState.DoNothing;
        }
        // ---------------------------------------------------------

        public ICommand btnPause
        {
            get { return new DelegateCommand(DoPause); }
        }

        public void DoPause()
        {
            _pause = false;
            _startTasterActivated = true;
        }

        // ---------------------------------------------------------
        public ICommand btnManual
        {
            get { return new DelegateCommand(DoManual); }
        }

        string LastStatusText;
        Color LastStatusColor;
        public void DoManual()
        {
            DigitalIOLonza.InstanceLonza.PushStartTasterLED();
            LastStatusText = HomeScreen.Instance.ViewModel.StatusText;
            LastStatusColor = HomeScreen.Instance.ViewModel.StatusTextColor;
            SaveMenu();
            StartEnabled = false;
            SetupMenuButtons(new cButtons[] { cButtons.btnEndManual, cButtons.btnPrintManual });
            GraphicStatusVisibility = Visibility.Collapsed;
            HomeScreen.Instance.ViewModel.BalanceButtonsEnabled = true;
            OnManualFunctionRequest(new ManualFunctionRequestEventArgs(true));
        }
        // ---------------------------------------------------------
        public ICommand btnEndManual
        {
            get { return new DelegateCommand(DoEndManual); }
        }

        public async void DoEndManual()
        {
            //binkert

            HomeScreen.Instance.ViewModel.BalanceButtonsEnabled = false;
            OnManualFunctionRequest(new ManualFunctionRequestEventArgs(false));
            GraphicStatusVisibility = Visibility.Visible;
            RestoreMenu();
            HomeScreen.Instance.ViewModel.StatusText = LastStatusText;
            HomeScreen.Instance.ViewModel.StatusTextColor = LastStatusColor;
            StartEnabled = true;
            if (NextStepDosing)
            {
                _startTasterActivated = true;
            }

        }


        // ---------------------------------------------------------
        public ICommand btnResume
        {
            get { return new DelegateCommand(DoResume); }
        }

        public async void DoResume()
        {
            if (!DigitalIOLonza.InstanceLonza.NotTaster)
            {
                MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.EmergencySwitchActivated),
                    Localization.Get(Localization.Key.Warning), MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (NotTasteActivated)
                {
                    MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.AskNotTaster),
                        Localization.Get(Localization.Key.Question), MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        new Action<DialogResult>(
                            answer =>
                            {
                                switch (answer)
                                {
                                    case DialogResult.Yes:
                                        {
                                            Pause = false;
                                            NotTasteActivated = false;
                                            _startTasterActivated = true;
                                            break;
                                        }
                                    case DialogResult.No:
                                        {
                                            break;
                                        }
                                }
                            }));
                }
                else
                {
                    _startTasterActivated = true;
                }
            }
        }

        // ---------------------------------------------------------
        public ICommand btnEndSeries
        {
            get { return new DelegateCommand(DoEndSeries); }
        }

        public async void DoEndSeries()
        {
            MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.AskEndSerie1) + "\n" + Localization.Get(Localization.Key.AskEndSerie2), Localization.Get(Localization.Key.Question), MessageBoxButtons.YesNo, MessageBoxIcon.Question, new Action<DialogResult>(answer =>
            {
                switch (answer)
                {
                    case DialogResult.Yes:
                        {
                            SetStatusText(Localization.Get(Localization.Key.PrintTotalLabel));
                            _printer = new Printer(_configuration.PrinterIP, _configuration.PrinterPort);
                            Task.Run(async () =>
                            {
                                if (!await _printer.PrintTotalLabel())
                                {
                                    Pause = true;
                                    SetStatusError(Localization.Get(Localization.Key.ErrorPrintTotalLabel));
                                    MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.AskReprint1) + "\n" + Localization.Get(Localization.Key.AskReprint2), Localization.Get(Localization.Key.Error),
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Error,
                                        new Action<DialogResult>(reprint =>
                                        {
                                            switch (reprint)
                                            {
                                                case DialogResult.Yes:
                                                    {
                                                        Pause = false;
                                                        break;
                                                    }
                                                case DialogResult.No:
                                                    {
                                                        Pause = false;
                                                        FillState = FillingState.EndSerie;
                                                        break;
                                                    }
                                            }

                                        }));

                                    Debug.WriteLine("Print error");
                                }
                                else
                                {
                                    _klemmenstatusBeforePause = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                                    Pause = false;
                                    FillState = FillingState.EndSerie;
                                }
                            });
                            break;
                        }
                    //case DialogResult.No:
                    //    {
                    //        Pause = false;
                    //        FillState = FillingState.EndSerie;
                    //        break;
                    //    }
                }

            }));
        }

        #endregion

        #region Properties



        private bool _startTasterActivated = false;
        public bool NotTasteActivated = false;

        private bool _startEnabled = false;
        public bool StartEnabled
        {
            get { return _startEnabled; }
            set
            {

                if (_startEnabled != value)
                {
                    _startEnabled = value;
                }
                _digIoLonza.StartTasterLED = value;
            }
        }

        Visibility _additionalManualFillingVisibility;

        public Visibility AdditionalManualFillingVisibility
        {
            get { return _additionalManualFillingVisibility; }
            set
            {
                if (_additionalManualFillingVisibility != value)
                {
                    _additionalManualFillingVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility panelDecisionVisibility = Visibility.Collapsed;
        public Visibility PanelDecisionVisibility
        {
            get { return panelDecisionVisibility; }
            set
            {
                if (panelDecisionVisibility != value)
                {
                    panelDecisionVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility graphicStatusVisibility;

        public Visibility GraphicStatusVisibility
        {
            get { return graphicStatusVisibility; }
            set
            {
                if (graphicStatusVisibility != value)
                {
                    graphicStatusVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private Visibility deltaTracVisibility;

        public Visibility DeltaTracVisibility
        {
            get { return deltaTracVisibility; }
            set
            {
                if (deltaTracVisibility != value)
                {
                    deltaTracVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private Visibility _btnStartVisibility;
        public Visibility btnStartVisibility
        {
            get { return _btnStartVisibility; }
            set
            {


                if (_btnStartVisibility != value)
                {
                    Debug.WriteLine("==========> Start    : " + value.ToString());
                    _btnStartVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnStopVisibility;
        public Visibility btnStopVisibility
        {
            get { return _btnStopVisibility; }
            set
            {


                if (_btnStopVisibility != value)
                {
                    _btnStopVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _btnPauseVisibility;
        public Visibility btnPauseVisibility
        {
            get { return _btnPauseVisibility; }
            set
            {

                if (_btnPauseVisibility != value)
                {
                    Debug.WriteLine("==========> Pause    : " + value.ToString());

                    _btnPauseVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnResumeVisibility;
        public Visibility btnResumeVisibility
        {
            get { return _btnResumeVisibility; }
            set
            {


                if (_btnResumeVisibility != value)
                {
                    Debug.WriteLine("==========> Resume   : " + value.ToString());
                    _btnResumeVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnEndSerieVisibility;
        public Visibility btnEndSerieVisibility
        {
            get { return _btnEndSerieVisibility; }
            set
            {


                if (_btnEndSerieVisibility != value)
                {
                    Debug.WriteLine("==========> End Serie: " + value.ToString());
                    _btnEndSerieVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnManualVisibility;
        public Visibility btnManualVisibility
        {
            get { return _btnManualVisibility; }
            set
            {


                if (_btnManualVisibility != value)
                {
                    Debug.WriteLine("==========> EndManual: " + value.ToString());
                    _btnManualVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _btnEndManualVisibility;
        public Visibility btnEndManualVisibility
        {
            get { return _btnEndManualVisibility; }
            set
            {

                if (_btnEndManualVisibility != value)
                {
                    Debug.WriteLine("==========> EndManual: " + value.ToString());
                    _btnEndManualVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnRejectVisibility;
        public Visibility btnRejectVisibility
        {
            get { return _btnRejectVisibility; }
            set
            {
                if (_btnRejectVisibility != value)
                {
                    _btnRejectVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _btnBackVisibility;
        public Visibility btnBackVisibility
        {
            get { return _btnBackVisibility; }
            set
            {


                if (_btnBackVisibility != value)
                {
                    Debug.WriteLine("==========> Back     : " + value.ToString());
                    _btnBackVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnContinueVisibility;
        public Visibility btnContinueVisibility
        {
            get { return _btnContinueVisibility; }
            set
            {


                if (_btnContinueVisibility != value)
                {
                    Debug.WriteLine("==========> Continue : " + value.ToString());
                    _btnContinueVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _btnPrintManualVisibility;
        public Visibility btnPrintManualVisibility
        {
            get { return _btnPrintManualVisibility; }
            set
            {


                if (_btnPrintManualVisibility != value)
                {
                    Debug.WriteLine("==========> PrintManual : " + value.ToString());
                    _btnPrintManualVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        string vorabschaltLimitFormattedWidthUnit;

        public string VorabschaltLimitFormattedWidthUnit
        {
            get { return vorabschaltLimitFormattedWidthUnit; }
            set
            {
                if (vorabschaltLimitFormattedWidthUnit != value)
                {
                    vorabschaltLimitFormattedWidthUnit = value;
                    NotifyPropertyChanged();
                }
            }
        }
        string grobFeinLimitFormattedWithUnit;

        public string GrobFeinLimitFormattedWithUnit
        {
            get { return grobFeinLimitFormattedWithUnit; }
            set
            {
                if (grobFeinLimitFormattedWithUnit != value)
                {
                    grobFeinLimitFormattedWithUnit = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private double _tare = -1;
        public double Tare
        {
            get { return _tare; }
            set
            {
                if (_tare != value)
                {
                    _tare = value;
                    _tareFormatted = Globals.FormatWeightWithUnits(_tare);
                    NotifyPropertyChanged("TareFormatted");
                    NotifyPropertyChanged("Tare");
                }
            }
        }

        private string _tareFormatted = "";
        public string TareFormatted
        {
            get { return _tareFormatted; }
        }

        public bool Filling = false;
        private bool fillingStopped = false;

        private FillingState _fillState;
        public FillingState FillState
        {
            get { return _fillState; }
            set
            {
                if (_fillState != value)
                {
                    _fillState = value;
                    Debug.WriteLine("FillStatus: " + value);
                    DosingParams.Instance.LastFillState = value;
                    NotifyPropertyChanged("FillState");
                }
            }
        }

        private Thread _fillTask = null;
        private bool _fillingInProgress = false;

        private FillingState _fillingStateBeforePause;
        private DigitalIOLonza.FuellkopfIstState _fuellkopfIstStateBeforePause;
        private DigitalIOLonza.KlemmenStatus _klemmenstatusBeforePause;

        private bool _pause = false;
        public bool Pause
        {
            get { return _pause; }
            set
            {
                if (_pause != value)
                {
                    _pause = value;
                    NotifyPropertyChanged("Pause");
                }
                if (_pause)
                {
                    _fillingStateBeforePause = FillState;
                    FillState = FillingState.DoNothing;
                    _fuellkopfIstStateBeforePause = _digIoLonza.FuellkopfIstStatus;
                    _klemmenstatusBeforePause = _digIoLonza.Klemme;
                    SetupMenuButtons(new cButtons[] { cButtons.btnResume, cButtons.btnCancel, cButtons.btnManual });
                    SetStatusText(Localization.Get(Localization.Key.Pause));
                }
                else
                {
                    SetupMenuButtons(new cButtons[] { cButtons.btnPause });

                    // restore FuellkopüfStatus
                    if (_digIoLonza.FuellkopfIstStatus != _fuellkopfIstStateBeforePause)
                    {
                        switch (_fuellkopfIstStateBeforePause)
                        {
                            case DigitalIOLonza.FuellkopfIstState.istHoch:
                                {
                                    _digIoLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollHoch);
                                    break;
                                }
                            case DigitalIOLonza.FuellkopfIstState.istTief:
                                {
                                    _digIoLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollTief);
                                    break;
                                }
                        }
                    }
                    // restore KlemmenStatus
                    _digIoLonza.Klemme = _klemmenstatusBeforePause;
                    // restore FillState
                    FillState = _fillingStateBeforePause;
                }
            }
        }
        #endregion



        enum cButtons
        {
            btnStart,
            btnCancel,
            btnPause,
            btnResume,
            btnEndSerie,
            btnManual,
            btnEndManual,
            btnBack,
            btnReject,
            btnContinue,
            btnPrintManual
        }

        cButtons[] allButtons = { cButtons.btnBack,
                                  cButtons.btnCancel,
                                  cButtons.btnContinue,
                                  cButtons.btnEndManual,
                                  cButtons.btnEndSerie,
                                  cButtons.btnManual,
                                  cButtons.btnPause,
                                  cButtons.btnResume,
                                  cButtons.btnReject,
                                  cButtons.btnStart
                                };



        void RestoreMenu()
        {
            Debug.WriteLine("==========> RestoreMenu");
            if (_lastButtonsSaved.Count > 0)
            {
                _lastButtonsSaved.ToArray();
                CollapsAll(_lastButtonsSaved.ToArray());
                _lastButtons.Clear();
                foreach (var item in _lastButtonsSaved)
                {
                    _lastButtons.Add(item);
                    UpdateVisibility(item);
                }
            }
        }

        private void SaveMenu()
        {
            _lastButtonsSaved.Clear();
            foreach (var item in _lastButtons)
            {
                _lastButtonsSaved.Add(item);
            }
            Debug.WriteLine("==========> SaveMenu");
        }

        List<cButtons> _lastButtons = new List<cButtons>();
        List<cButtons> _lastButtonsSaved = new List<cButtons>();
        void SetupMenuButtons(cButtons[] buttons)
        {
            CollapsAll(buttons);
            if (buttons != null)
            {
                _lastButtons.Clear();
                for (int i = 0; i < buttons.Length; i++)
                {
                    _lastButtons.Add(buttons[i]);
                    UpdateVisibility(buttons[i]);
                }
            }
        }

        private void UpdateVisibility(cButtons button)
        {
            switch (button)
            {
                case cButtons.btnStart:
                    btnStartVisibility = Visibility.Visible;
                    break;
                case cButtons.btnCancel:
                    btnStopVisibility = Visibility.Visible;
                    break;
                case cButtons.btnPause:
                    btnPauseVisibility = Visibility.Visible;
                    break;
                case cButtons.btnResume:
                    btnResumeVisibility = Visibility.Visible;
                    break;
                case cButtons.btnEndSerie:
                    btnEndSerieVisibility = Visibility.Visible;
                    break;
                case cButtons.btnManual:
                    btnManualVisibility = Visibility.Visible;
                    break;
                case cButtons.btnEndManual:
                    btnEndManualVisibility = Visibility.Visible;
                    break;
                case cButtons.btnReject:
                    btnRejectVisibility = Visibility.Visible;
                    break;
                case cButtons.btnBack:
                    btnBackVisibility = Visibility.Visible;
                    break;
                case cButtons.btnContinue:
                    btnContinueVisibility = Visibility.Visible;
                    break;
                case cButtons.btnPrintManual:
                    btnPrintManualVisibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }

        bool FindButton(cButtons btn, cButtons[] buttons)
        {
            if (buttons == null)
                return false;

            try
            {
                bool retval = false;
                foreach (var item in buttons)
                {
                    if (item == btn)
                    {
                        retval = true;
                        break;
                    }
                }
                return retval;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

        private void CollapsAll(cButtons[] buttons)
        {
            if (buttons != null)
            {
                if (!FindButton(cButtons.btnBack, buttons))
                    btnBackVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnCancel, buttons))
                    btnStopVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnContinue, buttons))
                    btnContinueVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnEndManual, buttons))
                    btnEndManualVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnEndSerie, buttons))
                    btnEndSerieVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnManual, buttons))
                    btnManualVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnPause, buttons))
                    btnPauseVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnResume, buttons))
                    btnResumeVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnReject, buttons))
                    btnRejectVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnStart, buttons))
                    btnStartVisibility = Visibility.Collapsed;
                if (!FindButton(cButtons.btnPrintManual, buttons))
                    btnPrintManualVisibility = Visibility.Collapsed;
            }
            else
            {
                btnBackVisibility = Visibility.Collapsed;
                btnStopVisibility = Visibility.Collapsed;
                btnContinueVisibility = Visibility.Collapsed;
                btnEndManualVisibility = Visibility.Collapsed;
                btnEndSerieVisibility = Visibility.Collapsed;
                btnManualVisibility = Visibility.Collapsed;
                btnPauseVisibility = Visibility.Collapsed;
                btnResumeVisibility = Visibility.Collapsed;
                btnRejectVisibility = Visibility.Collapsed;
                btnStartVisibility = Visibility.Collapsed;
                btnPrintManualVisibility = Visibility.Collapsed;
            }
        }

        bool FirstTimeCorrection;

        public void StartFillThread()
        {
            Filling = true;
            fillingStopped = false;
            FillState = FillingState.Idle;
            lastFillState = FillingState.Idle;
            _startTasterActivated = false;
            _fillingInProgress = false;
            _pause = false;
            // initialize
            ResetOutputs();
            threadTimer.Enabled = true;
        }

        public void StopFillThread()
        {
            fillingStopped = true;
        }
        bool NextStepDosing = false;

        DateTime _dribbleTime = DateTime.Now;
        FillingState lastFillState;
        public async void FillThread()
        {

            threadTimer.Enabled = false;

            if (_startTasterActivated)
            {
                if (_fillingInProgress)
                {
                    Pause = !Pause;
                    _startTasterActivated = false;
                    if (Pause)
                    {
                        SetPumpOff();
                    }
                }

                if (NextStepDosing)
                    FillState = FillingState.Fill;
            }

            WeightInformation weightInformation = await _scaleService.SelectedScale.GetWeightAsync(UnitType.Host);
            double convertedNet = WeightUnits.Convert(weightInformation.NetWeight, weightInformation.Unit, WellknownWeightUnit.Kilogram);
            double convertedGross = WeightUnits.Convert(weightInformation.GrossWeight, weightInformation.Unit, WellknownWeightUnit.Kilogram);

            switch (FillState)
            {
                case FillingState.Idle:
                    {
                        messageType = MT.LonzaPixon.Globals.MessageType.undefined;

                        DeltaTracVisibility = Visibility.Hidden;
                        GraphicStatusVisibility = Visibility.Visible;
                        FillState = FillingState.WaitForMinMaxTare;
                        VorabschaltLimitFormattedWidthUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.VorabschaltLimit);
                        GrobFeinLimitFormattedWithUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.GrobFeinLimit);

                        if (DosingParams.Instance.GebindeNr == 0)
                            DosingParams.Instance.NettoSum = 0;
                        Log4NetManager.ApplicationLogger.Info("Fill started," +
                                                               " SetWeight=" + DosingParams.Instance.Sollwert.ToString() +
                                                               " TaraMin=" + DosingParams.Instance.TaraMin.ToString() +
                                                               " TaraMax=" + DosingParams.Instance.TaraMax.ToString() +
                                                               " TolNeg=" + DosingParams.Instance.TolNeg.ToString() +
                                                               " TolPos=" + DosingParams.Instance.TolPos.ToString() +
                                                               " CoarseLimit=" + DosingParams.Instance.GrobFeinLimit.ToString() +
                                                               " Preact=" + DosingParams.Instance.VorabschaltLimit.ToString());
                        break;
                    }
                case FillingState.WaitForMinMaxTare:
                    {
                        Tare = 0;
                        _fillingInProgress = false;
                        NextStepDosing = false;
                        DeltaTracVisibility = Visibility.Hidden;
                        if (convertedGross < DosingParams.Instance.TaraMin)
                            FillState = FillingState.UnderMinTare;
                        else if (convertedGross > DosingParams.Instance.TaraMax)
                            FillState = FillingState.OverMaxTare;
                        else
                            FillState = FillingState.MinMaxTareFound;
                        break;
                    }
                case FillingState.OverMaxTare:

                    if (lastFillState != FillState)
                    {
                        SetStatusError(Localization.Get(Localization.Key.TareHigherAsMax));
                        lastFillState = FillState;
                        StartEnabled = false;
                        if (DosingParams.Instance.GebindeNr == 0)
                            SetupMenuButtons(new cButtons[] { cButtons.btnBack, cButtons.btnManual });
                        else
                            SetupMenuButtons(new cButtons[] { cButtons.btnEndSerie, cButtons.btnManual });
                    }
                    FillState = FillingState.WaitForMinMaxTare;
                    break;
                case FillingState.UnderMinTare:

                    if (lastFillState != FillState)
                    {
                        SetStatusError(Localization.Get(Localization.Key.TareSmallerAsMin));
                        lastFillState = FillState;
                        StartEnabled = false;
                        if (DosingParams.Instance.GebindeNr == 0)
                            SetupMenuButtons(new cButtons[] { cButtons.btnBack, cButtons.btnManual });
                        else
                            SetupMenuButtons(new cButtons[] { cButtons.btnEndSerie, cButtons.btnManual });
                    }
                    FillState = FillingState.WaitForMinMaxTare;
                    break;

                case FillingState.MinMaxTareFound:

                    if (lastFillState != FillState)
                    {
                        SetStatusText(Localization.Get(Localization.Key.ContainerTareOK));
                        lastFillState = FillState;
                        StartEnabled = true;
                        if (DosingParams.Instance.GebindeNr == 0)
                            SetupMenuButtons(new cButtons[] { cButtons.btnStart, cButtons.btnBack, cButtons.btnManual });
                        else
                            SetupMenuButtons(new cButtons[] { cButtons.btnStart, cButtons.btnEndSerie, cButtons.btnManual });
                    }
                    if (_startTasterActivated)
                    {
                        _startTasterActivated = false;
                        FillState = FillingState.TareBeforeFill;
                    }
                    else
                        FillState = FillingState.WaitForMinMaxTare;
                    break;

                case FillingState.LearnModeCoarse:
                case FillingState.LearnModeCoarseStart:
                case FillingState.LearnModeCoarseWait:
                    {
                        var res = await LearnModeCoarse();
                        break;
                    }
                case FillingState.LearnModeCoarseEnd:
                    {
                        FillState = FillingState.LearnModeFine;
                        break;
                    }
                case FillingState.LearnModeFine:
                case FillingState.LearnModeFineStart:
                case FillingState.LearnModeFineWait:
                    {
                        var res = await LearnModeFine();
                        break;
                    }
                case FillingState.LearnModeFineEnd:
                    {
                        FillState = FillingState.Fill;
                        break;
                    }
                case FillingState.TareBeforeFill:
                    {
                        // Dosing head down
                        FuellkopfTief();

                        SetStatusText(Localization.Get(Localization.Key.TareBalance));
                        FirstTimeCorrection = true;
                        WeightState state = await _scaleService.SelectedScale.TareAsync();

                        // Klemme open
                        _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeAuf;

                        if (state != WeightState.OK)
                        {
                            SetStatusError(Localization.Get(Localization.Key.TareFailed) + ", status: " + state.ToString());
                        }
                        else
                        {
                            SetupMenuButtons(new cButtons[] { cButtons.btnPause });
                            DeltaTracVisibility = Visibility.Visible;

                            WeightInformation wInfo = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
                            convertedGross = WeightUnits.Convert(wInfo.GrossWeight, wInfo.Unit, WellknownWeightUnit.Kilogram);
                            if (wInfo.IsValid)
                            {
                                if (convertedGross >= DosingParams.Instance.TaraMin &&
                                    convertedGross <= DosingParams.Instance.TaraMax)
                                {
                                    Tare = convertedGross;
                                    Log4NetManager.ApplicationLogger.Info("Tare " + Globals.FormatWeightWithUnits(Tare));

                                    FillState = FillingState.CheckAutoCalcPreact;
                                }
                                else
                                {
                                    FillState = FillingState.WaitForMinMaxTare;
                                }
                            }
                            else
                            {
                                Log4NetManager.ApplicationLogger.Error("Invalid weight;" + FillState.ToString());
                                SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                                StartEnabled = false;
                            }
                        }
                        break;
                    }
                case FillingState.CheckAutoCalcPreact:
                    {
                        DosingParams.Instance.GebindeNr = DosingParams.Instance.LastGebindeNr + 1;

                        if (DosingParams.Instance.GebindeNr == 1 && _configuration.Lernmodus == LonzaPixonConfiguration.LernModusEnum.Aktiv)
                        {
                            FillState = FillingState.LearnModeCoarse;
                        }
                        else
                        {
                            FillState = FillingState.Fill;
                        }
                        break;
                    }
                case FillingState.Fill:
                    {
                        _fillingInProgress = true;

                        _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeAuf;

                        if (weightInformation.IsValid)
                        {
                            // check for coarse fill
                            if (convertedNet < DosingParams.Instance.Sollwert - DosingParams.Instance.GrobFeinLimit - DosingParams.Instance.VorabschaltLimit)
                            {
                                SetPumpCoarse();
                                FillState = FillingState.FillCoarse;
                                break;
                            }

                            // check for fine fill
                            if (convertedNet < DosingParams.Instance.Sollwert - DosingParams.Instance.VorabschaltLimit)
                            {
                                SetPumpFine();
                                FillState = FillingState.FillFine;
                                break;
                            }

                            // preact reached
                            _flexiconPump.PumpOff(true);
                            _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                            FillState = FillingState.FillPreact;
                        }
                        else
                        {
                            Log4NetManager.ApplicationLogger.Error("Invalid weight;" + FillState.ToString());
                            SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                            StartEnabled = false;
                        }
                        break;
                    }
                case FillingState.FillCoarse:
                    {
                        NextStepDosing = false;
                        SetStatusText(Localization.Get(Localization.Key.FillCoarse));
                        FillState = FillingState.Fill;
                        break;
                    }
                case FillingState.FillFine:
                    {
                        NextStepDosing = false;
                        SetStatusText(Localization.Get(Localization.Key.FillFine));
                        FillState = FillingState.Fill;
                        break;
                    }
                case FillingState.FillPreact:
                    {
                        SetStatusText(Localization.Get(Localization.Key.Preact));
                        FillState = FillingState.FillDribbleStart;
                        break;
                    }
                case FillingState.FillDribbleStart:
                    {
                        SetStatusText(Localization.Get(Localization.Key.DribbleStart));
                        if (NextStepDosing)
                            _dribbleTime = DateTime.Now.AddSeconds(-1);
                        else
                            _dribbleTime = DateTime.Now.AddSeconds(_configuration.Beruhigungszeit);
                        FillState = FillingState.FillDribbleWait;
                        _fillingInProgress = false;
                        StartEnabled = false;

                        break;
                    }
                case FillingState.FillDribbleWait:
                    {
                        TimeSpan _diff = _dribbleTime - DateTime.Now;
                        if (!NextStepDosing)
                            SetStatusText(Localization.Get(Localization.Key.DribbleWait) + " " + _diff.Seconds.ToString() + " s");
                        if (DateTime.Now > _dribbleTime)
                        {
                            FillState = FillingState.FillDribbleEnd;
                        }
                        break;
                    }
                case FillingState.FillDribbleEnd:
                    {
                        SetStatusText(Localization.Get(Localization.Key.Measure));
                        // ToDo: write Alibi record
                        // save alibi record
                        var res = await HomeScreen.Instance.ViewModel.AlibiLogComponent.SaveAndGetAlibiRecordAsync(_scaleService.SelectedScale.ScaleNumber, UnitType.Display, "Gebinde " + DosingParams.Instance.GebindeNr.ToString());
                        WeightInformation wInfo = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
                        convertedNet = WeightUnits.Convert(wInfo.NetWeight, wInfo.Unit, WellknownWeightUnit.Kilogram);
                        if (wInfo.IsValid)
                        {
                            bool preactCorr = (convertedNet > DosingParams.Instance.Sollwert - DosingParams.Instance.TolNeg * 2) &&
                                              (convertedNet < DosingParams.Instance.Sollwert + DosingParams.Instance.TolPos * 2);
                            if (preactCorr && FirstTimeCorrection)
                            {
                                FirstTimeCorrection = false;
                                // Korrektur Vorabschalt Limite
                                double newPreact = DosingParams.Instance.VorabschaltLimit + (convertedNet - DosingParams.Instance.Sollwert) * _configuration.VorabschaltKorr / 100;
                                Log4NetManager.ApplicationLogger.Info("Recalculated Preact " + newPreact);
                                if (newPreact > 0)
                                {
                                    // save new preact
                                    Debug.WriteLine("Preact changed from " + DosingParams.Instance.VorabschaltLimit + " to " + newPreact);
                                    Log4NetManager.ApplicationLogger.Info("Preact changed from " + DosingParams.Instance.VorabschaltLimit + " to " + newPreact);
                                    DosingParams.Instance.VorabschaltLimit = newPreact;
                                    VorabschaltLimitFormattedWidthUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.VorabschaltLimit);
                                }
                                else
                                {
                                    Log4NetManager.ApplicationLogger.Info("Preact is negative, no change " + newPreact);
                                }
                            }

                            DosingParams.Instance.IstWert = convertedNet;
                            double percentIst = (convertedNet - DosingParams.Instance.Sollwert) * 100 / DosingParams.Instance.Sollwert;

                            if (convertedNet < DosingParams.Instance.Sollwert - DosingParams.Instance.TolNeg)
                            {
                                // unterdosiert
                                FillState = FillingState.FinishedUnderFilled;
                                Log4NetManager.ApplicationLogger.Info("Measured weight " + convertedNet.ToString() + " (finished underfilled) " + percentIst.ToString("0.00") + "%");
                            }
                            else
                            {
                                if (convertedNet > DosingParams.Instance.Sollwert + DosingParams.Instance.TolPos)
                                {
                                    // überdosiert
                                    FillState = FillingState.FinishedOverFilled;
                                    Log4NetManager.ApplicationLogger.Info("Measured weight " + convertedNet.ToString() + " (finished overfilled) " + percentIst.ToString("0.00") + "%");
                                }
                                else
                                {
                                    // in Toleranz
                                    SetStatusText(Localization.Get(Localization.Key.InTolerance));
                                    FillState = FillingState.FinishedInTolerance;
                                    Log4NetManager.ApplicationLogger.Info("Measured weight " + convertedNet.ToString() + " (finished in tolerance) " + percentIst.ToString("0.00") + "%");
                                }
                            }
                        }
                        else
                        {
                            Log4NetManager.ApplicationLogger.Error("Error invalid weight " + FillState.ToString());
                            SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                            StartEnabled = false;
                        }
                        break;
                    }

                case FillingState.RetrieveStableWeight:
                    SetStatusText(Localization.Get(Localization.Key.Measure));
                    // ToDo: write Alibi record
                    WeightInformation wgtInfo = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
                    convertedNet = WeightUnits.Convert(wgtInfo.NetWeight, wgtInfo.Unit, WellknownWeightUnit.Kilogram);
                    if (wgtInfo.IsValid)
                    {
                        DosingParams.Instance.IstWert = convertedNet;
                        FillState = FillingState.FinishedInTolerance;
                        Log4NetManager.ApplicationLogger.Info("Set weight " + convertedNet.ToString() + " (finished cancel)");
                    }
                    else
                    {
                        Log4NetManager.ApplicationLogger.Error("Error invalid weight " + FillState.ToString());
                        SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                        StartEnabled = false;
                    }
                    break;
                case FillingState.FinishedInTolerance:
                    {
                        NextStepDosing = true;
                        SetupMenuButtons(new cButtons[] { cButtons.btnEndSerie, cButtons.btnManual });
                        _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                        FuellkopfHoch();
                        DosingParams.Instance.NettoSum += DosingParams.Instance.IstWert;
                        FillState = FillingState.PrintLabel;
                        break;
                    }
                case FillingState.PrintLabel:
                    {
                        SetStatusText(Localization.Get(Localization.Key.PrintLabel));

                        _printer = new Printer(_configuration.PrinterIP, _configuration.PrinterPort);
                        if (!await _printer.PrintLabel())
                        {
                            Pause = true;
                            SetStatusError(Localization.Get(Localization.Key.ErrorLabel));
                            MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.AskReprint1) + "\n" + Localization.Get(Localization.Key.AskReprint2), Localization.Get(Localization.Key.Error), MessageBoxButtons.YesNo, MessageBoxIcon.Error, new Action<DialogResult>(answer =>
                            {
                                switch (answer)
                                {
                                    case DialogResult.Yes:
                                        {
                                            Pause = false;
                                            break;
                                        }
                                    case DialogResult.No:
                                        {
                                            Pause = false;
                                            FillState = FillingState.WaitForEmptyBalance;
                                            break;
                                        }
                                }

                            }));

                            Debug.WriteLine("Print error");
                        }
                        else
                        {
                            DosingParams.Instance.LastGebindeNr++;
                            FillState = FillingState.WaitForEmptyBalance;
                        }
                        break;
                    }
                case FillingState.FinishedOverFilled:
                    {
                        SetStatusError(Localization.Get(Localization.Key.Overdosed));
                        NextStepDosing = true;
                        ViewDecisionPanel(MT.LonzaPixon.Globals.MessageType.OverFilled, Localization.Get(Localization.Key.Overdosed) + "!", Localization.Get(Localization.Key.AcceptToleranceViolation));
                        FillState = FillingState.DoNothing;
                        break;
                    }
                case FillingState.FinishedUnderFilled:
                    {
                        SetStatusError(Localization.Get(Localization.Key.Underdosed));
                        NextStepDosing = true;
                        ViewDecisionPanel(MT.LonzaPixon.Globals.MessageType.UnderFilled, Localization.Get(Localization.Key.Underdosed) + "!", Localization.Get(Localization.Key.AcceptToleranceViolation));
                        FillState = FillingState.DoNothing;
                        break;
                    }
                case FillingState.WaitForEmptyBalance:
                    {
                        SetStatusText(Localization.Get(Localization.Key.WaitForEmptyBalance));
                        SetupMenuButtons(null);
                        if (weightInformation.IsValid)
                        {
                            if (convertedGross < DosingParams.Instance.TaraMin / 2)
                            {
                                Log4NetManager.ApplicationLogger.Info("Scale is empty");
                                await _scaleService.SelectedScale.ClearTareAsync();
                                FillState = FillingState.WaitForMinMaxTare;
                            }
                        }
                        else
                        {
                            Log4NetManager.ApplicationLogger.Error("Error invalid weight " + FillState.ToString());
                            SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                            StartEnabled = false;
                        }
                        break;
                    }
                case FillingState.EndSerie:
                    {
                        // end serie, clear sum and container number
                        Log4NetManager.ApplicationLogger.Info("End series, NettoSum=" +
                                                              DosingParams.Instance.NettoSum.ToString() + " GebindeNr=" +
                                                              DosingParams.Instance.GebindeNr.ToString());
                        DosingParams.Instance.NettoSum = 0;
                        DosingParams.Instance.GebindeNr = 0;
                        DosingParams.Instance.LastGebindeNr = 0;
                        if (ViewModel != null)
                        {
                            // Totaletikette drucken
                            Task.Run(async () => { ViewModel.DoBack(); });
                        }
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            if (!fillingStopped)
                threadTimer.Enabled = true;
        }

        private bool FuellkopfHoch()
        {
            string orgStatusText = HomeScreen.Instance.ViewModel.StatusText;
            SetStatusText(Localization.Get(Localization.Key.Fuellkopfhoch));
            DigitalIOLonza.FuellkopfIstState res = _digIoLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollHoch);
            if (res == DigitalIOLonza.FuellkopfIstState.istHoch)
            {
                SetStatusText(orgStatusText);
                return true;
            }
            else
            {
                SetStatusError(Localization.Get(Localization.Key.FehlerFuellkopf));
                Thread.Sleep(1500);
                return false;
            }
        }

        private bool FuellkopfTief()
        {
            string orgStatusText = HomeScreen.Instance.ViewModel.StatusText;
            SetStatusText(Localization.Get(Localization.Key.Fuellkopftief));
            DigitalIOLonza.FuellkopfIstState res = _digIoLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollTief);
            if (res == DigitalIOLonza.FuellkopfIstState.istTief)
            {
                SetStatusText(orgStatusText);
                return true;
            }
            else
            {
                SetStatusError(Localization.Get(Localization.Key.FehlerFuellkopf));
                Thread.Sleep(1500);
                return false;
            }
        }

        private void SetPumpCoarse()
        {
            if (!NotTasteActivated)
            {
                _flexiconPump.PumpDirection(_configuration.PumpDirection);
                _flexiconPump.PumpSpeed(_configuration.GrobStrom);
                _flexiconPump.PumpOn(true);
            }
        }

        private void SetPumpFine()
        {
            if (!NotTasteActivated)
            {
                _flexiconPump.PumpDirection(_configuration.PumpDirection);
                _flexiconPump.PumpSpeed(_configuration.FeinStrom);
                _flexiconPump.PumpOn(true);
            }
        }

        private void SetPumpOff()
        {
            _flexiconPump.PumpOff(true);
        }

        private void SetPumpEmergencyOff()
        {
            _flexiconPump.PumpOff(true);
            _flexiconPump.NextPumpOnOff = FlexiconPump.NotAusMarker;
            _flexiconPump.NextPumpDirection = FlexiconPump.NotAusMarker;
            _flexiconPump.NextPumpSpeed = FlexiconPump.NotAusMarker;
        }

        private async Task<bool> ResetOutputs()
        {
            StartEnabled = false;
            _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
            _digIoLonza.StartTasterLED = false;
            SetStatusText(Localization.Get(Localization.Key.SetPumpOff));
            _flexiconPump.PumpOff(true);
            FuellkopfHoch();
            return true;
        }

        private DateTime waitTime = DateTime.Now;
        private double coarseLearnSetWeight = 0;
        private double fineLearnSetWeight = 0;
        private double coarsePreact = 0;
        private double finePreact = 0;

        public async Task<bool> LearnModeCoarse()
        {
            switch (FillState)
            {
                // start LearnModeCoarse
                case FillingState.LearnModeCoarse:
                    {
                        SetupMenuButtons(new cButtons[] { cButtons.btnCancel });
                        SetStatusText(Localization.Get(Localization.Key.TareBalance));
                        WeightState state = await _scaleService.SelectedScale.TareAsync();
                        if (state != WeightState.OK)
                        {
                            SetStatusError(Localization.Get(Localization.Key.TareFailed) + ", status: " + state.ToString());
                        }
                        coarseLearnSetWeight = DosingParams.Instance.Sollwert * 0.75;
                        Log4NetManager.ApplicationLogger.Debug("LearnModeCoarse began, coarse set weight " + coarseLearnSetWeight);
                        FillState = FillingState.LearnModeCoarseStart;
                        SetPumpCoarse();
                        _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeAuf;
                        break;
                    }

                // Learnmode coarse
                case FillingState.LearnModeCoarseStart:
                    {
                        SetStatusText(Localization.Get(Localization.Key.LearnModeCoarseStart));
                        WeightInformation weightInformation = await _scaleService.SelectedScale.GetWeightAsync(UnitType.Host);
                        double convertedNet = WeightUnits.Convert(weightInformation.NetWeight, weightInformation.Unit, WellknownWeightUnit.Kilogram);
                        if (weightInformation.IsValid)
                        {
                            if (convertedNet < coarseLearnSetWeight)
                            {
                                _flexiconPump.PumpOn(true);
                            }
                            else
                            {
                                _flexiconPump.PumpOff(true);
                                _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                                waitTime = DateTime.Now.AddSeconds(_configuration.Beruhigungszeit);
                                FillState = FillingState.LearnModeCoarseWait;
                            }
                        }
                        else
                        {
                            SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                            StartEnabled = false;
                        }
                        break;
                    }
                case FillingState.LearnModeCoarseWait:
                    {
                        SetStatusText(Localization.Get(Localization.Key.LearnModeCoarseWait));
                        if (DateTime.Now > waitTime)
                        {
                            WeightInformation weightInformation = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
                            double convertedNet = WeightUnits.Convert(weightInformation.NetWeight, weightInformation.Unit, WellknownWeightUnit.Kilogram);
                            if (weightInformation.IsValid)
                            {
                                coarsePreact = convertedNet - coarseLearnSetWeight;
                                Log4NetManager.ApplicationLogger.Debug("LernModeCoarse end, coarsePreact " + coarsePreact);
                                SetStatusText(Localization.Get(Localization.Key.LearnModeCoarseFinished));
                                FillState = FillingState.LearnModeCoarseEnd;
                            }
                            else
                            {
                                SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                                StartEnabled = false;
                            }

                        }
                        break;
                    }
                case FillingState.LearnModeCoarseEnd:
                    {
                        break;
                    }

            }
            return true;
        }
        public async Task<bool> LearnModeFine()
        {
            switch (FillState)
            {
                // start LearnModeCoarse
                case FillingState.LearnModeFine:
                    {
                        fineLearnSetWeight = DosingParams.Instance.Sollwert * 0.85;
                        Log4NetManager.ApplicationLogger.Debug("LearnModeFine began, fine set weight " + fineLearnSetWeight);
                        FillState = FillingState.LearnModeFineStart;
                        SetPumpFine();
                        _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeAuf;
                        break;
                    }

                // Learnmode coarse
                case FillingState.LearnModeFineStart:
                    {
                        SetStatusText(Localization.Get(Localization.Key.LearnModeFineStart));
                        WeightInformation weightInformation = await _scaleService.SelectedScale.GetWeightAsync(UnitType.Host);
                        double convertedNet = WeightUnits.Convert(weightInformation.NetWeight, weightInformation.Unit, WellknownWeightUnit.Kilogram);
                        if (weightInformation.IsValid)
                        {
                            if (convertedNet < fineLearnSetWeight)
                            {
                                _flexiconPump.PumpOn(true);
                            }
                            else
                            {
                                _flexiconPump.PumpOff(true);
                                _digIoLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
                                waitTime = DateTime.Now.AddSeconds(_configuration.Beruhigungszeit);
                                FillState = FillingState.LearnModeFineWait;
                            }
                        }
                        else
                        {
                            SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                            StartEnabled = false;
                        }
                        break;
                    }
                case FillingState.LearnModeFineWait:
                    {
                        SetStatusText(Localization.Get(Localization.Key.LearnModeFineWait));
                        if (DateTime.Now > waitTime)
                        {
                            WeightInformation weightInformation = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
                            double convertedNet = WeightUnits.Convert(weightInformation.NetWeight, weightInformation.Unit, WellknownWeightUnit.Kilogram);
                            if (weightInformation.IsValid)
                            {
                                finePreact = convertedNet - fineLearnSetWeight;
                                Log4NetManager.ApplicationLogger.Debug("LernModeFine end, finePreact " + finePreact);
                                if (finePreact > 0)
                                {
                                    DosingParams.Instance.VorabschaltLimit = finePreact;
                                    DosingParams.Instance.GrobFeinLimit = finePreact + coarsePreact * 1.1;
                                    VorabschaltLimitFormattedWidthUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.VorabschaltLimit);
                                    GrobFeinLimitFormattedWithUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.GrobFeinLimit);
                                }
                                else
                                {
                                    Log4NetManager.ApplicationLogger.Debug("LearnModeFine: Preact is negative - no change");
                                    MessageBox.Show(HomeScreen.Instance.NavigationFrame, Localization.Get(Localization.Key.ErrorPreactNegativ), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                SetStatusText(Localization.Get(Localization.Key.LearnModeFineFinished));
                                FillState = FillingState.LearnModeFineEnd;
                            }
                            else
                            {
                                SetStatusError(Localization.Get(Localization.Key.ErrorWeightInvalid));
                                StartEnabled = false;
                            }

                        }
                        break;
                    }
                case FillingState.LearnModeFineEnd:
                    {
                        break;
                    }

            }
            return true;
        }


        private void SetStatusText(string text)
        {
            HomeScreen.Instance.ViewModel.SetStatusText(text);
        }

        private void SetStatusError(string text)
        {
            HomeScreen.Instance.ViewModel.SetStatusError(text);
        }

        /// <summary>
        /// Occures when the event is raised
        /// </summary>
        public event EventHandler<ManualFunctionRequestEventArgs> ManualFunctionRequest;

        /// <summary>
        /// Raises the <see cref="ManualFunctionRequest"/> event
        /// </summary>
        /// <param name="e">An <see cref="ManualFunctionRequestEventArgs"/> that contains the event data.</param>
        protected virtual void OnManualFunctionRequest(ManualFunctionRequestEventArgs e)
        {
            if (ManualFunctionRequest != null)
                ManualFunctionRequest(this, e);
        }

        /// <summary>
        /// Contains all information related to a ManualFunctionRequest event
        /// </summary>
        public class ManualFunctionRequestEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ManualFunctionRequest"/> class
            /// </summary>
            /// 
            public bool Request { get; set; }

            public ManualFunctionRequestEventArgs(bool Request)
            {
                this.Request = Request;
            }
        }

    }

    public enum FillingState
    {
        Idle,
        DoNothing,
        WaitForMinMaxTare,
        OverMaxTare,
        UnderMinTare,
        MinMaxTareFound,

        LearnModeCoarse,
        LearnModeCoarseStart,
        LearnModeCoarseWait,
        LearnModeCoarseEnd,

        LearnModeFine,
        LearnModeFineStart,
        LearnModeFineWait,
        LearnModeFineEnd,

        CheckAutoCalcPreact,

        TareBeforeFill,
        Fill,
        FillCoarse,
        FillFine,
        FillPreact,

        FillDribbleStart,
        FillDribbleWait,
        FillDribbleEnd,

        RetrieveStableWeight,
        FinishedInTolerance,
        FinishedUnderFilled,
        FinishedOverFilled,

        PrintLabel,

        WaitForEmptyBalance,

        EndSerie
    }


}
