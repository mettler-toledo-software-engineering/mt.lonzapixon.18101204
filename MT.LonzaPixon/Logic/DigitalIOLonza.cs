﻿using System;
using System.Diagnostics;
using System.Threading;
using MT.LonzaPixon.Pages;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;

namespace MT.LonzaPixon.Logic
{
    public class DigitalIOLonza : DigitalIO
    {
        // constant definitions for LonzaPixon Dig I/O

        // Digital In
        // ----------
        // Input1: Not-Aus
        private int idxNotTaster = 0;
        // Input2: Starttaster
        private int idxStarttaster = 1;
        // Input3: Füllkopf Endschalter unten
        private int idxEndschalterUnten = 2;
        // Input4: Füllkopf Endschalter oben
        private int idxEndschalterOben = 3;

        // Digital Out
        // -----------
        // Output1: (optional) LED Starttaster
        public int idxFuellkopf = 1;
        // Output2: Füllkopf hoch
        public int idxKlemme = 2;
        // Output3: Klemme
        // Output4: 

        private static int _timeout = 5; // seconds

        public DigitalIOLonza(IDigitalIOInterface digitalIo)
            : base(digitalIo)
        {
            EventDigitalInChanged += DigitalIOLonza_DigitalInChanged;
        }

        void DigitalIOLonza_DigitalInChanged(object sender, DigitalIO.EventDigitalInChangedArgs e)
        {
            EventDigitalInChanged -= DigitalIOLonza_DigitalInChanged;
            try
            {
                Log4NetManager.ApplicationLogger.Debug("DigitalInChanged" +
                    " Inputs=" + (e.Inputs[0] ? "1" : "0") + (e.Inputs[1] ? "1" : "0") + (e.Inputs[2] ? "1" : "0") + (e.Inputs[3] ? "1" : "0") +
                    " Changed=" + (e.InputsChanged[0] ? "1" : "0") + (e.InputsChanged[1] ? "1" : "0") + (e.InputsChanged[2] ? "1" : "0") + (e.InputsChanged[3] ? "1" : "0"));
                if (e.InputsChanged[idxNotTaster])
                {
                    // NotTaster changed
                    Log4NetManager.ApplicationLogger.Debug("EmergencySwitch: " + e.Inputs[idxNotTaster]);
                    OnEventNotTasterChanged(new EventNotTasterChangedArgs(e.Inputs[idxNotTaster]));
                }
                if (e.InputsChanged[idxStarttaster])
                {
                    // StartTaster changed
                    Log4NetManager.ApplicationLogger.Debug("StartSwitch: " + e.Inputs[idxStarttaster]);
                    OnEventStartTasterChanged(new EventStartTasterChangedArgs(e.Inputs[idxStarttaster]));
                }
                if (e.InputsChanged[idxEndschalterUnten])
                {
                    // Endschalter unten changed
                    Log4NetManager.ApplicationLogger.Debug("Fuel lance lower switch: " + e.Inputs[idxEndschalterUnten]);
                }
                if (e.InputsChanged[idxEndschalterOben])
                {
                    // Endschalter oben changed
                    Log4NetManager.ApplicationLogger.Debug("Fuel lance upper switch: " + e.Inputs[idxEndschalterOben]);
                }
            }
            finally
            {
                EventDigitalInChanged += DigitalIOLonza_DigitalInChanged;
            }
        }

        public static DigitalIOLonza InstanceLonza
        {
            get { return Instance as DigitalIOLonza; }
        }

        public enum FuellkopfSollState
        {
            sollQuery,
            sollHoch,
            sollTief
        }

        public enum FuellkopfIstState
        {
            istUndef,
            istHoch,
            istTief,
            istTimeout
        }

        private Boolean _startTasterLEDPushed;
        public void PushStartTasterLED()
        {
            _startTasterLEDPushed = StartTasterLED;
        }
        public void PopStartTasterLED()
        {
            StartTasterLED = _startTasterLEDPushed;
        }

        // Digital out
        public Boolean StartTasterLED
        {
            get { return Output1; }
            set
            {
                Output1 = value;
                Log4NetManager.ApplicationLogger.Debug("Start switch LED " + value);
                Debug.WriteLine("StartTasterLED: " + value);
            }
        }

        public Boolean Fuellkopf
        {
            get { return Output2; }
            set
            {
                Output2 = value;
                Log4NetManager.ApplicationLogger.Debug("Fuel lance " + value);
                Debug.WriteLine("FüllkopfHoch: " + value);
            }
        }

        public KlemmenStatus Klemme
        {
            get { return Output3 ? KlemmenStatus.KlemmeAuf : KlemmenStatus.KlemmeZu; }
            set
            {
                if ((Output3 ? KlemmenStatus.KlemmeAuf : KlemmenStatus.KlemmeZu) != value)
                {
                    Output3 = value == KlemmenStatus.KlemmeAuf ? true : false;
                    Log4NetManager.ApplicationLogger.Debug("Clamp " + value.ToString());
                    OnEventKlemmenStatusChanged(new EventKlemmenStatusChangedArgs(value));
                    Debug.WriteLine("Klemme: " + value);
                }
            }
        }

        private FuellkopfIstState _fuellkopfIstStatus = FuellkopfIstState.istUndef;
        public FuellkopfIstState FuellkopfIstStatus
        {
            get { return _fuellkopfIstStatus; }
            set
            {
                if (_fuellkopfIstStatus != value)
                {
                    _fuellkopfIstStatus = value;
                    Log4NetManager.ApplicationLogger.Debug("Fuel lance status " + value.ToString());
                    OnEventFuellkopfStatusChanged(new EventFuellkopfStatusChangedArgs(value));
                    NotifyPropertyChanged();
                }
            }
        }

        public enum KlemmenStatus
        {
            KlemmeAuf,
            KlemmeZu
        }

        // Digital in
        public Boolean EndschalterUnten
        {
            get { return Input3; }
        }

        public Boolean EndschalterOben
        {
            get { return Input4; }
        }

        public Boolean NotTaster
        {
            get
            {
                if (_digitalIo == null)
                    return true;
                else
                    return Input1;
            }
        }

        public Boolean Start
        {
            get { return Input2; }
        }

        // Workaround for Fuellkopf status and timeout:
        //  not responding to digital inputs Endschalter oben/unten
        //  if Config.EndschalterIgnore is true, method ignores Endschalter and waits 2 seconds before resuming

        private const int EndschalterNoObserveTime = 2000; // millisec

        // methods
        public FuellkopfIstState SetFuellkopf(FuellkopfSollState _state)
        {
            Log4NetManager.ApplicationLogger.Debug("Fuel lance " + _state.ToString());
            Debug.WriteLine("Füllkopf: " + _state);
            bool lastFuellkopf = Fuellkopf;
            switch (_state)
            {
                case FuellkopfSollState.sollHoch:
                    {
                        // Füllkopf hoch
                        Fuellkopf = true;
                        if (HomeScreen.Instance.ViewModel.Configuration.HWSimulation || HomeScreen.Instance.ViewModel.Configuration.EndschalterIgnore)
                        {
                            FuellkopfIstStatus = FuellkopfIstState.istHoch;
                            if (HomeScreen.Instance.ViewModel.Configuration.EndschalterIgnore && !lastFuellkopf)
                            {
                                for (int i = 1; i < 8; i++)
                                {
                                    Thread.Sleep(EndschalterNoObserveTime >> 3);
                                }
                            }
                            return FuellkopfIstStatus;
                        }
                        break;
                    }
                case FuellkopfSollState.sollTief:
                    {
                        // Füllkopf tief
                        Fuellkopf = false;
                        if (HomeScreen.Instance.ViewModel.Configuration.HWSimulation || HomeScreen.Instance.ViewModel.Configuration.EndschalterIgnore)
                        {
                            FuellkopfIstStatus = FuellkopfIstState.istTief;
                            if (HomeScreen.Instance.ViewModel.Configuration.EndschalterIgnore && lastFuellkopf)
                            {
                                Thread.Sleep(EndschalterNoObserveTime);
                            }
                            return FuellkopfIstStatus;
                        }
                        break;
                    }
                case FuellkopfSollState.sollQuery:
                    {
                        // query Füllkopf position without change it
                        if (HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
                            if (EndschalterUnten && !EndschalterOben)
                            {
                                FuellkopfIstStatus = FuellkopfIstState.istTief;
                                return FuellkopfIstStatus;
                            }
                            else if (!EndschalterUnten && EndschalterOben)
                            {
                                FuellkopfIstStatus = FuellkopfIstState.istHoch;
                                return FuellkopfIstStatus;
                            }
                            else
                            {
                                FuellkopfIstStatus = FuellkopfIstState.istUndef;
                                return FuellkopfIstStatus;
                            }
                        break;
                    }
                default:
                    break;
            }
            return WaitForFuellkopfEndschalter(_state);
        }

        private FuellkopfIstState WaitForFuellkopfEndschalter(FuellkopfSollState _sollState)
        {
            DateTime timeoutTime = DateTime.Now.AddSeconds(_timeout);
            bool timeout = false;

            while (true)
            {
                // read inputs, event not fired to thread
                ReadInputDigitalIO();

                var result = FuellkopfIstState.istUndef;
                if (EndschalterUnten && !EndschalterOben)
                {
                    FuellkopfIstStatus = FuellkopfIstState.istTief;
                    result = FuellkopfIstStatus;
                }
                else if (!EndschalterUnten && EndschalterOben)
                {
                    FuellkopfIstStatus = FuellkopfIstState.istHoch;
                    result = FuellkopfIstStatus;
                }
                Debug.WriteLine("Füllkopfüberwachung: " + _sollState.ToString() + " " + result.ToString());

                switch (_sollState)
                {
                    case FuellkopfSollState.sollHoch:
                        {
                            if (result == FuellkopfIstState.istHoch)
                            {
                                Log4NetManager.ApplicationLogger.Debug("Fuel lance result " + result.ToString());
                                return result;
                            }
                            else
                                break;
                        }
                    case FuellkopfSollState.sollTief:
                        {
                            if (result == FuellkopfIstState.istTief)
                            {
                                Log4NetManager.ApplicationLogger.Debug("Fuel lance result " + result.ToString());
                                return result;
                            }
                            else
                                break;
                        }
                    case FuellkopfSollState.sollQuery:
                        {
                            Log4NetManager.ApplicationLogger.Debug("Fuel lance result " + result.ToString());
                            return result;
                        }
                    default:
                        {
                            Log4NetManager.ApplicationLogger.Debug("Fuel lance result " + result.ToString());
                            return FuellkopfIstState.istUndef;
                        }
                }

                timeout = DateTime.Now > timeoutTime;
                if (timeout)
                {
                    Log4NetManager.ApplicationLogger.Debug("Fuel lance timeout");
                    Debug.WriteLine("Füllkopf Timeout");
                    return FuellkopfIstState.istTimeout;
                }
                Thread.Sleep(250);
            }
        }

        #region replaced async methods

        //public async Task<FuellkopfIstState> SetFuellkopfAsync(FuellkopfSollState _state)
        //{
        //    Debug.WriteLine("Füllkopf async: " + _state);
        //    switch (_state)
        //    {
        //        case FuellkopfSollState.sollHoch:
        //            {
        //                // Füllkopf hoch
        //                Fuellkopf = true;
        //                // ToDo: Endschalter rücklesen
        //                // if (HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
        //                {
        //                    FuellkopfIstStatus = FuellkopfIstState.istHoch;
        //                    return FuellkopfIstStatus;
        //                }
        //                break;
        //            }
        //        case FuellkopfSollState.sollTief:
        //            {
        //                // Füllkopf tief
        //                Fuellkopf = false;
        //                // ToDo: Endschalter rücklesen 
        //                // if (HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
        //                {
        //                    FuellkopfIstStatus = FuellkopfIstState.istTief;
        //                    return FuellkopfIstStatus;
        //                }
        //                break;
        //            }
        //        case FuellkopfSollState.sollQuery:
        //            {
        //                // query Füllkopf position without change it
        //                // ToDo: Endschalter rücklesen 
        //                // if (HomeScreen.Instance.ViewModel.Configuration.HWSimulation)
        //                if (EndschalterUnten && !EndschalterOben)
        //                {
        //                    FuellkopfIstStatus = FuellkopfIstState.istTief;
        //                    return FuellkopfIstStatus;
        //                }
        //                else if (!EndschalterUnten && EndschalterOben)
        //                {
        //                    FuellkopfIstStatus = FuellkopfIstState.istHoch;
        //                    return FuellkopfIstStatus;
        //                }
        //                else
        //                {
        //                    FuellkopfIstStatus = FuellkopfIstState.istUndef;
        //                    return FuellkopfIstStatus;
        //                }
        //                break;
        //            }
        //        default:
        //            break;
        //    }
        //    return await WaitForFuellkopfEndschalterAsync(_state);
        //}

        //private async Task<FuellkopfIstState> WaitForFuellkopfEndschalterAsync(FuellkopfSollState _sollState)
        //{
        //    DateTime timeoutTime = DateTime.Now.AddSeconds(_timeout);
        //    bool timeout = false;

        //    while (true)
        //    {
        //        // read inputs, event not fired to thread
        //        //ReadInputDigitalIO();

        //        var result = FuellkopfIstState.istUndef;
        //        if (EndschalterUnten && !EndschalterOben)
        //        {
        //            FuellkopfIstStatus = FuellkopfIstState.istTief;
        //            result = FuellkopfIstStatus;
        //        }
        //        else if (!EndschalterUnten && EndschalterOben)
        //        {
        //            FuellkopfIstStatus = FuellkopfIstState.istHoch;
        //            result = FuellkopfIstStatus;
        //        }
        //        Debug.WriteLine("Füllkopfüberwachung: " + _sollState.ToString() + " " + result.ToString());

        //        switch (_sollState)
        //        {
        //            case FuellkopfSollState.sollHoch:
        //                {
        //                    if (result == FuellkopfIstState.istHoch)
        //                        return result;
        //                    else
        //                        break;
        //                }
        //            case FuellkopfSollState.sollTief:
        //                {
        //                    if (result == FuellkopfIstState.istTief)
        //                        return result;
        //                    else
        //                        break;
        //                }
        //            case FuellkopfSollState.sollQuery:
        //                {
        //                    return result;
        //                }
        //            default:
        //                {
        //                    return FuellkopfIstState.istUndef;
        //                }
        //        }

        //        timeout = DateTime.Now > timeoutTime;
        //        if (timeout)
        //        {
        //            Debug.WriteLine("Füllkopf Timeout");
        //            return FuellkopfIstState.istTimeout;
        //        }
        //        Thread.Sleep(250);
        //    }
        //}

        #endregion

        #region Events

        #region Nottaster changed
        public class EventNotTasterChangedArgs : EventArgs
        {
            public bool NotTaster { get; set; }

            public EventNotTasterChangedArgs(bool notTaster)
            {
                NotTaster = notTaster;
            }
        }

        public event EventHandler<EventNotTasterChangedArgs> EventNotTasterChanged;

        protected virtual void OnEventNotTasterChanged(EventNotTasterChangedArgs e)
        {
            if (EventNotTasterChanged != null)
            {
                EventNotTasterChanged(this, e);
            }
        }

        #endregion

        #region Starttaster changed

        public class EventStartTasterChangedArgs : EventArgs
        {
            public bool StartTaster { get; set; }

            public EventStartTasterChangedArgs(bool startTaster)
            {
                StartTaster = startTaster;
            }
        }

        public event EventHandler<EventStartTasterChangedArgs> EventStartTasterChanged;

        protected virtual void OnEventStartTasterChanged(EventStartTasterChangedArgs e)
        {
            if (EventStartTasterChanged != null)
            {
                EventStartTasterChanged(this, e);
            }
        }

        #endregion

        #region KlemmenStatus changed

        public class EventKlemmenStatusChangedArgs : EventArgs
        {
            public KlemmenStatus KlemmenStatus { get; set; }

            public EventKlemmenStatusChangedArgs(KlemmenStatus klemmenStatus)
            {
                KlemmenStatus = klemmenStatus;
            }
        }

        public event EventHandler<EventKlemmenStatusChangedArgs> EventKlemmenStatusChanged;

        protected virtual void OnEventKlemmenStatusChanged(EventKlemmenStatusChangedArgs e)
        {
            if (EventKlemmenStatusChanged != null)
            {
                EventKlemmenStatusChanged(this, e);
            }
        }

        #endregion

        #region FuellkopfStatus changed

        public class EventFuellkopfStatusChangedArgs : EventArgs
        {
            public FuellkopfIstState FuellkopfStatus { get; set; }

            public EventFuellkopfStatusChangedArgs(FuellkopfIstState fuellkopfStatus)
            {
                FuellkopfStatus = fuellkopfStatus;
            }
        }

        public event EventHandler<EventFuellkopfStatusChangedArgs> EventFuellkopfStatusChanged;

        protected virtual void OnEventFuellkopfStatusChanged(EventFuellkopfStatusChangedArgs e)
        {
            if (EventFuellkopfStatusChanged != null)
            {
                EventFuellkopfStatusChanged(this, e);
            }
        }

        #endregion

        #endregion
    }
}
