﻿using System;
using System.Reflection;
using MT.LonzaPixon.Pages;
using MT.Singularity;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;

namespace MT.LonzaPixon
{
    public static class Globals
    {
        // MT Green and MT Blue are defined in Corporate_Design_Guidelindes on MT Global site document
        public static SolidColorBrush MTGreen = new SolidColorBrush(new Color(0xFF, 0x83, 0xBC, 0x34));
        //        public static SolidColorBrush MTBlue = new SolidColorBrush(new Color(0xFF, 0x23, 0x87, 0xCC));
        public static SolidColorBrush MTBlue = new SolidColorBrush(new Color(0xFF, 0x00, 0x1F, 0xA4));
        public static SolidColorBrush AlarmBrush = new SolidColorBrush(new Color(0xFF, 0xFF, 0x00, 0x00));

        public static SolidColorBrush OnIndicator = new SolidColorBrush(new Color(0xFF, 0xff, 0xFF, 0xff));
        public static SolidColorBrush OffIndicator = new SolidColorBrush(new Color(0xFF, 0x00, 0x00, 0x00));

        public const String INDNeoDeviceUSBDrive = "\\USB Hard Disk";
        public const String INDNeoSimulationUSBDrive = "E:\\";

        public const string ProjectNumber = "P18101204";

        private static int _decimals = 4;
        public static int Decimals
        {
            get { return _decimals; }
            set
            {
                _decimals = value;
                Log4NetManager.ApplicationLogger.Info("Decimals set to " + value);
            }
        }

        private static WellknownWeightUnit _currentWeightUnit = WellknownWeightUnit.Kilogram;
        public static WellknownWeightUnit CurrentWeightUnit
        {
            get { return _currentWeightUnit; }
            set
            {
                _currentWeightUnit = value;
                Decimals = (value == WellknownWeightUnit.Kilogram)
                    ? HomeScreen.Instance.ViewModel.Precision_kg
                    : Math.Max(HomeScreen.Instance.ViewModel.Precision_kg - 3, 0);
                Log4NetManager.ApplicationLogger.Info("WeightUnit set to " + value);
            }
        }

        public static string FormatWeight(double weight_Kg)
        {
            return String.Format("{0:n" + Decimals + "}", weight_Kg);
        }

        public static string FormatWeight_kg(double weight_kg)
        {
            return String.Format("{0:n" + HomeScreen.Instance.ViewModel.Precision_kg + "}", weight_kg);
        }

        public static string FormatWeightWithUnits(double weight_kg)
        {
            if (CurrentWeightUnit == WellknownWeightUnit.Kilogram)
                return String.Format("{0:n" + Decimals + "}" + " " + CurrentWeightUnit.ToAbbreviation(), weight_kg);
            else
                return String.Format("{0:n" + Decimals + "}" + " " + CurrentWeightUnit.ToAbbreviation(), weight_kg * 1000);
        }

        public static string FormatWeightUnit_g(double weight_kg)
        {
            int d = Decimals - 3;
            if (d < 0) d = 0;
            return String.Format("{0:n" + d + "} g", 1000 * weight_kg);
        }

        public static bool Double_TryParse(string value, out double result)
        {
            // simulates missing Double.TryParse in .NET35Compact
            result = 0;
            try
            {
                result = Double.Parse(value);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static string ProgVersionStr(bool withRevision)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            if (withRevision)
            {
                vers += "." + version.Revision.ToString();
#if DEBUG
                vers += " [DEBUG]";
#endif
            }

            return vers;
        }


        public enum MessageType
        {
            OverFilled,
            UnderFilled,
            EndSerie,
            Cancel,
            undefined
        }
    }
}
