﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using MT.LonzaPixon.Pages;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform;

namespace MT.LonzaPixon.Logic
{
    [Serializable]
    public class DosingParams : PropertyChangedBase
    {
        private string DosingParamFileName;

        private static readonly XmlSerializer serializer = new XmlSerializer(typeof(DosingParams));

        public static DosingParams Instance;
        private static bool _reading = false;
        private static bool _saving = false;


        public DosingParams()
        {
            SingularityEnvironment env = new SingularityEnvironment("MT.LonzaPixon");
            var dataPath = env.DataDirectory;
            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }
            DosingParamFileName = Path.Combine(dataPath, "DosingParams.xml");
        }

        private DosingParams _instanceCloned = null;
        [XmlIgnore]
        public DosingParams InstanceCloned
        {
            get { return _instanceCloned; }
            set { _instanceCloned = value; }
        }

        public void Clone()
        {
            InstanceCloned = new DosingParams();
            InstanceCloned.GebindeNr = GebindeNr;
            InstanceCloned.LastGebindeNr = LastGebindeNr;
            InstanceCloned.Sollwert = Sollwert;
            InstanceCloned.TaraMin = TaraMin;
            InstanceCloned.TaraMax = TaraMax;
            InstanceCloned.TolNeg = TolNeg;
            InstanceCloned.TolPos = TolPos;
            InstanceCloned.GrobFeinLimit = GrobFeinLimit;
            InstanceCloned.VorabschaltLimit = VorabschaltLimit;
        }

        public void LogValuesAndChanges()
        {
            Log4NetManager.ApplicationLogger.Info("DosingParams User: " + HomeScreen.Instance.ViewModel.Bediener);
            Log4NetManager.ApplicationLogger.Info("DosingParams GebindeNr: " + LastGebindeNr + (GebindeNr != InstanceCloned.GebindeNr ? " (changed from " + InstanceCloned.GebindeNr + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams LastGebindeNr: " + LastGebindeNr + (LastGebindeNr != InstanceCloned.LastGebindeNr ? " (changed from " + InstanceCloned.LastGebindeNr + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams Sollwert: " + Sollwert + (Sollwert != InstanceCloned.Sollwert ? " (changed from " + InstanceCloned.Sollwert + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams TaraMin: " + TaraMin + (TaraMin != InstanceCloned.TaraMin ? " (changed from " + InstanceCloned.TaraMin + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams TaraMax: " + TaraMax + (TaraMax != InstanceCloned.TaraMax ? " (changed from " + InstanceCloned.TaraMax + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams TolNeg: " + TolNeg + (TolNeg != InstanceCloned.TolNeg ? " (changed from " + InstanceCloned.TolNeg + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams TolPos: " + TolPos + (TolPos != InstanceCloned.TolPos ? " (changed from " + InstanceCloned.TolPos + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams GrobFeinLimit: " + GrobFeinLimit + (GrobFeinLimit != InstanceCloned.GrobFeinLimit ? " (changed from " + InstanceCloned.GrobFeinLimit + ")" : ""));
            Log4NetManager.ApplicationLogger.Info("DosingParams VorabschaltLimit: " + VorabschaltLimit + (VorabschaltLimit != InstanceCloned.VorabschaltLimit ? " (changed from " + InstanceCloned.VorabschaltLimit + ")" : ""));
        }

        public void LogValues()
        {
            Log4NetManager.ApplicationLogger.Info("DosingParams User: " + HomeScreen.Instance.ViewModel.Bediener);
            Log4NetManager.ApplicationLogger.Info("DosingParams GebindeNr: " + GebindeNr);
            Log4NetManager.ApplicationLogger.Info("DosingParams LastGebindeNr: " + LastGebindeNr);
            Log4NetManager.ApplicationLogger.Info("DosingParams Sollwert: " + Sollwert);
            Log4NetManager.ApplicationLogger.Info("DosingParams TaraMin: " + TaraMin);
            Log4NetManager.ApplicationLogger.Info("DosingParams TaraMax: " + TaraMax);
            Log4NetManager.ApplicationLogger.Info("DosingParams TolNeg: " + TolNeg);
            Log4NetManager.ApplicationLogger.Info("DosingParams TolPos: " + TolPos);
            Log4NetManager.ApplicationLogger.Info("DosingParams GrobFeinLimit: " + GrobFeinLimit);
            Log4NetManager.ApplicationLogger.Info("DosingParams VorabschaltLimit: " + VorabschaltLimit);
        }

        public void Save()
        {
            if (!_saving)
            {
                try
                {
                    try
                    {
                        _saving = true;
                        using (
                            FileStream fs = new FileStream(DosingParamFileName, FileMode.Create, FileAccess.Write,
                                FileShare.None))
                        {
                            try
                            {
                                serializer.Serialize(fs, this);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Error Serialize DosingParams: " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetManager.ApplicationLogger.Error("Error saving DosinParams", ex);
                    }
                }
                finally
                {
                    _saving = false;
                }
            }
        }

        public bool Read()
        {
            if (File.Exists(DosingParamFileName))
            {
                try
                {
                    try
                    {
                        _reading = true;
                        using (
                            FileStream fs = new FileStream(DosingParamFileName, FileMode.Open, FileAccess.Read,
                                FileShare.Read))
                        {
                            try
                            {
                                var res = (DosingParams)serializer.Deserialize(fs);
                                Instance = res;
                                Instance.LogValues();
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Error Deserialize DosingParams: " + ex.Message);
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetManager.ApplicationLogger.Error("Error reading DosingParams", ex);
                    }
                }
                finally
                {
                    _reading = false;
                }
            }
            return false;
        }

        protected void NotifyPropertyChanged(string propertyName = "")
        {
            if (!_reading && !_saving)
            {
                Save();
            }
            base.NotifyPropertyChanged(propertyName);
        }

        private int _gebindeNr = 0;
        public int GebindeNr
        {
            get { return _gebindeNr; }
            set
            {
                if (_gebindeNr != value)
                {
                    _gebindeNr = value;
                    if (value > 0)
                        GebindeNrFormated = value.ToString();
                    else
                        GebindeNrFormated = "-";
                }
            }
        }

        private int _lastGebindeNr = 0;
        public int LastGebindeNr
        {
            get { return _lastGebindeNr; }
            set
            {
                if (_lastGebindeNr != value)
                {
                    _lastGebindeNr = value;
                    NotifyPropertyChanged();
                }
            }
        }

        string gebindeNrFormated = "-";
        [XmlIgnore]
        public string GebindeNrFormated
        {
            get { return gebindeNrFormated; }
            set
            {
                if (gebindeNrFormated != value)
                {
                    gebindeNrFormated = value;
                    NotifyPropertyChanged("GebindeNrFormated");
                }
            }
        }

        private FillingState _lastFillState = FillingState.Idle;
        public FillingState LastFillState
        {
            get { return _lastFillState; }
            set
            {
                if (_lastFillState != value)
                {
                    _lastFillState = value;
                    NotifyPropertyChanged("LastFillState");
                }
            }
        }

        private double _nettoSum = 0;
        public double NettoSum
        {
            get { return _nettoSum; }
            set
            {
                if (Globals.FormatWeightWithUnits(value) != NettoSumFormatted)
                {
                    _nettoSum = value;
                    NettoSumFormatted = Globals.FormatWeightWithUnits(_nettoSum);
                    NotifyPropertyChanged("NettoSum");
                }
            }
        }

        private string _nettoSumFormatted = "";
        [XmlIgnore]
        public string NettoSumFormatted
        {
            get { return _nettoSumFormatted; }
            set
            {
                if (_nettoSumFormatted != value)
                {
                    _nettoSumFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        NettoSum = 0;
                    }
                    else
                    {
                        try
                        {
                            NettoSum = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                NotifyPropertyChanged("NettoSumFormatted");
            }
        }

        private double _istWert = 0;
        [XmlIgnore]
        public double IstWert
        {
            get { return _istWert; }
            set
            {
                if (Math.Abs(_istWert - value) > 0.000001)
                {
                    _istWert = value;
                    NotifyPropertyChanged("IstWert");
                }
            }
        }

        private string _sollwertFormatted = "";
        [XmlIgnore]
        public string SollwertFormatted
        {
            get { return _sollwertFormatted; }
            set
            {
                if (_sollwertFormatted != value)
                {
                    _sollwertFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        Sollwert = 0;
                    }
                    else
                    {
                        try
                        {
                            Sollwert = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    NotifyPropertyChanged("SollwertFormatted");
                }
            }
        }

        private string _sollwertFormattedWithkg = "";
        [XmlIgnore]
        public string SollwertFormattedWithkg
        {
            get { return _sollwertFormattedWithkg; }
            set
            {
                if (_sollwertFormattedWithkg != value)
                {
                    _sollwertFormattedWithkg = value;
                    NotifyPropertyChanged("SollwertFormattedWithkg");
                }
            }
        }

        private double _sollwert = 0;
        public double Sollwert
        {
            get { return _sollwert; }
            set
            {
                if (Globals.FormatWeight_kg(_sollwert) != _sollwertFormatted)
                {
                    //if (!_reading)
                    //Log4NetManager.ApplicationLogger.Info("User " + HomeScreen.Instance.ViewModel.SecurityService.CurrentUser.Name + ", DosingParam Sollwert changed " + _sollwert + " -> " + value);
                    _sollwert = value;
                    SollwertFormatted = Globals.FormatWeight_kg(value);
                    SollwertFormattedWithkg = SollwertFormatted + " kg";
                    NotifyPropertyChanged("Sollwert");
                }
            }
        }

        private string _taraMinFormatted = "";
        [XmlIgnore]
        public string TaraMinFormatted
        {
            get { return _taraMinFormatted; }
            set
            {
                if (_taraMinFormatted != value)
                {
                    _taraMinFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        TaraMin = 0;
                    }
                    else
                    {
                        try
                        {
                            TaraMin = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                NotifyPropertyChanged("TaraMinFormatted");
            }
        }

        private double _taraMin = 0;
        public double TaraMin
        {
            get { return _taraMin; }
            set
            {
                if (Globals.FormatWeight_kg(value) != _taraMinFormatted)
                {
                    _taraMin = value;
                    TaraMinFormatted = Globals.FormatWeight_kg(value);
                    NotifyPropertyChanged("TaraMin");
                }
            }
        }

        private string _taraMaxFormatted = "";
        [XmlIgnore]
        public string TaraMaxFormatted
        {
            get { return _taraMaxFormatted; }
            set
            {
                if (_taraMaxFormatted != value)
                {
                    _taraMaxFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        TaraMax = 0;
                    }
                    else
                    {
                        try
                        {
                            TaraMax = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }

                    }
                    NotifyPropertyChanged("TaraMaxFormatted");
                }
            }
        }

        private double _taraMax = 0;
        public double TaraMax
        {
            get { return _taraMax; }
            set
            {
                if (Globals.FormatWeight_kg(value) != _taraMaxFormatted)
                {
                    _taraMax = value;
                    TaraMaxFormatted = Globals.FormatWeight_kg(value);
                    NotifyPropertyChanged("TaraMax");
                }
            }
        }

        private string _tolNegFormatted = "";
        [XmlIgnore]
        public string TolNegFormatted
        {
            get { return _tolNegFormatted; }
            set
            {
                if (_tolNegFormatted != value)
                {
                    _tolNegFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        TolNeg = 0;
                    }
                    else
                    {
                        try
                        {
                            TolNeg = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    NotifyPropertyChanged("TolNegFormatted");
                }
            }
        }

        private double _tolNeg = 0;
        public double TolNeg
        {
            get { return _tolNeg; }
            set
            {
                if (Globals.FormatWeight_kg(value) != _tolNegFormatted)
                {
                    _tolNeg = value;
                    TolNegFormatted = Globals.FormatWeight_kg(value);
                    NotifyPropertyChanged("TolNeg");
                }
            }
        }

        private string _tolPosFormatted = "";
        [XmlIgnore]
        public string TolPosFormatted
        {
            get { return _tolPosFormatted; }
            set
            {
                if (_tolPosFormatted != value)
                {
                    _tolPosFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        TolPos = 0;
                    }
                    else
                    {
                        try
                        {
                            TolPos = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    NotifyPropertyChanged("TolPosFormatted");
                }
            }
        }

        private double _tolPos = 0;
        public double TolPos
        {
            get { return _tolPos; }
            set
            {
                if (Globals.FormatWeight_kg(value) != _tolPosFormatted)
                {
                    _tolPos = value;
                    TolPosFormatted = Globals.FormatWeight_kg(value);
                    NotifyPropertyChanged("TolPos");
                }
            }
        }

        private string _grobFeinLimitFormatted = "";
        [XmlIgnore]
        public string GrobFeinLimitFormatted
        {
            get { return _grobFeinLimitFormatted; }
            set
            {
                if (_grobFeinLimitFormatted != value)
                {
                    _grobFeinLimitFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        GrobFeinLimit = 0;
                    }
                    else
                    {
                        try
                        {
                            GrobFeinLimit = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    NotifyPropertyChanged("GrobFeinLimitFormatted");
                }
            }
        }

        private double _grobFeinLimit = 0;
        public double GrobFeinLimit
        {
            get { return _grobFeinLimit; }
            set
            {
                if (Globals.FormatWeight_kg(value) != _grobFeinLimitFormatted)
                {
                    _grobFeinLimit = value;
                    GrobFeinLimitFormatted = Globals.FormatWeight_kg(value);
                    NotifyPropertyChanged("GrobFeinLimit");
                }
            }
        }

        private string _vorabschaltLimitFormatted = "";
        [XmlIgnore]
        public string VorabschaltLimitFormatted
        {
            get { return _vorabschaltLimitFormatted; }
            set
            {
                if (_vorabschaltLimitFormatted != value)
                {
                    _vorabschaltLimitFormatted = value;
                    if (String.IsNullOrEmpty(value))
                    {
                        VorabschaltLimit = 0;
                    }
                    else
                    {
                        try
                        {
                            VorabschaltLimit = Double.Parse(value);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    NotifyPropertyChanged("VorabschaltLimitFormatted");
                }
            }
        }

        private double _vorabschaltLimit = 0;
        public double VorabschaltLimit
        {
            get { return _vorabschaltLimit; }
            set
            {
                if (Globals.FormatWeight_kg(value) != _vorabschaltLimitFormatted)
                {
                    _vorabschaltLimit = value;
                    VorabschaltLimitFormatted = Globals.FormatWeight_kg(value);
                    NotifyPropertyChanged("VorabschaltLimit");
                    NotifyPropertyChanged("VorabschaltLimitFormatted");
                }
            }
        }

    }
}
