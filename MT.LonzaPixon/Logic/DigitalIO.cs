﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;

namespace MT.LonzaPixon.Logic
{
    public class DigitalIO : PropertyChangedBase
    {

        public IDigitalIOInterface _digitalIo;

        public DigitalIO(IDigitalIOInterface digitalIo)
        {
            _digitalIo = digitalIo;
            Initialize();
        }

        private static DigitalIO _instance = null;
        public static DigitalIO Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }

        private void Initialize()
        {
            if (_digitalIo == null)
            {
                Log4NetManager.ApplicationLogger.Error("Error initializing Digital IO");
                Debug.WriteLine("Error on initializing Digital IO");
            }
            else
            {
                _digitalIo.InputChanged += _digitalIo_InputChanged;
            }
        }

        #region Input #############################################################################

        void _digitalIo_InputChanged(int index, bool newValue)
        {
            switch (index)
            {
                case 0:
                    Input1 = newValue;
                    break;
                case 1:
                    Input2 = newValue;
                    break;
                case 2:
                    Input3 = newValue;
                    break;
                case 3:
                    Input4 = newValue;
                    break;
            }
        }

        public void ReadInputDigitalIO()
        {
            if (_digitalIo == null)
                return;
            
            Input1 = _digitalIo.Inputs[0];
            Input2 = _digitalIo.Inputs[1];
            Input3 = _digitalIo.Inputs[2];
            Input4 = _digitalIo.Inputs[3];
            Debug.WriteLine("Read digital inputs: " + (Input1 ? "1" : "0") + (Input2 ? "1" : "0") + (Input3 ? "1" : "0") + (Input4 ? "1" : "0")+" "+DateTime.Now.ToString("HH:mm:ss"));
        }

        private bool _input1;
        public bool Input1
        {
            get { return _input1; }
            set
            {
                if (_input1 != value)
                {
                    _input1 = value;
                    OnEventDigitalInChanged(new EventDigitalInChangedArgs(
                        new bool[] { Input1, Input2, Input3, Input4 },
                        new bool[] { true, false, false, false }));
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _input2;
        public bool Input2
        {
            get { return _input2; }
            set
            {
                if (_input2 != value)
                {
                    _input2 = value;
                    OnEventDigitalInChanged(new EventDigitalInChangedArgs(
                        new bool[] { Input1, Input2, Input3, Input4 },
                        new bool[] { false, true, false, false }));
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _input3;
        public bool Input3
        {
            get { return _input3; }
            set
            {
                if (_input3 != value)
                {
                    _input3 = value;
                    OnEventDigitalInChanged(new EventDigitalInChangedArgs(
                        new bool[] { Input1, Input2, Input3, Input4 },
                        new bool[] { false, false, true, false }));
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _input4;
        public bool Input4
        {
            get { return _input4; }
            set
            {
                if (_input4 != value)
                {
                    _input4 = value;
                    OnEventDigitalInChanged(new EventDigitalInChangedArgs(
                        new bool[] { Input1, Input2, Input3, Input4 },
                        new bool[] { false, false, false, true }));
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Ouput ############################################################################

        private bool _output1;
        public bool Output1
        {
            get { return _output1; }
            set
            {
                if (_output1 != value)
                {
                    _output1 = value;
                    OnEventDigitalOutChanged(new EventDigitalOutChangedArgs(
                        new bool[] { Output1, Output2, Output3, Output4 },
                        new bool[] { true, false, false, false }));
                    Task.Run(async () => { return await WriteOutputDigitalIO(); });
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _output2;
        public bool Output2
        {
            get { return _output2; }
            set
            {
                if (_output2 != value)
                {
                    _output2 = value;
                    OnEventDigitalOutChanged(new EventDigitalOutChangedArgs(
                        new bool[] { Output1, Output2, Output3, Output4 },
                        new bool[] { false, true, false, false }));
                    Task.Run(async () => { return await WriteOutputDigitalIO(); });
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _output3;
        public bool Output3
        {
            get { return _output3; }
            set
            {
                if (_output3 != value)
                {
                    _output3 = value;
                    OnEventDigitalOutChanged(new EventDigitalOutChangedArgs(
                        new bool[] { Output1, Output2, Output3, Output4 },
                        new bool[] { false, false, true, false }));
                    Task.Run(async () => { return await WriteOutputDigitalIO(); });
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _output4;
        public bool Output4
        {
            get { return _output4; }
            set
            {
                if (_output4 != value)
                {
                    _output4 = value;
                    OnEventDigitalOutChanged(new EventDigitalOutChangedArgs(
                        new bool[] { Output1, Output2, Output3, Output4 },
                        new bool[] { false, false, false, true }));
                    Task.Run(async () => { return await WriteOutputDigitalIO(); });
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        public async Task<bool> WriteOutputDigitalIO()
        {
            if (_digitalIo == null)
                return false;

            var array = new[] { Output1, Output2, Output3, Output4 };

            if (array.Length > this._digitalIo.NumberOfOutputs)
            {
                Debug.WriteLine("Error: Number of inputs does not match");
                return false;
            }

            return await this._digitalIo.WriteOutputAsync(array);
        }

        #region Events ############################################################################

        #region Event digital in ##################################################################

        public class EventDigitalInChangedArgs : EventArgs
        {
            public bool[] Inputs { get; set; }
            public bool[] InputsChanged { get; set; }

            public EventDigitalInChangedArgs(bool[] inputs, bool[] inputsChanged)
            {
                Inputs = inputs;
                InputsChanged = inputsChanged;
            }
        }

        public event EventHandler<EventDigitalInChangedArgs> EventDigitalInChanged;

        protected virtual void OnEventDigitalInChanged(EventDigitalInChangedArgs e)
        {
            if (EventDigitalInChanged != null)
            {
                EventDigitalInChanged(this, e);
            }
        }

        #endregion
        #region Event digital out #################################################################

        public event EventHandler<EventDigitalOutChangedArgs> EventDigitalOutChanged;

        protected virtual void OnEventDigitalOutChanged(EventDigitalOutChangedArgs e)
        {
            if (EventDigitalOutChanged != null)
            {
                EventDigitalOutChanged(this, e);
            }
        }

        public class EventDigitalOutChangedArgs : EventArgs
        {
            public bool[] Outputs { get; set; }
            public bool[] OutputsChanged { get; set; }

            public EventDigitalOutChangedArgs(bool[] outputs, bool[] outputsChanged)
            {
                Outputs = outputs;
                OutputsChanged = outputsChanged;
            }
        }

        #endregion

        #endregion


    }
}
