﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MT.LonzaPixon.Logic
{
    public class ButtonLogic
    {
        Button button;
        Timer delayTimer;
        Timer repeatTimer;
        public ButtonLogic(Button Button)
        {
            button = Button;
            button.Click += button_Click;
            button.PointerDown += button_PointerDown;
            button.PointerUp += button_PointerUp;
            delayTimer = new Timer();
            repeatTimer = new Timer();
        }



        void delayTimer_Tick(object sender, EventArgs e)
        {
            delayTimer.Enabled = false;
            delayTimer.Tick -= delayTimer_Tick;
            repeatTimer.Interval = 100;
            repeatTimer.Tick += repeatTimer_Tick;
            repeatTimer.Enabled = true;
            OnKeyEvents(new KeyEventsEventArgs(KeystrokeType.LongClick));
        }

        void repeatTimer_Tick(object sender, EventArgs e)
        {
            OnKeyEvents(new KeyEventsEventArgs(KeystrokeType.Repeater));
            Debug.WriteLine(DateTime.Now.ToString("mm:ss.fff"));
        }

        void button_PointerUp(object sender, Singularity.Presentation.Input.PointerEventArgs e)
        {
            delayTimer.Enabled = false;
            repeatTimer.Enabled = false;
            repeatTimer.Tick -= repeatTimer_Tick;
            delayTimer.Tick -= delayTimer_Tick;
            OnKeyEvents(new KeyEventsEventArgs(KeystrokeType.KeyUp));
        }

        void button_PointerDown(object sender, Singularity.Presentation.Input.PointerEventArgs e)
        {
            OnKeyEvents(new KeyEventsEventArgs(KeystrokeType.KeyDown));
            delayTimer.Interval = 500;
            delayTimer.Enabled = false;
            delayTimer.Tick += delayTimer_Tick;
            delayTimer.Enabled = true;
        }

        void button_Click(object sender, Singularity.Presentation.Input.HandledEventArgs e)
        {
            if (delayTimer.Enabled == true)
                OnKeyEvents(new KeyEventsEventArgs(KeystrokeType.ShortClick));
        }


        /// <summary>
        /// Occures when the event is raised
        /// </summary>
        public event EventHandler<KeyEventsEventArgs> KeyEvents;

        /// <summary>
        /// Raises the <see cref="KeyEvents"/> event
        /// </summary>
        /// <param name="e">An <see cref="KeyEventsEventArgs"/> that contains the event data.</param>
        protected virtual void OnKeyEvents(KeyEventsEventArgs e)
        {
            if (KeyEvents != null)
                KeyEvents(this, e);
        }

        /// <summary>
        /// Contains all information related to a KeyEvents event
        /// </summary>
        public class KeyEventsEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="KeyEvents"/> class
            /// </summary>
            /// 
            public KeystrokeType KeyStrokeType { get; set; }
            public KeyEventsEventArgs(KeystrokeType KeyStrokeType)
            {
                this.KeyStrokeType = KeyStrokeType;
            }
        }

        public enum KeystrokeType
        {
            Repeater,
            ShortClick,
            LongClick,
            KeyDown,
            KeyUp
        }


    }
}
