﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using MT.LonzaPixon.Config;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Infrastructure;

namespace MT.LonzaPixon.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        /// <summary>
        /// Initializes the application in this method. This method is called after all
        /// services have been started and before the <see cref="T:MT.Singularity.Platform.Infrastructure.IShell" /> is shown.
        /// </summary>
        protected override async void InitializeApplication()
        {
            base.InitializeApplication();
            await InitializeCustomerService();
        }

        /// <summary>
        /// Initializes the customer service.
        /// Put all customer services or components initialization code in this method.
        /// </summary>
        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new LonzaPixonComponents(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<ILonzaPixonComponents>(customerComponent);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);
                //throw;
            }
        }
    }
}
