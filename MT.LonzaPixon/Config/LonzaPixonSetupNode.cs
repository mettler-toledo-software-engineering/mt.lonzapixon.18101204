﻿using System;
using System.Diagnostics;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace MT.LonzaPixon.Config
{
    [Export(typeof(CustomerGroupSetupMenuItem))]
    public class LonzaPixonSetupNode : CustomerGroupSetupMenuItem
    {
        public LonzaPixonSetupNode(SetupMenuContext context)
            : base(
                context,
                new TitleAndSubtitle(
                    Localization.GetTranslationModule(),
                    (int)Localization.Key.MainSetupNode))
        {
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public async override System.Threading.Tasks.Task ShowChildrenAsync()
        {
            try
            {
                var customerComponent = _context.CompositionContainer.Resolve<ILonzaPixonComponents>();
                LonzaPixonConfiguration customerConfiguration = await customerComponent.GetConfigurationToChangeAsync();
                Children =
                    Indexable.ImmutableValues<SetupMenuItem>(
                        new LonzaPixonWeightParamSubNode(_context, customerComponent, customerConfiguration),
                        new LonzaPixonTerminalSubNode(_context, customerComponent,customerConfiguration));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
