﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace MT.LonzaPixon.Config
{
    [Export(typeof(ILonzaPixonComponents))]
    [Export(typeof(IConfigurable))]
    [InjectionBehavior(IsSingleton = true)]
    public class LonzaPixonComponents : ConfigurationStoreConfigurable<LonzaPixonConfiguration>, ILonzaPixonComponents
    {
        public LonzaPixonComponents(IConfigurationStore configurationStore, 
            ISecurityService securityService, CompositionContainer compositionContainer, bool traceChanges = true)
            : base(configurationStore, securityService, compositionContainer, traceChanges)
        {
        }

        public override string ConfigurationSelector
        {
            get { return "LonzaPixonConfiguration"; }
        }

        public override string FriendlyName
        {
            get { return "LonzaPixon Configuration"; }
        }

    }
}
