﻿using System;
using System.ComponentModel;
using MT.LonzaPixon.Logic;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Config
{
    [Component]
    public partial class LonzaPixonConfiguration : ComponentConfiguration
    {
        public LonzaPixonConfiguration()
        {
            AnimationItems = AnimationItemStrings;
            LernmodusItems = LernModusItemStrings;
            PumpDirectionItems = PumpDirectionEnumItemStrings;
            PrecisionAndUnitItems = PrecisionAndUnitStrings;
        }

        #region Dosierparameter ###############################################

        // configuratin members
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(50)] // %
        public virtual int VorabschaltKorr
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(0)]
        public virtual LernModusEnum Lernmodus
        {
            get { return (LernModusEnum)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(5)] // s
        public virtual int Beruhigungszeit
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(80)] // %
        public virtual int GrobStrom
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(30)] // %
        public virtual int FeinStrom
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(false)]
        public virtual bool HWSimulation
        {
            get { return (bool)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(false)]
        public virtual bool EndschalterIgnore
        {
            get { return (bool)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("0.0000 kg")]
        public virtual string PrecisionAndUnit
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        #endregion

        #region Summierung, Stati #############################################

        #endregion

        private IImmutableIndexable<TransitionAnimation> _animationItems;
        public IImmutableIndexable<TransitionAnimation> AnimationItems
        {
            get { return _animationItems; }
            set
            {
                if (!Equals(_animationItems, value))
                {
                    _animationItems = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }
        }

        private IImmutableIndexable<PumpDirectionEnum> _pumpDirectionItems;
        public IImmutableIndexable<PumpDirectionEnum> PumpDirectionItems
        {
            get { return _pumpDirectionItems; }
            set
            {
                if (!Equals(_pumpDirectionItems, value))
                {
                    _pumpDirectionItems = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }
        }

        private IImmutableIndexable<string> _precisionAndUnitItems;
        public IImmutableIndexable<string> PrecisionAndUnitItems
        {
            get { return _precisionAndUnitItems; }
            set
            {
                if (!Equals(_precisionAndUnitItems, value))
                {
                    _precisionAndUnitItems = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }
        }

        private IImmutableIndexable<LernModusEnum> _lernmodusItems;
        public IImmutableIndexable<LernModusEnum> LernmodusItems
        {
            get { return _lernmodusItems; }
            set
            {
                if (!Equals(_lernmodusItems, value))
                {
                    _lernmodusItems = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("192.168.174.109")]
        public virtual string PrinterIP
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(9100)]
        public virtual int PrinterPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(TransitionAnimation.None)]
        public virtual TransitionAnimation Animation
        {
            get { return (TransitionAnimation)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(PumpDirectionEnum.Right_Clockwise)]
        public virtual PumpDirectionEnum PumpDirection
        {
            get { return (PumpDirectionEnum)GetLocal(); }
            set { SetLocal(value); }
        }

        public static readonly IImmutableIndexable<PumpDirectionEnum> PumpDirectionEnumItemStrings = Indexable.ImmutableValues
            (
                PumpDirectionEnum.Right_Clockwise,
                PumpDirectionEnum.Left_Counterclockwise
            );

        public static readonly IImmutableIndexable<string> PrecisionAndUnitStrings = Indexable.ImmutableValues
            (
                "0.0000 kg", // kg, 4 Stellen  resolution   0.1g unit kg
                "0.0 g",     // g,  1 Stelle   resolution   0.1g unit g
                "0.000 kg",  // kg, 3 Stellen  resolution   1  g unit kg
                "0 g",       // g,  0 Stellen  resolution   1  g unit g
                "0.00 kg",   // kg, 2 Stellen  resolution  10g   unit kg
                "0.0 kg"     // kg, 1 Stelle   resolution 100g   unit kg
            );

        private static readonly IImmutableIndexable<TransitionAnimation> AnimationItemStrings = Indexable.ImmutableValues
                        (
                            TransitionAnimation.PushRight,
                            TransitionAnimation.None,
                            TransitionAnimation.PushLeft,
                            TransitionAnimation.PushBottom,
                            TransitionAnimation.PushTop,
                            TransitionAnimation.PopRight,
                            TransitionAnimation.PopLeft,
                            TransitionAnimation.PopBottom,
                            TransitionAnimation.PopTop,
                            TransitionAnimation.ReplaceRight,
                            TransitionAnimation.RightToLeft,
                            TransitionAnimation.LeftToRight,
                            TransitionAnimation.ReplaceLeft,
                            TransitionAnimation.ReplaceBottom,
                            TransitionAnimation.BottomToTop,
                            TransitionAnimation.BottomRightToTopLeft,
                            TransitionAnimation.BottomLeftToTopRight,
                            TransitionAnimation.TopToBottom,
                            TransitionAnimation.ReplaceTop,
                            TransitionAnimation.TopRightToBottomLeft,
                            TransitionAnimation.TopLeftToBottomRight
                        );

        public enum LernModusEnum
        {
            Inaktiv,
            Aktiv
        }

        private static readonly IImmutableIndexable<LernModusEnum> LernModusItemStrings = Indexable.ImmutableValues
        (
        LernModusEnum.Inaktiv,
        LernModusEnum.Aktiv
        );

    }

}
