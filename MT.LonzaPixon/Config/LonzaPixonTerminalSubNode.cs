﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace MT.LonzaPixon.Config
{
    class LonzaPixonTerminalSubNode : GroupSetupMenuItem
    {
        private readonly LonzaPixonConfiguration _configuration;
        private readonly ILonzaPixonComponents _customerComponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="MySubNodeOne"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="customerComponent">The customer component.</param>
        /// <param name="configuration">The configuration.</param>
        public LonzaPixonTerminalSubNode(
            SetupMenuContext context,
            ILonzaPixonComponents customerComponent,
            LonzaPixonConfiguration configuration)
            : base(context,
                new TitleAndSubtitle(
                    Localization.GetTranslationModule(),
                    (int)Localization.Key.TerminalSubNode),
                configuration,
                customerComponent)
        {
            _customerComponent = customerComponent;
            _configuration = configuration;
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override Task ShowChildrenAsync()
        {
            try
            {
                // Titles for the my Setup parameters
                var animationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.Animation);
                var simulationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.HWSimulation);
                var endSchalterIgnoreTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.EndschalterIgnore);
                var printerIPTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterIP);
                var printerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterPort);

                var animationTarget = new DropDownSetupMenuItem(_context, animationTitle, _configuration, "AnimationItems", "Animation");
                var simulationTarget=new CheckBoxSetupMenuItem(_context,simulationTitle,_configuration,"HWSimulation");
                var endSchalterIgnoreTarget = new CheckBoxSetupMenuItem(_context, endSchalterIgnoreTitle, _configuration, "EndschalterIgnore");
                var printerIPTarget = new TextSetupMenuItem(_context, printerIPTitle, _configuration, "PrinterIP");
                var printerPortTarget = new TextSetupMenuItem(_context, printerPortTitle, _configuration, "PrinterPort");

                var rangeGroup1 = new GroupedSetupMenuItems(_context, animationTarget, simulationTarget, endSchalterIgnoreTarget);
                var rangeGroup2 = new GroupedSetupMenuItems(_context, printerIPTarget, printerPortTarget);

                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error TerminalSubnode.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }
    }
}
