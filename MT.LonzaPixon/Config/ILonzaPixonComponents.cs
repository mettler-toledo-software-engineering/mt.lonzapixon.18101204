﻿namespace MT.LonzaPixon.Config
{
    using MT.Singularity.Platform.Configuration;

    /// <summary>
    ///  
    /// </summary>
    public interface ILonzaPixonComponents : IConfigurable<LonzaPixonConfiguration>
    {
    }
}
