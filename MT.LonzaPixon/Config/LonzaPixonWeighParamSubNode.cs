﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace MT.LonzaPixon.Config
{
    class LonzaPixonWeightParamSubNode : GroupSetupMenuItem
    {
        private readonly LonzaPixonConfiguration _configuration;
        private readonly ILonzaPixonComponents _customerComponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="MySubNodeOne"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="customerComponent">The customer component.</param>
        /// <param name="configuration">The configuration.</param>
        public LonzaPixonWeightParamSubNode(
            SetupMenuContext context,
            ILonzaPixonComponents customerComponent,
            LonzaPixonConfiguration configuration)
            : base(context,
                new TitleAndSubtitle(
                    Localization.GetTranslationModule(),
                    (int)Localization.Key.WeightParamSubNode),
                configuration,
                customerComponent)
        {
            _customerComponent = customerComponent;
            _configuration = configuration;
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override Task ShowChildrenAsync()
        {
            try
            {
                // Titles for the my Setup parameters
                var lernmodusTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.Lernmodus);
                var pumpDirectionTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DirRotation);
                var PrecisionAndUnitTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrecisionAndUnit);

                var beruhigungszeitTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.Beruhigungszeit);
                var grobStromTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GrobStrom);
                var feinStromTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.FeinStrom);
                var vorabschaltKorrTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.VorabschaltKorr);

                var lernmodusTarget = new DropDownSetupMenuItem(_context, lernmodusTitle, _configuration, "LernmodusItems", "Lernmodus");
                var pumpDirectionTarget = new DropDownSetupMenuItem(_context, pumpDirectionTitle, _configuration, "PumpDirectionItems", "PumpDirection");
                var precisionAndUnitTarget = new DropDownSetupMenuItem(_context, PrecisionAndUnitTitle, _configuration, "PrecisionAndUnitItems", "PrecisionAndUnit");
                var beruhigungszeitTarget = new TextSetupMenuItem(_context, beruhigungszeitTitle, _configuration, "Beruhigungszeit");
                var grobStromTarget = new TextSetupMenuItem(_context, grobStromTitle, _configuration, "GrobStrom");
                var feinStromTarget = new TextSetupMenuItem(_context, feinStromTitle, _configuration, "FeinStrom");
                var vorabschaltKorrTarget = new TextSetupMenuItem(_context, vorabschaltKorrTitle, _configuration, "VorabschaltKorr");

                var rangeGroup1 = new GroupedSetupMenuItems(_context, lernmodusTarget, pumpDirectionTarget, precisionAndUnitTarget);
                var rangeGroup2 = new GroupedSetupMenuItems(_context, grobStromTarget, feinStromTarget);
                var rangeGroup3 = new GroupedSetupMenuItems(_context, vorabschaltKorrTarget, beruhigungszeitTarget);

                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2, rangeGroup3);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error WeightParamsSubnode.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }
    }
}
