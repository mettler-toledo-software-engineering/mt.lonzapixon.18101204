﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.LonzaPixon.Pages
{
    /// <summary>
    /// Interaction logic for USBUpdatePage
    /// </summary>
    public partial class USBUpdatePage
    {
        USBUpdatePageViewModel viewModel;

        public USBUpdatePage()
        {
            viewModel = new USBUpdatePageViewModel(this);
            InitializeComponents();
        }

        public ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        void DoGoBack()
        {
            HomeScreen.Instance.NavigationFrame.Back();
        }

    }
}
