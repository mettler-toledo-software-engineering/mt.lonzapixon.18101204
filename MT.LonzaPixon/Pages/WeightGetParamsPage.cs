﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using MT.LonzaPixon.Logic;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.LonzaPixon.Pages
{
    /// <summary>
    /// Interaction logic for WeightSeriePage
    /// </summary>
    public partial class WeightGetParamsPage
    {
        private WeightGetParamsPageViewModel viewModel;
        public WeightGetParamsPageViewModel ViewModel { get { return viewModel; } }

        public static WeightGetParamsPage Instance;

        public WeightGetParamsPage()
        {
            viewModel = new WeightGetParamsPageViewModel();
            InitializeComponents();
            viewModel.InitEnableControls();
            Instance = this;
            DosingParams.Instance.Clone();
        }
        protected override void OnFirstNavigation()
        {
            HomeScreen.Instance.ViewModel.WindowTitle = viewModel.WindowTitle();
            HomeScreen.Instance.ViewModel.StatusText = "";
            base.OnFirstNavigation();
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreen.Instance.ViewModel.StatusTextVisibility = Visibility.Collapsed;
            Task.Run(async () =>
            {
                base.OnNavigationReturning(previousPage);
                HomeScreen.Instance.ViewModel.WindowTitle = viewModel.WindowTitle();
                HomeScreen.Instance.ViewModel.StatusText = "";
                DigitalIOLonza.InstanceLonza.StartTasterLED = false;
            });
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            HomeScreen.Instance.ViewModel.IsFillParamsNotRunning = true;
            DosingParams.Instance.LogValuesAndChanges();
            viewModel.RemoveUserChangeEvent();
            return base.OnNavigatingBack(nextPage);
        }
    }
}
