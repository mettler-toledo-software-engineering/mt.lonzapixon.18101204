﻿using System;
using System.Threading.Tasks;
using MT.LonzaPixon.Config;
using MT.LonzaPixon.Logic;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.LonzaPixon.Pages
{
    public class WeightGetParamsPageViewModel : PropertyChangedBase
    {
        private FlexiconPump _pump { get { return FlexiconPump.Instance; } }

        public WeightGetParamsPageViewModel()
        {
            Task.Run(async () =>
            {
                return await InitializeViewModel();
            });
        }

        private async Task<bool> InitializeViewModel()
        {
            // catch user login event
            HomeScreen.Instance.ViewModel.SecurityService.CurrentUserChanged += _securityService_CurrentUserChanged;
            _securityService_CurrentUserChanged(HomeScreen.Instance.ViewModel.SecurityService.CurrentUser); 
            EnableLearnModeFields = HomeScreen.Instance.ViewModel.Configuration.Lernmodus == LonzaPixonConfiguration.LernModusEnum.Inaktiv;
            return true;
        }

        void _securityService_CurrentUserChanged(User user)
        {
            // force change of member to ensure GUI-Update
            EnableControls = HomeScreen.Instance.ViewModel.SecurityService.CurrentUser.Permission == Permissions.Operator;
            // Disable for Operator
            EnableControls = HomeScreen.Instance.ViewModel.SecurityService.CurrentUser.Permission != Permissions.Operator;
        }

        private bool _enableControls = false;
        public bool EnableControls
        {
            get { return _enableControls; }
            set
            {
                if (_enableControls != value)
                {
                    _enableControls = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public void RemoveUserChangeEvent()
        {
            HomeScreen.Instance.ViewModel.SecurityService.CurrentUserChanged -= _securityService_CurrentUserChanged;
        }


        // to call after yml-Initialization
        public void InitEnableControls()
        {
        }

        private bool _enableLearnModeFields = false;
        public bool EnableLearnModeFields
        {
            get { return _enableLearnModeFields; }
            set
            {
                if (_enableLearnModeFields != value)
                {
                    _enableLearnModeFields = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _statusText = "";
        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText != value)
                {
                    _statusText = value;
                    HomeScreen.Instance.ViewModel.SetStatusText(value);
                    NotifyPropertyChanged();
                }
            }
        }


        public LonzaPixonConfiguration Configuration
        {
            get { return HomeScreen.Instance.ViewModel.Configuration; }
        }

        private bool _onFocusKeyboard = true;
        public bool OnFocusKeyboard
        {
            get { return _onFocusKeyboard; }
            set
            {
                if (_onFocusKeyboard != value)
                {
                    if (value)
                    {
                        double d;
                        string s;

                        try
                        {
                            DosingParams.Instance.Sollwert = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.Sollwert));
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            DosingParams.Instance.TaraMin = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.TaraMin));
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            DosingParams.Instance.TaraMax = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.TaraMax));
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            DosingParams.Instance.TolNeg = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.TolNeg));
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            DosingParams.Instance.TolPos = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.TolPos));
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            DosingParams.Instance.GrobFeinLimit = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.GrobFeinLimit));
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            DosingParams.Instance.VorabschaltLimit = Double.Parse(Globals.FormatWeight_kg(DosingParams.Instance.VorabschaltLimit));
                        }
                        catch (Exception ex)
                        {
                        }

                        DosingParams.Instance.Save();

                    }
                    else
                    {
                    }
                    _onFocusKeyboard = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public String WindowTitle()
        {
            return String.Format("{0}", Localization.Get(Localization.Key.DosierParams));
        }


        public ICommand Continue
        {
            get { return new DelegateCommand(DoContinue); }
        }

        public async void DoContinue()
        {
        }

        public ICommand Back
        {
            get { return new DelegateCommand(DoBack); }
        }

        public async void DoBack()
        {
            StatusText = "Set pump off";
            if (_pump.IsOpen)
            {
                _pump.PumpOff(true);
            }
            HomeScreen.Instance.NavigationFrame.Back();
        }
    }
}
