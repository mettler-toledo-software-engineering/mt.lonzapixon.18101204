﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using MT.LonzaPixon.Logic;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.LonzaPixon.Pages
{
    public class USBUpdatePageViewModel : PropertyChangedBase
    {
        private Visual _visual;
        private string _uSBPath = "";

        public USBUpdatePageViewModel(Visual visual)
        {
            _visual = visual;

            if (Directory.Exists(Globals.INDNeoDeviceUSBDrive))
            {
                _uSBPath = Globals.INDNeoDeviceUSBDrive;
                var fileList = Directory.GetFiles(Globals.INDNeoDeviceUSBDrive);
                foreach (var filename in fileList)
                {
                    if (filename.ToLower().Contains(SingularityEnvironment.AppPackageExtension))
                    {
                        UpdateFileList.Add(filename);
                    }
                }
            }
            else
            {
                if (Directory.Exists(Globals.INDNeoSimulationUSBDrive))
                {
                    _uSBPath = Globals.INDNeoSimulationUSBDrive;
                    try
                    {
                        var fileList = Directory.GetFiles(Globals.INDNeoSimulationUSBDrive);
                        foreach (var filename in fileList)
                        {
                            if (filename.ToLower().Contains(SingularityEnvironment.AppPackageExtension))
                            {
                                UpdateFileList.Add(filename);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetManager.ApplicationLogger.Error("Error DoUSBStickClick", ex);
                    }
                }

            }
        }

        public ICommand USBStickClick
        {
            get { return new DelegateCommand(DoUSBStickClick); }
        }

        private void DoUSBStickClick()
        {
            if (UpdateFileList.Count == 0)
            {
                MessageBox.Show(_visual, Localization.Get(Localization.Key.NoUpdateFile), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (SelectedFileIndex < 0)
            {
                MessageBox.Show(_visual, Localization.Get(Localization.Key.ChooseUpdateFile), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                String src = UpdateFileList[SelectedFileIndex];
                MessageBox.Show(_visual, Localization.Get(Localization.Key.UpdateWithFile) + "?\n\n" + src, "Update",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, new Action<DialogResult>(
                        answerupdate =>
                        {
                            switch (answerupdate)
                            {
                                case DialogResult.Yes:
                                    {
                                        String dst = Path.Combine(SingularityEnvironment.ServiceDirectory, Path.GetFileName(src));

                                        // copy updatefile to Hard Disk\Service
                                        File.Copy(src, dst, true);

                                        Log4NetManager.ApplicationLogger.Info("Ask for reboot terminal after update with " + src);

                                        AskForReboot();
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }));
            }
        }

        public ICommand RetrieveLogfiles
        {
            get { return new DelegateCommand(DoRetrieveLogfiles); }
        }

        public List<String> LogFiles = new List<string>();
        public String tempDstPath = "";
        private const int _logFileAgeToDelete = 30;

        private void DoRetrieveLogfiles()
        {
            // copy all files in temp directory to USB directory
            try
            {
                SingularityEnvironment env = new SingularityEnvironment("MT.LonzaPixon");
                String logPath = env.LogDirectory;

                LogFiles.Clear();
                tempDstPath = Globals.ProjectNumber.Substring(0, 9) + "_temp_" + DateTime.Now.ToString("yyyyMMdd");
                String tempDstFullPath = Path.Combine(_uSBPath, tempDstPath);

                if (!Directory.Exists(tempDstFullPath))
                    Directory.CreateDirectory(tempDstFullPath);

                DirectoryInfo di = new DirectoryInfo(logPath);
                FileInfo[] files = di.GetFiles("*");
                foreach (var file in files)
                {
                    String fileDstName = Path.Combine(tempDstFullPath, Path.GetFileName(file.FullName));
                    LogFiles.Add(file.Name);
                    if (File.Exists(fileDstName))
                    {
                        File.Delete(fileDstName);
                    }
                    Log4NetManager.ApplicationLogger.Info("Copy Logfile " + file.Name + " to " + fileDstName);
                    File.Copy(file.FullName, fileDstName);
                }
                int filesCopied = files.Length;

                // delete logfiles
                MessageBox.Show(HomeScreen.Instance.NavigationFrame,
                    "Delete logfiles older than " + _logFileAgeToDelete + " days?", "Question", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, new Action<DialogResult>(
                        (answerdelete) =>
                        {
                            switch (answerdelete)
                            {
                                case DialogResult.Yes:
                                    {
                                        int logFilesDeleted = 0;
                                        foreach (var file in files)
                                        {
                                            if (file.CreationTime < DateTime.Now.AddDays(-_logFileAgeToDelete))
                                            {
                                                File.Delete(file.FullName);
                                                Log4NetManager.ApplicationLogger.Info("Logfile " + file.Name + " deleted");
                                                logFilesDeleted++;
                                            }
                                        }
                                        MessageBox.Show(HomeScreen.Instance.NavigationFrame, filesCopied + " Logfiles sucessfully copied to USBStick (" + tempDstFullPath + ")\n" +
                                        logFilesDeleted + " Logfiles older than " + _logFileAgeToDelete + " days deleted", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        break;
                                    }
                                case DialogResult.No:
                                    {
                                        MessageBox.Show(HomeScreen.Instance.NavigationFrame, filesCopied + " Logfiles sucessfully copied to USBStick (" + tempDstFullPath + ")", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        break;
                                    }
                            }
                        }));
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error copy logfile to USBStick", ex);
                MessageBox.Show(HomeScreen.Instance.NavigationFrame, "Error copying logfiles to USBStick\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async Task AskForReboot()
        {
            MessageBox.Show(_visual, Localization.Get(Localization.Key.UpdateFileCopied) + "!\n\n" + Localization.Get(Localization.Key.RebootTerminalNow) + "?\n\n", "Reboot",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, new Action<DialogResult>(async
                    answerboot =>
                    {
                        switch (answerboot)
                        {
                            case DialogResult.Yes:
                                {
                                    var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
                                    var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
                                    Log4NetManager.ApplicationLogger.Info("Reboot terminal");
                                    FlexiconPump.Instance.EventEmitter = null;
                                    await FlexiconPump.Instance.CloseSerialAsync();
                                    await platformEngine.RebootAsync();
                                    break;
                                }
                            default:
                                break;
                        }
                    }));
        }

        private Boolean _updateButtonEnabled = false;
        public Boolean UpdateButtonEnabled
        {
            get { return _updateButtonEnabled; }
            set
            {
                if (_updateButtonEnabled != value)
                {
                    _updateButtonEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _selectedFileIndex;
        public int SelectedFileIndex
        {
            get { return _selectedFileIndex; }
            set
            {
                if (_selectedFileIndex != value)
                {
                    _selectedFileIndex = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private ObservableCollection<String> _updateFileList = new ObservableCollection<string>();
        public ObservableCollection<String> UpdateFileList
        {
            get { return _updateFileList; }
            set
            {
                if (_updateFileList != value)
                {
                    _updateFileList = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}
