﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.LonzaPixon.Pages
{
    /// <summary>
    /// Interaction logic for StartPage
    /// </summary>
    public partial class StartPage
    {
        private StartPageViewModel viewModel;

        public StartPage()
        {
            viewModel = new StartPageViewModel();
            InitializeComponents();
#if !DEBUG
            btnExit.Visibility = Visibility.Collapsed;
#endif
        }

        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            HomeScreen.Instance.ViewModel.USBObserveStop();
            return base.OnNavigatingAway(nextPage);
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            HomeScreen.Instance.ViewModel.StatusTextVisibility = Visibility.Collapsed;
            HomeScreen.Instance.ViewModel.WindowTitle = Localization.Get(Localization.Key.Grundzustand);
            HomeScreen.Instance.ViewModel.StatusText = "";
            viewModel.EnableStartButtons = true;
            HomeScreen.Instance.ViewModel.BalanceButtonsEnabled = true;
            HomeScreen.Instance.ViewModel.USBObserveStart();
            }
    }
}
