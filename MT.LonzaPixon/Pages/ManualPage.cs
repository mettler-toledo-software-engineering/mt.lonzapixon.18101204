﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.LonzaPixon.Controls;
using MT.LonzaPixon.Logic;

namespace MT.LonzaPixon.Pages
{
    /// <summary>
    /// Interaction logic for ManualPage
    /// </summary>
    public partial class ManualPage
    {
        private ManualPageViewModel viewModel;
        private AllManualControls manualControls;

        public ManualPage(HomeScreen homeScreen)
        {
            viewModel = new ManualPageViewModel();
            InitializeComponents();
            //    viewModel.InitCmdFeinstrom(cmdFeinstrom);
            manualControls = new AllManualControls(this);
            PanelControls.Add(manualControls);
            HomeScreen.Instance.ViewModel.BalanceButtonsEnabled = true;
        }

        protected override Singularity.Presentation.Controls.Navigation.NavigationResult OnNavigatingBack(Singularity.Presentation.Controls.Navigation.INavigationPage nextPage)
        {
            manualControls.Dispose();

            return base.OnNavigatingBack(nextPage);
        }
    }
}
