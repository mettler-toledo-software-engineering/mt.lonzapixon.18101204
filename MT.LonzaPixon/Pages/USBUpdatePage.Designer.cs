﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
namespace MT.LonzaPixon.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class USBUpdatePage : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.ListBox internal5;
            MT.Singularity.Presentation.Controls.DockPanel internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.Button internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.Button internal13;
            MT.Singularity.Presentation.Controls.GroupPanel internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            MT.Singularity.Presentation.Controls.StackPanel internal17;
            MT.Singularity.Presentation.Controls.Button internal18;
            MT.Singularity.Presentation.Controls.GroupPanel internal19;
            MT.Singularity.Presentation.Controls.Image internal20;
            MT.Singularity.Presentation.Controls.TextBlock internal21;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.FontSize = ((System.Nullable<System.Int32>)28);
            internal4.Width = 1000;
            internal4.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal4.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278198180u));
            internal4.Padding = new MT.Singularity.Presentation.Thickness(10);
            internal4.Margin = new MT.Singularity.Presentation.Thickness(10, 10, 20, 10);
            internal4.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.UpdateFilesAufUsbStick);
            internal4.AddTranslationAction(() => {
                internal4.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.UpdateFilesAufUsbStick);
            });
            internal5 = new MT.Singularity.Presentation.Controls.ListBox();
            internal5.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(251658240u));
            internal5.Height = 310;
            internal5.Width = 1000;
            internal5.Margin = new MT.Singularity.Presentation.Thickness(10, 0, 120, 10);
            internal5.FontSize = ((System.Nullable<System.Int32>)28);
            internal5.Padding = new MT.Singularity.Presentation.Thickness(5);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.SelectedIndex,() =>  viewModel.SelectedFileIndex,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.ItemsSource,() =>  viewModel.UpdateFileList,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal5);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal9 = new MT.Singularity.Presentation.Controls.Button();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9.Width = 180;
            internal9.Height = 90;
            internal9.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Memostick.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.UsbUpdate);
            internal12.AddTranslationAction(() => {
                internal12.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.UsbUpdate);
            });
            internal12.FontSize = ((System.Nullable<System.Int32>)20);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11, internal12);
            internal9.Content = internal10;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.IsEnabled,() =>  viewModel.UpdateButtonEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Command,() =>  viewModel.USBStickClick,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.Button();
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13.Width = 180;
            internal13.Height = 90;
            internal13.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal13.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Log file icon.al8";
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Logfiles);
            internal16.AddTranslationAction(() => {
                internal16.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Logfiles);
            });
            internal16.FontSize = ((System.Nullable<System.Int32>)20);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal14 = new MT.Singularity.Presentation.Controls.GroupPanel(internal15, internal16);
            internal13.Content = internal14;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.IsEnabled,() =>  viewModel.UpdateButtonEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Command,() =>  viewModel.RetrieveLogfiles,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal13);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal18 = new MT.Singularity.Presentation.Controls.Button();
            internal18.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal18.Width = 180;
            internal18.Height = 90;
            internal18.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal18.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal20 = new MT.Singularity.Presentation.Controls.Image();
            internal20.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.ArrowLeft.al8";
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal20.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal21 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal21.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Back);
            internal21.AddTranslationAction(() => {
                internal21.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Back);
            });
            internal21.FontSize = ((System.Nullable<System.Int32>)20);
            internal21.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal19 = new MT.Singularity.Presentation.Controls.GroupPanel(internal20, internal21);
            internal18.Content = internal19;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.Command,() =>  GoBack,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal17 = new MT.Singularity.Presentation.Controls.StackPanel(internal18);
            internal17.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8, internal17);
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal7.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal7.Visibility = MT.Singularity.Presentation.Visibility.Visible;
            internal6 = new MT.Singularity.Presentation.Controls.DockPanel(internal7);
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal6);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[7];
    }
}
