﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace MT.LonzaPixon.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow weightDisplay;
        private MT.Singularity.Presentation.Controls.TextBlock StatusTextBlock;
        private MT.Singularity.Presentation.Controls.Navigation.AnimatedNavigationFrame navigationFrame;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.DockPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.Button internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.Image internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.Button internal10;
            MT.Singularity.Presentation.Controls.GroupPanel internal11;
            MT.Singularity.Presentation.Controls.Image internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.GroupPanel internal15;
            MT.Singularity.Presentation.Controls.Image internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            MT.Singularity.Presentation.Controls.StackPanel internal18;
            MT.Singularity.Presentation.Controls.Button internal19;
            MT.Singularity.Presentation.Controls.GroupPanel internal20;
            MT.Singularity.Presentation.Controls.Image internal21;
            MT.Singularity.Presentation.Controls.TextBlock internal22;
            MT.Singularity.Presentation.Controls.StackPanel internal23;
            MT.Singularity.Presentation.Controls.TextBlock internal24;
            MT.Singularity.Presentation.Controls.DockPanel internal25;
            weightDisplay = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            weightDisplay.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4292927712u));
            weightDisplay.Height = 200;
            weightDisplay.FontSize = ((System.Nullable<System.Int32>)18);
            weightDisplay.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            weightDisplay.Width = 600;
            internal6 = new MT.Singularity.Presentation.Controls.Button();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal6.Width = 100;
            internal6.Height = 60;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.IsEnabled,() =>  viewModel.BalanceButtonsEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal6.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal8 = new MT.Singularity.Presentation.Controls.Image();
            internal8.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Zero.al8";
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Zero);
            internal9.AddTranslationAction(() => {
                internal9.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Zero);
            });
            internal9.FontSize = ((System.Nullable<System.Int32>)16);
            internal9.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8, internal9);
            internal6.Content = internal7;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Command,() =>  viewModel.PerformZero,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.Button();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal10.Width = 100;
            internal10.Height = 60;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.IsEnabled,() =>  viewModel.BalanceButtonsEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal10.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal12 = new MT.Singularity.Presentation.Controls.Image();
            internal12.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Tare.al8";
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Tare);
            internal13.AddTranslationAction(() => {
                internal13.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Tare);
            });
            internal13.FontSize = ((System.Nullable<System.Int32>)16);
            internal13.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal11 = new MT.Singularity.Presentation.Controls.GroupPanel(internal12, internal13);
            internal10.Content = internal11;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Command,() =>  viewModel.PerformTare,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14.Width = 100;
            internal14.Height = 60;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.IsEnabled,() =>  viewModel.BalanceButtonsEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal14.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal14.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal16 = new MT.Singularity.Presentation.Controls.Image();
            internal16.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.ClearTare.al8";
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.ClearTare);
            internal17.AddTranslationAction(() => {
                internal17.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.ClearTare);
            });
            internal17.FontSize = ((System.Nullable<System.Int32>)16);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal15 = new MT.Singularity.Presentation.Controls.GroupPanel(internal16, internal17);
            internal14.Content = internal15;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() =>  viewModel.PerformClearTare,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal10, internal14);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal19 = new MT.Singularity.Presentation.Controls.Button();
            internal19.Margin = new MT.Singularity.Presentation.Thickness(4, 4, 4, 4);
            internal19.Width = 180;
            internal19.Height = 60;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal19.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal19.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal19.IsEnabled,() =>  viewModel.IsFillParamsNotRunning,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal21 = new MT.Singularity.Presentation.Controls.Image();
            internal21.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Settings.al8";
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal22 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal22.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal22.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal22.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.DosierParams);
            internal22.AddTranslationAction(() => {
                internal22.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.DosierParams);
            });
            internal22.FontSize = ((System.Nullable<System.Int32>)16);
            internal20 = new MT.Singularity.Presentation.Controls.GroupPanel(internal21, internal22);
            internal19.Content = internal20;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal19.Command,() =>  viewModel.DosierParams,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal18 = new MT.Singularity.Presentation.Controls.StackPanel(internal19);
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5, internal18);
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal4.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal4.Visibility = MT.Singularity.Presentation.Visibility.Visible;
            internal24 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal24.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal24.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleLeft;
            internal24.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal24.FontSize = ((System.Nullable<System.Int32>)42);
            internal24.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal24.Text,() =>  viewModel.WindowTitle,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal23 = new MT.Singularity.Presentation.Controls.StackPanel(internal24);
            internal23.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal23.Visibility = MT.Singularity.Presentation.Visibility.Visible;
            internal23.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4284461385u));
            internal23.Height = 132;
            internal23.Width = 680;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal23);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(weightDisplay, internal3);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            StatusTextBlock = new MT.Singularity.Presentation.Controls.TextBlock();
            StatusTextBlock.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            StatusTextBlock.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            StatusTextBlock.FontSize = ((System.Nullable<System.Int32>)28);
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => StatusTextBlock.Visibility,() =>  viewModel.StatusTextVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            StatusTextBlock.Margin = new MT.Singularity.Presentation.Thickness(10);
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => StatusTextBlock.Foreground,() =>  viewModel.StatusTextColor,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => StatusTextBlock.Text,() =>  viewModel.StatusText,MT.Singularity.Expressions.BindingMode.OneWay,false);
            navigationFrame = new MT.Singularity.Presentation.Controls.Navigation.AnimatedNavigationFrame();
            navigationFrame.Padding = new MT.Singularity.Presentation.Thickness(0, 0, 0, 0);
            navigationFrame.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            navigationFrame.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal25 = new MT.Singularity.Presentation.Controls.DockPanel(StatusTextBlock, navigationFrame);
            internal25.Height = 30;
            internal25.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.DockPanel(internal2, internal25);
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal1.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            this.Content = internal1;
            this.Width = 1280;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[12];
    }
}
