﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MT.LonzaPixon.Config;
using MT.LonzaPixon.Logic;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.Memories;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation;

namespace MT.LonzaPixon.Pages
{
    public class HomeScreenViewModel : PropertyChangedBase
    {
        private LonzaPixonConfiguration _configuration;
        public LonzaPixonConfiguration Configuration { get { return _configuration; } }

        private IScaleService _scaleService;
        public IScaleService ScaleService { get { return _scaleService; } }

        private ISecurityService _securityService;
        public ISecurityService SecurityService { get { return _securityService; } }

        private IInterfaceService _interfaces;
        public IInterfaceService Interfaces { get { return _interfaces; } }

        private IAlibiLogComponent _alibiLogComponent;
        public IAlibiLogComponent AlibiLogComponent { get { return _alibiLogComponent; } }

        private IConnectionChannel<DataSegment> _pumpConnectionChannel;
        public IConnectionChannel<DataSegment> PumpConnectionChannel
        {
            get { return _pumpConnectionChannel; }
        }

        private Timer _uSBTimer;
        private Boolean _usbUpdateAsking = false;
        private Boolean _uSBObserveRunning = false;

        // synch initialize
        public bool Initializing;

        public HomeScreenViewModel()
        {
        }

        private string _windowTitle = "HomeScreen";
        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                if (_windowTitle != value)
                {
                    _windowTitle = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public bool Initialize()
        {
            var logLevel = Log4NetManager.ApplicationLogger.IsDebugEnabled;
            Log4NetManager.ApplicationLogger.Info("++++++++++++++++++++ Program LonzaPixon started, Version " + Globals.ProgVersionStr(true) + " ++++++++++++++++++++");

            Initializing = true;
            bool result = false;
            Task.Run(async () =>
            {
                result = await InitializeViewModel();
                if (!result)
                {
                    MessageBox.Show(HomeScreen.Instance, "Error init StartPageViewModel", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Initializing = false;
            });
            while (Initializing)
            {
                Debug.WriteLine("..wait until Initializing is finished..");
                Thread.Sleep(100);
            }

            _uSBTimer = new Timer(new TimerCallback(USBTimerEvent), null, Timeout.Infinite, Timeout.Infinite);
            USBObserveStart();

            return result;
        }

        private async Task<bool> InitializeViewModel()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();

            // init security service (UserManagement)
            _securityService = await platformEngine.GetSecurityServiceAsync();
            // event for user changed
            _securityService.CurrentUserChanged += _securityService_CurrentUserChanged;
            _securityService_CurrentUserChanged(_securityService.CurrentUser);

            // init scale service
            _scaleService = await platformEngine.GetScaleServiceAsync();
            if (_scaleService == null)
            {
                Log4NetManager.ApplicationLogger.Error("ScaleService not initialized");
                Debug.WriteLine("ScaleService not initialized");
                return false;
            }

            // get log service
            var _logService = await platformEngine.GetLogServiceAsync();
            try
            {
                // get alibi log service
                _alibiLogComponent = await _logService.GetAlibiLogComponentAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log4NetManager.ApplicationLogger.Error("Error init alibiLogComponent" + ex.Message);
            }

            // init interfaces
            _interfaces = await platformEngine.GetInterfaceServiceAsync();
            var allInterfaces = await HomeScreen.Instance.ViewModel.Interfaces.GetAllInterfacesAsync();
            var allserialInterface = allInterfaces.OfType<ISerialInterface>();
            var serialInterface = allserialInterface.OfType<ISerialInterface>().FirstOrDefault((item => item.Type == InterfaceType.Serial));
            if (serialInterface == null)
            {
                MessageBox.Show(HomeScreen.Instance, "Error: no serial Port installed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw new Exception("Error: no serial Port installed");
            }
            _pumpConnectionChannel = await serialInterface.CreateConnectionChannelAsync();

            // init config
            try
            {
                var customerComponent = compositionContainer.Resolve<ILonzaPixonComponents>();
                _configuration = await customerComponent.GetConfigurationAsync();
                _configuration.PropertyChanged += _configuration_PropertyChanged;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log4NetManager.ApplicationLogger.Error("Error Resolve ILonzaPixomComponents", ex);

                // create default configuration
                _configuration = new LonzaPixonConfiguration();
            }

            //GetDisplayUnits();

            // init Digital IO
            // searching interface for digital IO
            IDigitalIOInterface _digitalIo = null;
            for (int i = 1; i < 7; i++)
            {
                try
                {
                    _digitalIo = await _interfaces.GetDigitalIOInterfaceAsync(i);
                    if (_digitalIo != null)
                    {
                        DigitalIOLonza.Instance = new DigitalIOLonza(_digitalIo);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Log4NetManager.ApplicationLogger.Error("Error on initializing Digital IO:\n" + ex.Message);
                    Debug.WriteLine(ex.Message);
                }
            }

            if (_digitalIo == null)
            {
                Log4NetManager.ApplicationLogger.Warning("No Digital IO found");
                DigitalIOLonza.Instance = new DigitalIOLonza(null);
            }
            DigitalIOLonza.InstanceLonza.Output1 = false;
            DigitalIOLonza.InstanceLonza.Output2 = false;
            DigitalIOLonza.InstanceLonza.Output3 = false;
            DigitalIOLonza.InstanceLonza.Output4 = false;
            await DigitalIOLonza.InstanceLonza.WriteOutputDigitalIO();
            DigitalIOLonza.InstanceLonza.ReadInputDigitalIO();

            // init DosinParams
            WeightInformation weightInformation = await _scaleService.SelectedScale.GetWeightAsync(UnitType.Host);
            DosingParams.Instance = new DosingParams();
            DosingParams.Instance.Read();

            // init Flexicon pump class
            FlexiconPump.Instance = new FlexiconPump();

            // init FillEngine class
            FillEngine.Instance = new FillEngine();

            ParsePrecisionAndUnit();

            return true;
        }

        void _configuration_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "PrecisionAndUnit")
            {
                ParsePrecisionAndUnit();
            }
        }

        private async Task ParsePrecisionAndUnit()
        {
            string precisionAndUnit = _configuration.PrecisionAndUnit;
            WellknownWeightUnit selectedWeightunit = WellknownWeightUnit.Kilogram;
            int precisionSelectedUnit = 4;
            // parse unit
            if (!precisionAndUnit.Contains("kg"))
            {
                selectedWeightunit = WellknownWeightUnit.Gram;
            }
            // remove unit
            precisionAndUnit = precisionAndUnit.Replace(" kg", "");
            precisionAndUnit = precisionAndUnit.Replace(" g", "");
            // remove leadinbg zero and decimal point
            if (precisionAndUnit == "0.0")
            {
                precisionSelectedUnit = 1;
            }
            else
            {
                if (precisionAndUnit == "0")
                {
                    precisionSelectedUnit = 0;
                }
                else
                {
                    precisionAndUnit = precisionAndUnit.Replace("0.", "");
                    precisionSelectedUnit = precisionAndUnit.Length;
                }
            }
            // set precision
            if (selectedWeightunit == WellknownWeightUnit.Kilogram)
            {
                Precision_kg = precisionSelectedUnit;
            }
            else
            {
                Precision_kg = precisionSelectedUnit + 3;
            }
            WeightUnit = selectedWeightunit;
            Globals.CurrentWeightUnit = WeightUnit;
            await _scaleService.SelectedScale.SwitchUnitAsync(UnitType.Display, WeightUnit);
            OnUnitChanged(EventArgs.Empty);
        }

        private WellknownWeightUnit _weightUnit = WellknownWeightUnit.Kilogram;
        public WellknownWeightUnit WeightUnit
        {
            get { return _weightUnit; }
            set
            {
                if (_weightUnit != value)
                {
                    _weightUnit = value;
                    NotifyPropertyChanged("WeightUnit");
                }
            }
        }

        private int _precision_kg = 4;
        public int Precision_kg
        {
            get { return _precision_kg; }
            set
            {
                if (_precision_kg != value)
                {
                    _precision_kg = value;
                    NotifyPropertyChanged("Precision_kg");
                }
            }
        }

        private Boolean _uSBStickFound = false;
        public Boolean USBStickFound
        {
            get { return _uSBStickFound; }
            set
            {
                if (_uSBStickFound != value)
                {
                    _uSBStickFound = value;
                    NotifyPropertyChanged();
                    USBVisible = _uSBStickFound ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        private Visibility _uSBVisible = Visibility.Collapsed;
        public Visibility USBVisible
        {
            get { return _uSBVisible; }
            set
            {
                if (_uSBVisible != value)
                {
                    _uSBVisible = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public void USBObserveStart()
        {
            USBStickFound = false;
            _usbUpdateAsking = false;
            _uSBObserveRunning = true;
            _uSBTimer.Change(1000, Timeout.Infinite);
        }

        public void USBObserveStop()
        {
            _uSBObserveRunning = false;
            _uSBTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void USBTimerEvent(object state)
        {
            _uSBTimer.Change(Timeout.Infinite, Timeout.Infinite);

            // check for "\USB Hard Disk" (IND890 INDPro) or "E:" (Simulation)
            if (Directory.Exists(Globals.INDNeoDeviceUSBDrive) || Directory.Exists(Globals.INDNeoSimulationUSBDrive))
            {
                if (!USBStickFound)
                {
                    if (!_usbUpdateAsking)
                    {
                        _usbUpdateAsking = true;
                        Log4NetManager.ApplicationLogger.Info("USBStick found " +
                                                              (Directory.Exists(Globals.INDNeoDeviceUSBDrive)
                                                                  ? Globals.INDNeoDeviceUSBDrive
                                                                  : Globals.INDNeoSimulationUSBDrive));
                        MessageBox.Show(HomeScreen.Instance, Localization.Get(Localization.Key.UsbStickFound) +
                            "\n" + Localization.Get(Localization.Key.UpdateStart),
                            Localization.Get(Localization.Key.USBUpdate),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question, new Action<DialogResult>(
                                answer =>
                                {
                                    switch (answer)
                                    {
                                        case DialogResult.Yes:
                                            {
                                                HomeScreen.Instance.NavigationFrame.NavigateTo(new USBUpdatePage(), Configuration.Animation);
                                                break;
                                            }
                                        default:
                                            break;
                                    }
                                    _usbUpdateAsking = false;
                                }));
                    }
                }
                USBStickFound = true;
            }
            else
            {
                USBStickFound = false;
            }
            if (_uSBObserveRunning)
                _uSBTimer.Change(1000, Timeout.Infinite);
        }

        private string _bediener = "";
        public string Bediener
        {
            get { return _bediener; }
            set
            {
                if (_bediener != value)
                {
                    _bediener = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public Permission UserPermission;

        void _securityService_CurrentUserChanged(User user)
        {
            Bediener = user.Name;
            UserPermission = user.Permission;
            Log4NetManager.ApplicationLogger.Info("User " + Bediener + " logged in");
        }

        public ICommand PerformZero
        {
            get
            {
                return new DelegateCommand(DoPerformZero);
            }
        }

        public async void DoPerformZero()
        {
            WeightState state = await _scaleService.SelectedScale.ZeroAsync();
            if (state != WeightState.OK)
            {
                MessageBox.Show(HomeScreen.Instance, "Zero failed, status = " + state.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean _balanceButtonsEnabled = true;
        public Boolean BalanceButtonsEnabled
        {
            get { return _balanceButtonsEnabled; }
            set
            {
                if (_balanceButtonsEnabled != value)
                {
                    _balanceButtonsEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ICommand PerformTare
        {
            get
            {
                return new DelegateCommand(DoPerformTare);
            }
        }

        public async void DoPerformTare()
        {
            WeightState state = await _scaleService.SelectedScale.TareAsync();
            if (state != WeightState.OK)
            {
                MessageBox.Show(HomeScreen.Instance, "Tare failed, status = " + state.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ICommand PerformClearTare
        {
            get
            {
                return new DelegateCommand(DoPerformClearTare);
            }
        }

        public async void DoPerformClearTare()
        {
            WeightState state = await _scaleService.SelectedScale.ClearTareAsync();
            if (state != WeightState.OK)
            {
                MessageBox.Show(HomeScreen.Instance, "Clear tare failed, status = " + state.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ICommand DosierParams
        {
            get
            {
                return new DelegateCommand(DoDosierParams);
            }
        }

        public async void DoDosierParams()
        {
            IsFillParamsNotRunning = false;
            HomeScreen.Instance.NavigationFrame.NavigateTo(new WeightGetParamsPage(), _configuration.Animation);
        }


        public async void GetDisplayUnits()
        {
            WeightInformation weightinfo = await _scaleService.SelectedScale.GetWeightAsync(UnitType.Display);
            if (weightinfo.IsValid)
            {
                Log4NetManager.ApplicationLogger.Info("WeightInformation read, CurrentWeightUnit set to " + weightinfo.Unit);
                Globals.CurrentWeightUnit = weightinfo.Unit;
            }
            else
            {
                // balance not ready yet or busy with fact
                Log4NetManager.ApplicationLogger.Info("WeightInformation invalid, CurrentWeightUnit set manually to " + WellknownWeightUnit.Kilogram);
                Globals.CurrentWeightUnit = WellknownWeightUnit.Kilogram;
            }
        }

        private bool _isFillParamsNotRunning = true;
        public bool IsFillParamsNotRunning
        {
            get { return _isFillParamsNotRunning; }
            set
            {
                if (_isFillParamsNotRunning != value)
                {
                    _isFillParamsNotRunning = value;
                    NotifyPropertyChanged();
                }
            }
        }

        string inputCheckState = "off";

        public string InputCheckState
        {
            get { return inputCheckState; }
            set
            {
                if (inputCheckState != value)
                {
                    inputCheckState = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _statusText = "";
        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText != value)
                {
                    _statusText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Color _clRed = new Color(0xff, 0xff, 0x00, 0x00);
        private Color _clWhite = new Color(0xff, 0xff, 0xff, 0xff);

        public void SetStatusText(string text)
        {
            StatusText = text;
            StatusTextColor = _clWhite;
            StatusTextVisibility = Visibility.Visible;
        }

        public void SetStatusError(string text)
        {

            StatusText = text;
            StatusTextColor = _clRed;
            StatusTextVisibility = Visibility.Visible;
        }

        private Color _statusTextColor = new Color(0xff, 0xff, 0xff, 0xff);
        public Color StatusTextColor
        {
            get { return _statusTextColor; }
            set
            {
                if (_statusTextColor != value)
                {
                    _statusTextColor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        Visibility statusTextVisibility = Visibility.Collapsed;

        public Visibility StatusTextVisibility
        {
            get { return statusTextVisibility; }
            set
            {
                if (statusTextVisibility != value)
                {
                    statusTextVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Occures when the event is raised
        /// </summary>
        public event EventHandler UnitChanged;

        /// <summary>
        /// Raises the <see cref="UnitChanged"/> event
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnUnitChanged(EventArgs e)
        {
            if (UnitChanged != null)
                UnitChanged(this, e);
        }

    }
}
