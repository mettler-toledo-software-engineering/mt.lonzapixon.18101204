﻿using System.Threading.Tasks;
using MT.LonzaPixon.Logic;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation.Controls;
using MT.LonzaPixon.Controls;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace MT.LonzaPixon.Pages
{
    public class WeightFillPageViewModel : PropertyChangedBase
    {
        private HomeScreenViewModel hscr = HomeScreen.Instance.ViewModel;

        private DoubleDeltaTrac deltaTrac;
        Visual Parent;
        public WeightFillPageViewModel(Visual Parent)
        {
            this.Parent = Parent;
            Task.Run(async () =>
            {
                return await InitializeViewModel();
            });
        }

        private async Task<bool> InitializeViewModel()
        {
            HomeScreen.Instance.ViewModel.WindowTitle = Localization.Get(Localization.Key.FillPage);
            return true;
        }

        public void SetUnitChangedEvent()
        {
            HomeScreen.Instance.ViewModel.UnitChanged += ViewModel_UnitChanged;
        }
        public void RemoveEvents()
        {
            HomeScreen.Instance.ViewModel.UnitChanged -= ViewModel_UnitChanged;
        }

        void ViewModel_UnitChanged(object sender, System.EventArgs e)
        {
            var x = FillEngine.Instance.Tare;
            FillEngine.Instance.Tare = double.MinValue;
            FillEngine.Instance.Tare = x;
            var d = DosingParams.Instance.NettoSum;
            DosingParams.Instance.NettoSum = double.MinValue;
            DosingParams.Instance.NettoSum = d;
            FillEngine.Instance.VorabschaltLimitFormattedWidthUnit = "";
            FillEngine.Instance.VorabschaltLimitFormattedWidthUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.VorabschaltLimit);
            FillEngine.Instance.GrobFeinLimitFormattedWithUnit = "";
            FillEngine.Instance.GrobFeinLimitFormattedWithUnit = Globals.FormatWeightWithUnits(DosingParams.Instance.GrobFeinLimit);
            if (deltaTrac != null)
                deltaTrac.UpdateTarget();
        }

        public DoubleDeltaTrac InitDeltatrac()
        {
            deltaTrac = new DoubleDeltaTrac(Parent, 1200, 88, DosingParams.Instance.Sollwert, DosingParams.Instance.TolPos, DosingParams.Instance.TolNeg);
            return deltaTrac;
        }


        private int _gebindeNr = 0;
        public int GebindeNr
        {
            get { return _gebindeNr; }
            set
            {
                if (_gebindeNr != value)
                {
                    _gebindeNr = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public ICommand btnBack
        {
            get { return new DelegateCommand(DoBack); }
        }

        public async void DoBack()
        {
            //DosingParams.Instance.GebindeNr = 0;
            DosingParams.Instance.NettoSum = 0;
            FillEngine.Instance.StopFillThread();
            FillEngine.Instance.ViewModel = null;
            
            HomeScreen.Instance.NavigationFrame.Back();
        }

        private string _statusText = "";
        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText != value)
                {
                    _statusText = value;
                    hscr.SetStatusText(value);
                }
            }
        }

        public ICommand PrintManual
        {
            get
            {
                return new DelegateCommand(DoPrintManual);
            }
        }

        public async void DoPrintManual()
        {
            Printer _printer = new Printer(HomeScreen.Instance.ViewModel.Configuration.PrinterIP, HomeScreen.Instance.ViewModel.Configuration.PrinterPort);
            WeightInformation wInfo = await HomeScreen.Instance.ViewModel.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
            double convertedNet = WeightUnits.Convert(wInfo.NetWeight, wInfo.Unit, WellknownWeightUnit.Kilogram);

            if (!await _printer.PrintManualLabel(convertedNet))
            {
                MessageBox.Show(HomeScreen.Instance, "Printer connection failed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
