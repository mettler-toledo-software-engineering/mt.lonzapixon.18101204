﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.LonzaPixon.Pages
{
    /// <summary>
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
        public static HomeScreen Instance;
        private readonly HomeScreenViewModel viewModel;
        public HomeScreenViewModel ViewModel { get { return viewModel; } }


        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen()
        {
            viewModel = new HomeScreenViewModel();

            InitializeComponents();
            Instance = this;

            // Initialize: Homscreen is constructed, Instance is set
            bool res = viewModel.Initialize();
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        public AnimatedNavigationFrame NavigationFrame
        {
            get
            {
                return navigationFrame;
            }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            weightDisplay.Activate();
            navigationFrame.NavigateTo(new StartPage(), HomeScreen.Instance.viewModel.Configuration.Animation);
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            if (result == NavigationResult.Proceed)
            {
                weightDisplay.Deactivate();
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            weightDisplay.Activate();
        }

        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor
        {
            get
            {
                return
#if DEBUG
 false;
#else
 true;
#endif
            }
        }
    }
}
