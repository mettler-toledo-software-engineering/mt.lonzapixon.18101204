﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
namespace MT.LonzaPixon.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class ManualPage : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.DynamicStackPanel PanelControls;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.DockPanel internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.Button internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.Image internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.StackPanel internal10;
            MT.Singularity.Presentation.Controls.Button internal11;
            MT.Singularity.Presentation.Controls.GroupPanel internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            PanelControls = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            PanelControls.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            PanelControls.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(PanelControls);
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(50);
            internal6 = new MT.Singularity.Presentation.Controls.Button();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal6.Width = 180;
            internal6.Height = 90;
            internal6.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal6.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal8 = new MT.Singularity.Presentation.Controls.Image();
            internal8.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.Print.al8";
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.PrintManual);
            internal9.AddTranslationAction(() => {
                internal9.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.PrintManual);
            });
            internal9.FontSize = ((System.Nullable<System.Int32>)20);
            internal9.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8, internal9);
            internal6.Content = internal7;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Command,() =>  viewModel.PrintManual,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal11 = new MT.Singularity.Presentation.Controls.Button();
            internal11.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal11.Width = 180;
            internal11.Height = 90;
            internal11.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://MT.LonzaPixon/MT.LonzaPixon.Images.ArrowLeft.al8";
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Back);
            internal14.AddTranslationAction(() => {
                internal14.Text = MT.LonzaPixon.Localization.Get(MT.LonzaPixon.Localization.Key.Back);
            });
            internal14.FontSize = ((System.Nullable<System.Int32>)20);
            internal14.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal12 = new MT.Singularity.Presentation.Controls.GroupPanel(internal13, internal14);
            internal11.Content = internal12;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Command,() =>  viewModel.Back,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.StackPanel(internal11);
            internal10.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5, internal10);
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal4.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal3 = new MT.Singularity.Presentation.Controls.DockPanel(internal4);
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal3);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[2];
    }
}
