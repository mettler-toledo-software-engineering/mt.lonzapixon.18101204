﻿using System;
using MT.LonzaPixon.Logic;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.LonzaPixon.Controls;
using System.Threading.Tasks;
using System.Diagnostics;
using MT.Singularity.Logging;

namespace MT.LonzaPixon.Pages
{
    /// <summary>
    /// Interaction logic for WeightFillPage
    /// </summary>
    public partial class WeightFillPage
    {
        private WeightFillPageViewModel viewModel;
        private DoubleDeltaTrac ddt;
        AllManualControls amctrls = null;
        GraphicStatusControl gsctrl;


        public WeightFillPage()
        {
            viewModel = new WeightFillPageViewModel(this);
            InitializeComponents();
            FillEngine.Instance.ViewModel = viewModel;
            FillEngine.Instance.ManualFunctionRequest += Instance_ManualFunctionRequest;
            gsctrl = new GraphicStatusControl(this);
            ddt = viewModel.InitDeltatrac();
            PanelDeltaTrac.Add(ddt);
            FillEngine.Instance.GraphicStatusVisibility = Visibility.Visible;
            PanelGrapicStatus.Add(gsctrl);
            FillEngine.Instance.decisionControl = new DecisionControl(this);
            PanelDecision.Add(FillEngine.Instance.decisionControl);
            FillEngine.Instance.PanelDecisionVisibility = Visibility.Collapsed;

            FillEngine.Instance.NotTasteActivated = false;
            Log4NetManager.ApplicationLogger.Info("User " + HomeScreen.Instance.ViewModel.Bediener + " starts filling");
            FillEngine.Instance.StartFillThread();
        }

        void Instance_ManualFunctionRequest(object sender, FillEngine.ManualFunctionRequestEventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                if (e.Request)
                {
                    if (amctrls != null)
                        Debug.WriteLine("amctrls is not null !!!!!!!!!!!!");
                    else
                    {
                        amctrls = new AllManualControls(this);
                        PanelManualWork.Clear();
                        PanelManualWork.Add(amctrls);
                        PanelManualWork.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    Task.Run(async () =>
                    {

                        if (amctrls != null)
                        {
                            await amctrls.Dispose();
                            amctrls = null;
                            DigitalIOLonza.InstanceLonza.PopStartTasterLED();
                            Debug.WriteLine("AllManualControls disposed");
                        }
                        PanelManualWork.Visibility = Visibility.Collapsed;
                    });
                }
            }));
        }

        protected override void OnFirstNavigation()
        {
            DosingParams.Instance.GebindeNr = DosingParams.Instance.LastGebindeNr;
            viewModel.SetUnitChangedEvent();
            HomeScreen.Instance.ViewModel.BalanceButtonsEnabled = false;
            base.OnFirstNavigation();
        }
        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            FillEngine.Instance.ManualFunctionRequest -= Instance_ManualFunctionRequest;
            if (ddt != null)
                ddt.Dispose();
            if (amctrls != null)
                amctrls.Dispose();
            if (gsctrl != null)
                gsctrl.Dispose();
            viewModel.RemoveEvents();
            return base.OnNavigatingBack(nextPage);
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreen.Instance.ViewModel.WindowTitle = Localization.Get(Localization.Key.FillPage);
            base.OnNavigationReturning(previousPage);
        }
    }
}
