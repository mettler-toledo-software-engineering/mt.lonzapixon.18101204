﻿using MT.LonzaPixon.Logic;
using MT.Singularity;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Data;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.LonzaPixon.Pages
{
    public class ManualPageViewModel : PropertyChangedBase
    {
        public ManualPageViewModel()
        {
            HomeScreen.Instance.ViewModel.WindowTitle = Localization.Get(Localization.Key.Manuell);
            PrintTestButtonVisible = HomeScreen.Instance.ViewModel.UserPermission == Permissions.Administrator
                ? PrintTestButtonVisible = Visibility.Visible
                : Visibility.Collapsed;
        }

        public ICommand Back
        {
            get { return new DelegateCommand(DoBack); }
        }

        public async void DoBack()
        {
            HomeScreen.Instance.ViewModel.StatusText = Localization.Get(Localization.Key.SetPumpOff);
            DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
            DigitalIOLonza.InstanceLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollTief);
            if (FlexiconPump.Instance.IsOpen)
            {
                FlexiconPump.Instance.PumpOff(true);
            }
            HomeScreen.Instance.NavigationFrame.Back();
        }

        private Visibility _printTestButtonVisible = Visibility.Collapsed;
        public Visibility PrintTestButtonVisible
        {
            get { return _printTestButtonVisible; }
            set
            {
                if (_printTestButtonVisible != value)
                {
                    _printTestButtonVisible = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ICommand PrintManual
        {
            get
            {
                return new DelegateCommand(DoPrintManual);
            }
        }

        public async void DoPrintManual()
        {
            Printer _printer = new Printer(HomeScreen.Instance.ViewModel.Configuration.PrinterIP, HomeScreen.Instance.ViewModel.Configuration.PrinterPort);
            WeightInformation wInfo = await HomeScreen.Instance.ViewModel.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Host);
            double convertedNet = WeightUnits.Convert(wInfo.NetWeight, wInfo.Unit, WellknownWeightUnit.Kilogram);

            if (!await _printer.PrintManualLabel(convertedNet))
            {
                MessageBox.Show(HomeScreen.Instance, "Printer connection failed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
