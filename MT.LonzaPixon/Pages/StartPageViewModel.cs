﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using MT.LonzaPixon.Config;
using MT.LonzaPixon.Logic;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.LonzaPixon.Pages
{
    public class StartPageViewModel : PropertyChangedBase
    {
        private IInterfaceService _interfaces;
        private IScaleService _scaleService;
        private LonzaPixonConfiguration _configuration;

        public StartPageViewModel()
        {
            bool result = false;
            Task.Run(async () =>
            {
                result = await InitializeViewModel();
                if (!result)
                {
                    MessageBox.Show(HomeScreen.Instance, "Error init StartPageViewModel", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            });
        }

        private async Task<bool> InitializeViewModel()
        {
            HomeScreen.Instance.ViewModel.WindowTitle = Localization.Get(Localization.Key.Grundzustand);
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            _interfaces = await platformEngine.GetInterfaceServiceAsync();

            // init scale service
            _scaleService = await platformEngine.GetScaleServiceAsync();
            if (_scaleService == null)
            {
                Log4NetManager.ApplicationLogger.Error("ScaleService not initialized");
                Debug.WriteLine("ScaleService not initialized");
                return false;
            }

            // init config
            try
            {
                var customerComponent = compositionContainer.Resolve<ILonzaPixonComponents>();
                _configuration = await customerComponent.GetConfigurationAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);

                // create default configuration
                _configuration = new LonzaPixonConfiguration();
            }
            _configuration.PropertyChanged += _configuration_PropertyChanged;

            return true;
        }

        void _configuration_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var values = sender as LonzaPixonConfiguration;
            Debug.WriteLine("Config property changed: " + e.PropertyName);
        }

        public String ProgVersion
        {
            get
            {
#if DEBUG
                return Globals.ProjectNumber + "\nLonza Pixon Flaschenabfüllung\n" + Globals.ProgVersionStr(true);
#else
                return Globals.ProjectNumber + "\nLonza Pixon Flaschenabfüllung\n" + Globals.ProgVersionStr(false);
#endif
            }
        }

        public ICommand PerformZero
        {
            get
            {
                return new DelegateCommand(DoPerformZero);
            }
        }

        public async void DoPerformZero()
        {
            WeightState state = await _scaleService.SelectedScale.ZeroAsync();
            if (state != WeightState.OK)
            {
                MessageBox.Show(HomeScreen.Instance, "Zero failed, status = " + state.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ICommand PerformTare
        {
            get
            {
                return new DelegateCommand(DoPerformTare);
            }
        }

        public async void DoPerformTare()
        {
            WeightState state = await _scaleService.SelectedScale.TareAsync();
            if (state != WeightState.OK)
            {
                MessageBox.Show(HomeScreen.Instance, "Tare failed, status = " + state.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ICommand PerformClearTare
        {
            get
            {
                return new DelegateCommand(DoPerformClearTare);
            }
        }

        public async void DoPerformClearTare()
        {
            WeightState state = await _scaleService.SelectedScale.ClearTareAsync();
            if (state != WeightState.OK)
            {
                MessageBox.Show(HomeScreen.Instance, "Clear tare failed, status = " + state.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ICommand StartSerie
        {
            get
            {
                return new DelegateCommand(DoStartSerie);
            }
        }

        private bool _enableStartButtons = true;
        public bool EnableStartButtons
        {
            get { return _enableStartButtons; }
            set
            {
                if (_enableStartButtons != value)
                {
                    _enableStartButtons = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public async void DoStartSerie()
        {
            if (DigitalIOLonza.InstanceLonza.NotTaster)
            {
                EnableStartButtons = false;
                DosingParams.Instance.LogValues();
                HomeScreen.Instance.NavigationFrame.NavigateTo(new WeightFillPage(), HomeScreen.Instance.ViewModel.Configuration.Animation);
            }
            else
            {
                Log4NetManager.ApplicationLogger.Warning("User " + HomeScreen.Instance.ViewModel.Bediener + " tried to start filling but emergency signal activated");
                MessageBox.Show(HomeScreen.Instance.NavigationFrame, Localization.Get(Localization.Key.EmergencySwitchActivated), "Emergency", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public ICommand StartManual
        {
            get
            {
                return new DelegateCommand(DoStartManual);
            }
        }

        public async void DoStartManual()
        {
            if (DigitalIOLonza.InstanceLonza.NotTaster)
            {
                HomeScreen.Instance.NavigationFrame.NavigateTo(new ManualPage(HomeScreen.Instance), _configuration.Animation);
            }
            else
            {
                Log4NetManager.ApplicationLogger.Warning("User " + HomeScreen.Instance.ViewModel.Bediener + " tried to start manuak filling but emergency signal activated");
                MessageBox.Show(HomeScreen.Instance.NavigationFrame, Localization.Get(Localization.Key.EmergencySwitchActivated), "Emergency", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public ICommand ExitProgram
        {
            get
            {
                return new DelegateCommand(DoExitProgram);
            }
        }

        public async void DoExitProgram()
        {
            FlexiconPump.Instance.Running = false;
            DigitalIOLonza.InstanceLonza.Klemme = DigitalIOLonza.KlemmenStatus.KlemmeZu;
            DigitalIOLonza.InstanceLonza.SetFuellkopf(DigitalIOLonza.FuellkopfSollState.sollTief);
            if (FlexiconPump.Instance.IsOpen)
            {
                FlexiconPump.Instance.PumpOff(true);
                if (!await FlexiconPump.Instance.CloseSerialAsync())
                {
                    MessageBox.Show(HomeScreen.Instance, "Error closing serial channel", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            // ToDo: exit application

        }

    }
}
