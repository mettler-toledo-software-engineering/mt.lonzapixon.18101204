﻿/***************************************************************************************
/* File head comments and Copy Right Information
/*
/***************************************************************************************

MT.Sigularity.Platform.ApplicationExample provides a complete example for developing
your application on Singularity platfrom. Normally you could use this example as the
start point of your application.

Firstly we have to talk the common methodology of development, we commonly divide our
code into five layers.

----------------------------------------------------------------------------------------
View 
	"View" is the face of our application, our view classes should focus on showing 
	data and give controls to manipulate data. Please don't put business logic and 
	complex control logic in your view code.
----------------------------------------------------------------------------------------
View Model
	Singularity follows the MVVM pattern, the classes in business logic layer will 
	take the "Model" role in MVVM, and View Model classes will provide data and control
	logic for view code to manipulate data. Between the "View" and "View Model" we will
	have a lot of One-Way binding or Two-Way binding to tranfer data or signal between
	each other.
	View Model layer is the middle point between "View" and "Model", from achitect view,
	we don't allow "View" to directly access "Model".
	View <=> View Model <=> Model
	For detailed information of MVVM pattern, 
	you could go to https://msdn.microsoft.com/en-us/library/hh848246.aspx
----------------------------------------------------------------------------------------
Business Logic Layer
	Define validation rule of the data and how to manipulate your data.
	Please see the classses in the BusinessLogicLayer ...
----------------------------------------------------------------------------------------
Data Access Layer
	Define how the data will be persistent. For example, stores your data in database
	XML file, or other storage.
	Please see the classes in the DataAccessLayer ...
----------------------------------------------------------------------------------------
Data Layer
	Define all the data structure you will use in your application
	Please see the classes in the Data folder ...
----------------------------------------------------------------------------------------

Let's have a look on this application's drama ...

User story 1:
As a operator, I would like to have a color based indicator to tell me whether the 
weight of the goods is in the tolerance, then I could improve the speed of the operation.
Critieria:
	The indicator should show blue color when the weight is under the tolerance.
	The indicator should show green color when the weight is in the tolerance.
	The indicator should show red color when the weight is out of the tolerance.
	The indicator should tell me how far the weight is from the target weight.

User story 2:
As a supervior, I would like to record all transaction what I do, then I could search,
export these records, then I could know what happens in the process.
Criteria:
	Terminal should save the check-weigh result when operator clicks the print soft key.
	Terminal should provide a page to show the check weight record list and you could use
	one condition to search these records.
	Terminal should allow user to export the records as csv or xml format to USB disk or
	internal SD card, then user could get the file and do more analysis on the computer.

User story 3:
As the application provider, I would like to support multiple languages in my application,
then I could sell my application to different country. 
Criterial:
	Terminal should provide the possibility to translate all description text within the 
	application to the specified language (Now only support English, French, German)

User story 4:
As the customer application provider, I want to have a mechanism to forbid the application 
from the using without authorization, then I could protect my intellectual property.
Critieria:
	The user of application should input a proper password into the dongle, and the application
	will read this password from dongle when the applicaiton starts up and check if the password
	is correct.
		

We should split down the user story to detailed tasks for our implementation.
----------------------------------------------------------------------------------------
User story 1:
As a operator, I would like to have a color based indicator to tell me whether the 
weight of the goods is in the tolerance, then I could improve the speed of the operation.
Critieria:
	The indicator should show blue color when the weight is under the tolerance.
	The indicator should show green color when the weight is in the tolerance.
	The indicator should show red color when the weight is out of the tolerance.
	The indicator should tell me how far the weight is from the target weight.

	Task 1.1, Set your own database to store your data.
		1.1.1 Infrastructure\Bootstrapper.NeedToCreateCustomerDatabase
		1.1.2 Infrastructure\CustomDatabasePasswordProvider

	Task 1.2, Create and manipulate your configuration data.
		Task 1.2.1, Create the configuration data. (Note: need Supervisor or above write permission)
			Data\Configuration\CheckWeighConfiguration.cs
		Task 1.2.2, Create the data access class for configuration data persistence. 
			DataAccessLayer\Configuration\ConfigurationStore.cs
			This class is a common class for configuration data, you don't to implement
			other data access class for any other configuration data.
		Task 1.2.3, Create the parent setup node for your configuration data.
			ViewModel\Configuration\CheckWeighSetupParentNode.cs
		Task 1.2.4, Create the children setup node under your parent setup node. 
			ViewModel\Configuration\CheckWeighSetupChildrenNodeDeltaTrac.cs
		Task 1.2.5, Create the children setup node under your parent setup node.
			ViewModel\CheckWeighTablePage.cs
		Task 1.2.6, Create the setup page for reading and setting your configuration data. 
			ViewModel\Configuration\CheckWeighSetupChildrenNodeDeltaTrac.cs
			Please have a look on the ShowChildrenAsync method in this class.
	
	Task 1.3, Create a component who use the configuration to represent your business logic. 
		BusinessLogicLayer\ICheckWeighComponent.cs
		BusinessLogicLayer\CheckWeighComponent.cs

	Task 1.4, Create the presentation layer to use the component.
		Task 1.4.1, Create the home screen.
			View\CheckWeighHomeScreen.yml
			The Home Screen is a Navigation Page, and using Grid as layout panel. 
			The Grid have 1 column and 3 Rows.
			The first row is Weight Display Window, 
			second row is a DeltaTrac Control, 
			third row is a SoftKey Ribbon.
		Task 1.4.2, Manipulate the scale and soft keys.
			ViewModel\CheckWeighHomeScreenViewModel.cs
		Task 1.4.3, Add addition UI part to use your component.
			View\CheckWeighHomeScreen.yml
			ViewModel\CheckWeighHomeScreenViewModel.cs

----------------------------------------------------------------------------------------
User story 2:
As a supervior, I would like to record all transaction what I do, then I could search,
export these records, then I could know what happens in the process.
Criteria:
	Terminal should save the check-weigh result when operator clicks the print soft key.
	Terminal should provide a page to show the check weight record list and you could use
	one condition to search these records.
	Terminal should allow user to export the records as csv or xml format to USB disk or
	internal SD card, then user could get the file and do more analysis on the computer.

	Task 2.1, Create the check weigh data entity.
		Data\CheckWeigh\CheckWeighData.cs
		Data\CheckWeigh\CheckWeighState.cs
		Please pay attention to attributes on the property.
		PrimaryKey, the attribute represents the property it attatched to is a primary key in the label.
		AutoIncrement, that means this property should not be written by code, it will be generated by database.
		RelationName, the field name mapping to the property in the particular table.
		Searchable, the property supports the generic search operation as search condition.
		ColumnHeadTranslation, the name of the field supports the multi-language.

	Task 2.2, Create and manipulate your data entries.
		Infrastructure\DataAccess\RelationalDataStore.cs
		DataAccessLayer\CheckWeigh\CheckWeighStore.cs
		RelationalDataStore provides basic methods of a relational store class of an entity.
		CheckWeighStore inherits from RelationalDataStore, to manages the check-weigh data persistence.
	
	Task 2.3, Create a component that use check weigh data to represent your business logic. 
		Infrastructure\BusinessLogic\DataComponent.cs
		BusinessLogicLayer\CheckWeighComponent.cs
		DataComponent represents business logic of access data table.
		CheckWeighComponent owns a DataComponent instance as its field.
	
	Task 2.4, Provide check weigh data and control logic for view to manipulate
		Infrastructure\ViewModel\GenericSearchTableViewModel.cs
		ViewModel\CheckWeighTableViewModel.cs
		GenericSearchTableViewModel provides a generic class of the searchable table view.
		CheckWeighTableViewModel inherits from GenericSearchTableViewModel.
	
	Task 2.5, Create the presentation layer by using the component to show check weigh information.
		View\CheckWeighTableView.yml
		The Check Weigh Table is a Navigation Page, and using Grid as layout panel.
		The Grid have 1 column and 3 Rows.
		The first row is a TextBlock control to diaplay the tile of the table,
		second row is a DataGrid Control to show check weigh data entries,
		third row is a StackPanel to show the generic search and sort functions.

----------------------------------------------------------------------------------------
User story 3:
As the application provider, I would like to support multiple languages in my application,
then I could sell my application to different country. 
Criterial:
	Terminal should provide the possibility to translate all description text within the 
	application to the specified language (Now only support English, French, German)

	Task 3.1, Make your applicaiton globalized.
		Translation\CustomerTranslation.langxml
		Translation\HowToUserTranslation.txt
	
----------------------------------------------------------------------------------------
User story 4:
As the customer application provider, I want to have a mechanism to forbid the application 
from the using without authorization, then I could protect my itellectual property.
Critieria:
	The user of application should input a proper password into the dongle, and the application
	will read this password from dongle when the applicaiton starts up and check if the password
	is correct.

	Task 4.1 How to use dongle to protect your intellectual property
		Infrastructure\CustomApplicationVerifier.cs
		Infrastructure\HowToWorkWithDongle.txt
	

